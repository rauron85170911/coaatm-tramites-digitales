import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { TramitesModule } from './modules/tramites/tramites.module';
import { CoreModule } from './modules/core/core.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CertificadosModule } from './modules/certificados/certificados.module';
import { UsuariosModule } from './modules/usuarios/usuarios.module';
import { AuthGuard } from './guards/auth.guard';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, LOCALE_ID } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxCaptchaModule } from 'ngx-captcha';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutModule } from './modules/layout/layout.module';
import { MyCommonModule } from './modules/mycommon/mycommon.module';
import { ColegiacionModule } from './modules/colegiacion/colegiacion.module';
import { VisadosModule } from './modules/visados/visados.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoadingInterceptor } from './interceptors/loading.interceptor';
import { HomeComponent } from './pages/home/home.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { GenericErrorComponent } from './pages/generic-error/generic-error.component';
import { LoginComponent } from './pages/login/login.component';
import { fakeBackendProvider } from './interceptors/fakebackend.interceptor';
import { ErrorInterceptor } from './interceptors/error.interceptor';
import { JwtInterceptor } from './interceptors/auth.interceptor';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';

registerLocaleData(localeEs, 'es');
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NotFoundComponent,
    GenericErrorComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule, ReactiveFormsModule,
    NgxCaptchaModule,
    NgxSpinnerModule,
    NgbModule,
    MyCommonModule,
    LayoutModule,
    ColegiacionModule,
    VisadosModule,
    UsuariosModule,
    CertificadosModule,
    TramitesModule,
    AppRoutingModule,
    CoreModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoadingInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
    , {provide: LOCALE_ID, useValue: 'es-ES'}
    //fakeBackendProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

