import { LoginComponent } from './pages/login/login.component';
import { GenericErrorComponent } from './pages/generic-error/generic-error.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { IndexComponent as IndexColegiacionComponent } from './modules/colegiacion/index/index.component';
// import { IndexComponent as IndexVisadosComponent} from './modules/visados/index/index.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { AuthGuard } from './guards/auth.guard';
import { IndexComponent as IndexUsuarios } from './modules/usuarios/index/index.component';
const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuard], canActivateChild: [AuthGuard],
    data: {titulo: 'Portal de trámites digitales'}
  },
  {
    path: 'login',
    component: LoginComponent,
    data: { titulo: 'Portal de trámites digitales', layoutgeneral: true }
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuard], canActivateChild: [AuthGuard],
    data: {titulo: 'Portal de trámites digitales'}
  },
  {
    path: 'colegiacion',
    component: IndexColegiacionComponent
  },
  // {
  //   path: 'visados',
  //   component: IndexVisadosComponent,
  //   canActivateChild: [AuthGuard],
  //   canActivate: [AuthGuard],

  // },
  {
    path: 'usuarios/:id',
    component: IndexUsuarios
  },
  {
    path: 'usuarios',
    component: IndexUsuarios
  },
  {
    path: 'error',
    component: GenericErrorComponent
  },
  {
    path: 'error404',
    component: NotFoundComponent
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
