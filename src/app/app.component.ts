import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { AuthenticationService } from './modules/core/services/authentication.service';
import { Component, OnInit, HostListener, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { HeaderComponent } from './modules/layout/header/header.component';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  @ViewChild('contenedorPrincipal', null) contenedorPrincipal: ElementRef;
  title = 'coaatm-colegiacion';
  layoutgeneral = true;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authService: AuthenticationService,
    private renderer: Renderer2,
    private paginationconfig: NgbPaginationConfig) { }

  ngOnInit(): void {

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.layoutgeneral = this.activatedRoute.firstChild.snapshot.data.layoutgeneral !== false;
        // this.showHeader = this.activatedRoute.firstChild.snapshot.data.showHeader !== false;
        // this.showSidebar = this.activatedRoute.firstChild.snapshot.data.showSidebar !== false;
        // this.showFooter = this.activatedRoute.firstChild.snapshot.data.showFooter !== false;
      }
    });


    this.paginationconfig.boundaryLinks = true;
    this.paginationconfig.directionLinks = true;
    this.paginationconfig.ellipses = false;
    this.paginationconfig.maxSize = 5;


    this.authService.checkInicial();




  }

  /* Funcionalidad para calcular altos en el container principal  */
  @HostListener('window:scroll', ['$event'])
  doSomethingOnWindowsScroll($event: Event) {
    // const scrollOffset = $event.target.children[0].scrollTop;
    const ejeY = window.pageYOffset;
    // TODO: buscar otra forma de no usar el objeto window
    const sticky = this.contenedorPrincipal.nativeElement.offsetHeight;
    // console.log('el scroll vale: ' + scrollOffset);
    if (ejeY > 0) {
      // console.log('es sticky');
      this.renderer.addClass(this.contenedorPrincipal.nativeElement, 'sticky');
    } else {
      // console.log('no es sticky');
      this.renderer.removeClass(this.contenedorPrincipal.nativeElement, 'sticky');
    }
  }

}
