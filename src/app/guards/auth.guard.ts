import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild } from '@angular/router';
// import { AuthenticationService } from '@/_services';
import { AuthenticationService } from './../modules/core/services/authentication.service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate, CanActivateChild {
    
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

        if (!this.authenticationService.UsuarioEstaAutenticado) {
            this.router.navigate(['login'], {
                queryParams: {
                    return: state.url
                }
            });
            return false;
        }
        return true;
        // return this.authenticationService.currentUser$.pipe(
        //     map(usuario => !!usuario)
        // );
        // const currentUser = this.authenticationService.currentUser;
        // if (currentUser) {
        //     // authorised so return true
        //     return of(true);
        // }

        // // not logged in so redirect to login page with the return url
        // this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
        // return of(false);
    }
    canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.authenticationService.currentUser$.pipe(
            map(usuario => !!usuario)
        );
        // const currentUser = this.authenticationService.currentUser;
        // if (currentUser) {
        //     // authorised so return true
        //     return true;
        // }

        // // not logged in so redirect to login page with the return url
        // this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
        // return false;
    }
}