import { Router } from '@angular/router';
import { BackendTramitesService } from './../../../../tramites/services/backend-tramites.service';
import { GuiUtilsService } from 'src/app/modules/mycommon/services/gui-utils.service';
import { TablasMaestrasCacheService } from 'src/app/modules/core/services/tablas-maestras-cache.service';
import { CodigoDescripcion } from 'src/app/modules/core/models/models.index';
import { BackendService } from './../../services/backend.service';
import { Component, OnInit, Input } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Tramite2RouteService } from 'src/app/modules/core/services/tramite2-route.service';
import { FakeBackendService } from '../../services/fake-backend.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-ficha-intervencion',
  templateUrl: './ficha-intervencion.component.html',
  styleUrls: ['./ficha-intervencion.component.scss'],
  animations:
    [
      trigger(
        'dropdown',
        [
          state('open', style({
            opacity: 1, height: '*', overflow: 'hidden'
          })),
          state('closed', style({
            opacity: 0, height: '0px', overflow: 'hidden', padding: '0px'
          })),
          transition(
            '* => open',
            [
              animate('0.2s ease-in')
            ]
          ),
          transition(
            '* => closed',
            [
              animate('0.2s ease-in')
            ]
          )
        ]
      )
    ]
})
export class FichaIntervencionComponent implements OnInit {
  @Input() intervencion: any = null;
  constructor(
    private backend: BackendService,
    public t2route: Tramite2RouteService,
    private TablasMaestras: TablasMaestrasCacheService,
    private tramitesService: BackendTramitesService,
    private guiutils: GuiUtilsService,
    private route: Router) { }



  tramitesparavisado: string[] = ['ME', 'RE', 'AX'];
  tramitespararegistro: string[] = ['MR', 'RR', 'RX'];
  acciones: CodigoDescripcion[] = [];
  page = 1;
  pageSize = 3;
  desplegado = false;
  ngOnInit() {

    this.backend.GetIntervencion(this.intervencion.Id).subscribe(
      intervencion => {
        this.intervencion = intervencion;

        this.TablasMaestras.TiposTramite.subscribe(tramites => {
          if (intervencion.EsRegistro) {
            this.acciones = tramites.filter(t => {
              return this.tramitespararegistro.indexOf(t.Codigo) > -1;
            });
          } else {
            this.acciones = tramites.filter(t => {
              return this.tramitesparavisado.indexOf(t.Codigo) > -1;
            });
          }
        });
      }
    );
  }


  LanzarAccion(accion) {

    // TODO. De momento hasta confirmar más información lo vamos a implementar así
    let o: Observable<any> = null;
    if (['ME', 'MR'].indexOf(accion.Codigo) > -1) {
      o = this.tramitesService.CrearModificacionIntervencion(this.intervencion.Id, this.intervencion.EsRegistro);
    } else if (['RE', 'RR'].indexOf(accion.Codigo) > -1) {
      o = this.tramitesService.CrearRenunciaIntervencion(this.intervencion.Id, this.intervencion.EsRegistro);
    } else if (['AX', 'RX'].indexOf(accion.Codigo) > -1) {
      o = this.tramitesService.CrearAnexoIntervencion(this.intervencion.Id, this.intervencion.EsRegistro);
    } else {
      this.guiutils.ShowError('Acción no implementada');
    }
    if (o) {
      o.subscribe(resultado => {
        if (resultado.Ok) {
          this.t2route.Navigate(accion.Codigo, resultado.IdAsociado);
        } else {
          this.guiutils.ShowError(resultado.Descripcion);
        }
      });
    }






  }







}
