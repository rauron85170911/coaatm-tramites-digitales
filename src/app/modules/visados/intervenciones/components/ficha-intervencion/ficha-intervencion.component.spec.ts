import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FichaIntervencionComponent } from './ficha-intervencion.component';

describe('FichaIntervencionComponent', () => {
  let component: FichaIntervencionComponent;
  let fixture: ComponentFixture<FichaIntervencionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FichaIntervencionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FichaIntervencionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
