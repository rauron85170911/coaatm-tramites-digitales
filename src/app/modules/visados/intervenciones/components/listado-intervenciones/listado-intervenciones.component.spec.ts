import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListadoIntervencionesComponent } from './listado-intervenciones.component';

describe('ListadoIntervencionesComponent', () => {
  let component: ListadoIntervencionesComponent;
  let fixture: ComponentFixture<ListadoIntervencionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListadoIntervencionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadoIntervencionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
