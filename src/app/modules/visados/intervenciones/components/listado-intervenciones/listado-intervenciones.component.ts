import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { BackendService } from '../../services/backend.service';
import { FakeBackendService } from '../../services/fake-backend.service';

@Component({
  selector: 'app-listado-intervenciones',
  templateUrl: './listado-intervenciones.component.html',
  styleUrls: ['./listado-intervenciones.component.scss']
})
export class ListadoIntervencionesComponent implements OnInit, OnChanges {


  @Input() filtro: any = null;
  constructor(private backend: BackendService) { }

  page = 1;
  pageSize = 5;
  orden = 'fcreacion';
  sentido = '1';
  intervenciones: any[] = [];

  ordenaciones: any[] = [
    {
      value: 'fcreacion',
      text: 'F. Creación'
    },
    {
      value: 'factualizacion',
      text: 'F. Actualización'
    },
    {
      value: 'idintervencion',
      text: 'Cod. intervención'
    },
    {
      value: 'tipointervencion',
      text: 'Tipo intervención'
    },
  ];

  sentidos: any[] = [
    {
      value: '1',
      text: 'Ascendente'
    },
    {
      value: '-1',
      text: 'Descendente'
    }
  ];


  ngOnInit() {

    this.backend.Buscar(this.filtro).subscribe(intervenciones => {
      this.reordenar(intervenciones);
    });

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.filtro && changes.filtro.currentValue) {
      this.backend.Buscar(this.filtro).subscribe(intervenciones => {
        this.reordenar(intervenciones);
        this.filtro = null;
      });
    }
  }

  reordenar(v) {

    const sentido = Number(this.sentido);
    console.log(sentido);
    const ordenados: any[] = v.sort((i1, i2) => {
      switch (this.orden) {
        case 'fcreacion':
          return i1.FechaCreacion < i2.FechaCreacion ? -1 * sentido : 1 * sentido;
        case 'factualizacion':
          return i1.FechaActualizacion < i2.FechaActualizacion ? -1 * sentido : 1 * sentido;
        case 'tipointervencion':
          return i1.TipoIntervencion < i2.TipoIntervencion ? -1 * sentido : 1 * sentido;
        case 'idintervencion':
          return i1.Id < i2.Id ? -1 * sentido : 1 * sentido;
        default:
          return 0;
      }
    });
    this.intervenciones = ordenados;
    this.page = 1;


  }


}
