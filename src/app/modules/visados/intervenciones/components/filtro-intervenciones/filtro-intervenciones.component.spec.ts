import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltroIntervencionesComponent } from './filtro-intervenciones.component';

describe('FiltroIntervencionesComponent', () => {
  let component: FiltroIntervencionesComponent;
  let fixture: ComponentFixture<FiltroIntervencionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltroIntervencionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltroIntervencionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
