import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Filtro } from '../../models/models.index';

@Component({
  selector: 'app-filtro-intervenciones',
  templateUrl: './filtro-intervenciones.component.html',
  styleUrls: ['./filtro-intervenciones.component.scss']
})
export class FiltroIntervencionesComponent implements OnInit {

  @Output() filtrar: EventEmitter<Filtro> = new EventEmitter<Filtro>();

  constructor() { }
  estadosobra: string[] = ['Uno', 'Dos', 'Tres'];
  filtro: Filtro = new Filtro();

  ngOnInit() {
  }

}
