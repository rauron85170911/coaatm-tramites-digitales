import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable, of } from 'rxjs';
import { map, share, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FakeBackendService {

  constructor(private http: HttpClient) { }

  private cache: { [key: string]: any } = {};
  private observableCache: { [key: string]: Observable<any> } = {};

  Buscar(filtro: any): Observable<any[]> {
    const url = 'assets/mocks/intervenciones.json';
    return this.http.get(url).pipe(
      map((response: any) => response)
    );
  }

  GetIntervencion(idintervencion: number) {
    return this.pedirIntervencion(idintervencion);
    // const url = `${environment.apiUrlBase}/intervenciones/${idintervencion}`;
    // return this.http.get(url).pipe(
    //   map((response: any) => response)
    // );
  }

  private pedirIntervencion(idintervencion: number): Observable<any> {

    const key: string = '' + idintervencion;
    // if (filtro) {
    //   key += '#' + filtro;
    // }
    if (this.cache[key]) {
      return of(this.cache[key]);
    } else if (this.observableCache[key]) {
      return this.observableCache[key];
    } else {
      this.observableCache[key] = this.fetchIntervencion(idintervencion);
      return this.observableCache[key];

    }
  }



  private fetchIntervencion(idintervencion: number): Observable<any> {

    const url = `assets/mocks/intervenciones.json`;
    const key: string = '' + idintervencion;
    return this.http.get<any>(url).pipe(
      map(rawData => {
        this.observableCache[key] = null;
        this.cache[key] = rawData.filter(i => i.Id === idintervencion)[0];
        return this.cache[key];
      }),
      share(),
      catchError(error => {
        return of([]);
      })
    );
  }
}
