import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map, share, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  constructor(private http: HttpClient) { }

  private cache: { [key: string]: any } = {};
  private observableCache: { [key: string]: Observable<any> } = {};

  Buscar(filtro: any): Observable<any[]> {
    const url = `${environment.apiUrlBase}/intervenciones/`;
    return this.http.post(url, filtro).pipe(
      map((response: any) => response)
    );
  }

  GetIntervencion(idintervencion: number) {
    return this.pedirIntervencion(idintervencion);
    // const url = `${environment.apiUrlBase}/intervenciones/${idintervencion}`;
    // return this.http.get(url).pipe(
    //   map((response: any) => response)
    // );
  }

  private pedirIntervencion(idintervencion: number): Observable<any> {

    const key: string = '' + idintervencion;
    // if (filtro) {
    //   key += '#' + filtro;
    // }
    if (this.cache[key]) {
      return of(this.cache[key]);
    } else if (this.observableCache[key]) {
      return this.observableCache[key];
    } else {
      this.observableCache[key] = this.fetchIntervencion(idintervencion);
      return this.observableCache[key];

    }
  }



  private fetchIntervencion(idintervencion: number): Observable<any> {

    const url = `${environment.apiUrlBase}/intervenciones/${idintervencion}`;
    const key: string = '' + idintervencion;
    return this.http.get<any>(url).pipe(
      map(rawData => {
        this.observableCache[key] = null;
        this.cache[key] = rawData;
        return this.cache[key];
      }),
      share(),
      catchError(error => {
        return of([]);
      })
    );
  }







}
