import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CoreModule } from './../../core/core.module';
import { MyCommonModule } from './../../mycommon/mycommon.module';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FiltroIntervencionesComponent } from './components/filtro-intervenciones/filtro-intervenciones.component';
import { ListadoIntervencionesComponent } from './components/listado-intervenciones/listado-intervenciones.component';
import { FichaIntervencionComponent } from './components/ficha-intervencion/ficha-intervencion.component';

@NgModule({
  declarations: [FiltroIntervencionesComponent, ListadoIntervencionesComponent, FichaIntervencionComponent],
  imports: [
    CommonModule,
    FormsModule,
    MyCommonModule,
    CoreModule,
    NgbModule,
    RouterModule
  ],
  exports: [FiltroIntervencionesComponent, ListadoIntervencionesComponent, FichaIntervencionComponent]
})
export class IntervencionesModule { }
