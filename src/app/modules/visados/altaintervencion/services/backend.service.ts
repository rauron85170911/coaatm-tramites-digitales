import { ResultadoAccion } from './../../../core/models/resultadoaccion.model';
import { ErrorsService } from '../../../mycommon/services/errors.service';
import { environment } from 'src/environments/environment';
import {
  Ubicacion, Coste, AltaIntervencion,
  DatoComplementario, Estadisticas, DatosComplementarios
} from '../../models/models.index';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { TipoDocumento } from '../../../core/models/models.index';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  constructor(private http: HttpClient, private errorService: ErrorsService) { }

  public GetParametrosDatosComplementarios$(
    tipointervencion: string,
    registro: boolean,
    modificacion: boolean): Observable<DatoComplementario[]> {

    
    const url = `${environment.apiUrlBase}/altaintervencion/${tipointervencion}/parametros?registro=${registro}&modificacion=${modificacion}`;
    return this.httpget(url).pipe(
      map(response => {
        return response;
      })
    );
  }


  public GetTiposDocumento(idSolicitud: number, idTipoIntervencion: string, registro: boolean, modificacion: boolean): Observable<TipoDocumento[]> {
    const subpath = modificacion ? 'modificacion' : 'alta';
    const url = `${environment.apiUrlBase}/altaintervencion/${idSolicitud}/${idTipoIntervencion}/tiposdocumento?registro=${registro}&modificacion=${modificacion}`;
    return this.httpget(url).pipe(
      map(response => {
        return response;
      })
    );
  }


  public CalcularCostes(tipointervencion: string,
    datoscomplementarios: DatosComplementarios,
    registro: boolean, modificacion: boolean ): Observable<Coste[]> {
    const url = `${environment.apiUrlBase}/costes/gastosgestion`;

    return this.http.post(url, datoscomplementarios).pipe(
      map((response: Coste[]) => {
        return response;
      })
    );
  }

  public CrearSolicitud(datos: AltaIntervencion, registro: boolean): Observable<AltaIntervencion> {
    const url = `${environment.apiUrlBase}/altaintervencion?registro=${registro}&modificacion=${false}`;
    return this.http.post(url, datos).pipe(
      map(response => response),
      catchError(error => {
        this.errorService.RegistrarErrorHttp(error);
        return of(null);
      })
    );
  }

  public GuardarSolicitud(datos: AltaIntervencion, registro: boolean, modificacion: boolean): Observable<AltaIntervencion> {

    const url = `${environment.apiUrlBase}/altaintervencion/${datos.IdSolicitud}?registro=${registro}&modificacion=${modificacion}`;
    return this.http.put(url, datos).pipe(
      map((response: AltaIntervencion) => response),
      catchError(error => {
        this.errorService.RegistrarErrorHttp(error);
        return of(null);
      })
    );

  }

  public VerificarAltaIntervencion(idSolicitud: number, registro: boolean, modificacion: boolean): Observable<any> {

    const url = `${environment.apiUrlBase}/altaintervencion/${idSolicitud}/verificar?registro=${registro}&modificacion=${modificacion}`;
    return this.http.get(url).pipe(
      map((response) => {
        return response;
      })
    );
  }

  public ValidarAltaIntervencion(idSolicitud: number, consentimientos: any, registro: boolean, modificacion: boolean): Observable<any> {
    const url = `${environment.apiUrlBase}/altaintervencion/${idSolicitud}/validar?registro=${registro}&modificacion=${modificacion}`;

    return this.http.put(url, consentimientos).pipe(
      map((response) => {
        return response;
      })
    );
  }
  public RevertirAltaIntervencion(idSolicitud: number, registro: boolean, modificacion: boolean): Observable<any> {
    const url = `${environment.apiUrlBase}/altaintervencion/${idSolicitud}/revertir?registro=${registro}&modificacion=${modificacion}`;
    return this.http.put(url, null).pipe(
      map((response) => {
        return response;
      })
    );
  }

  public EnviarSolicitud(idSolicitud: number): Observable<ResultadoAccion> {
    const url = `${environment.apiUrlBase}/tramites/${idSolicitud}/enviar`;
    return this.http.put(url, null).pipe(
      map((response: ResultadoAccion) => response)
    );

  }



  public GetSolicitud(idsolicitud: number, registro: boolean, modificacion: boolean): Observable<AltaIntervencion> {

    const url = `${environment.apiUrlBase}/altaintervencion/${idsolicitud}?registro=${registro}&modificacion=${modificacion}`;
    return this.http.get(url).pipe(
      map((response: AltaIntervencion) => {
        response.DocumentacionAportada = [];
        if (!response.Estadisticas) {
          response.Estadisticas = new Estadisticas();
        }
        return response;
      })
    );
  }

  public GetSolicitudVacia(registro: boolean): Observable<AltaIntervencion> {
    const url = `${environment.apiUrlBase}/altaintervencion/nueva?registro=${registro}&modificacion=${false}`;
    return this.http.get(url).pipe(
      map((response: AltaIntervencion) => {
        response.IdSolicitud = null;
        response.DocumentacionAportada = [];
        if (!response.Estadisticas) {
          response.Estadisticas = new Estadisticas();
        }
        return response;
      })
    );
  }




  public handleError(error: any) {
    return of(null);
  }
  private httpget(url: string): Observable<any> {
    // const url = `${environment.apiUrlBase}/maestros/${tabla}`;
    return this.http.get(url);
  }



}
