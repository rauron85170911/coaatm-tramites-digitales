import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CoreModule } from '../../core/core.module';
import { TipoActuacionIntervencionComponent } from './components/tipo-actuacion-intervencion/tipo-actuacion-intervencion.component';
import { TipoActuacionComponent } from './components/tipo-actuacion/tipo-actuacion.component';
import { MyCommonModule } from '../../mycommon/mycommon.module';
import { UbicacionIntervencionComponent } from './components/ubicacion-intervencion/ubicacion-intervencion.component';
import { DatosComplementariosComponent } from './components/datos-complementarios/datos-complementarios.component';
import { EstadisticasComponent } from './components/estadisticas/estadisticas.component';
import { CosteComponent } from './components/coste/coste.component';
// tslint:disable-next-line: import-spacing
import { ResultadoDesfavorableIteComponent }
          from './components/estadisticas/resultado-desfavorable-ite/resultado-desfavorable-ite.component';


import { ValidacionConsentimientoComponent } from './components/validacion-consentimiento/validacion-consentimiento.component';
// tslint:disable-next-line: max-line-length
import { DatosModificacionIntervencionComponent } from './components/datos-modificacion-intervencion/datos-modificacion-intervencion.component';

@NgModule({
  declarations: [TipoActuacionIntervencionComponent,
    TipoActuacionComponent,
    UbicacionIntervencionComponent,
    DatosComplementariosComponent,
    EstadisticasComponent,
    CosteComponent,
    ResultadoDesfavorableIteComponent,
    ValidacionConsentimientoComponent,
    DatosModificacionIntervencionComponent],
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,
    CoreModule,
    MyCommonModule
  ],
  exports: [TipoActuacionIntervencionComponent,
    UbicacionIntervencionComponent,
    DatosComplementariosComponent,
    EstadisticasComponent,
    CosteComponent,
    ValidacionConsentimientoComponent,
    DatosModificacionIntervencionComponent]
})
export class AltaintervencionModule { }
