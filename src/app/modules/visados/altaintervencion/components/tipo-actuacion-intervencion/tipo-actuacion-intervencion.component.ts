import { FavoritosService } from './../../../../core/services/favoritos.service';

import { CodigoDescripcionAmpliado } from './../../../../core/models/codigodescripcionampliado.model';
import { TablasMaestrasCacheService } from 'src/app/modules/core/services/tablas-maestras-cache.service';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { CodigoDescripcion, TipoIntervencion } from 'src/app/modules/core/models/models.index';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { trigger, transition, style, animate } from '@angular/animations';
import { BackendService } from '../../services/backend.service';

@Component({
  selector: 'app-tipo-actuacion-intervencion',
  templateUrl: './tipo-actuacion-intervencion.component.html',
  styleUrls: ['./tipo-actuacion-intervencion.component.scss'],
  animations:
    [
      trigger(
        'fadeinout',
        [
          transition(
            ':enter',
            [
              style({ opacity: 0, height: '0px', overflow: 'hidden' }),
              animate('0.3s 0.4s ease-in',
                style({ opacity: 1, height: '*' })
              )
            ]
          ),
          transition(
            ':leave',
            [
              style({ opacity: 1, overflow: 'hidden' }),
              animate('0.3s ease-in',
                style({ opacity: 0, height: '0px' })
              )
            ]
          )
        ]
      )
    ]
})
export class TipoActuacionIntervencionComponent implements OnInit {

  // tslint:disable-next-line: no-output-on-prefix
  @Input() actuacion: CodigoDescripcion = null;
  @Input() intervencion: TipoIntervencion = null;
  @Input() readonly = false;
  @Output() actuacionChange: EventEmitter<CodigoDescripcion> = new EventEmitter<CodigoDescripcion>();
  @Output() intervencionChange: EventEmitter<TipoIntervencion> = new EventEmitter<TipoIntervencion>();






  seleccionando = false;
  filtroChanged: Subject<string> = new Subject<string>();
  filtro = '';

  tiposactuacion: CodigoDescripcion[] = [];
  tiposintervencion: TipoIntervencion[] = [];
  tiposintervencionfiltradas: TipoIntervencion[] = [];

  favoritos: CodigoDescripcionAmpliado[] = [];

  seleccionada: any = {
    actuacion: '',
    intervencion: ''
  };

  constructor(
    private tablasMaestrasCache: TablasMaestrasCacheService,
    private favoritosService: FavoritosService,
    private backend: BackendService) { }


  ngOnInit() {

    this.tablasMaestrasCache.TiposActuacion.subscribe(data => {
      this.tiposactuacion = data;
    });
    this.tablasMaestrasCache.TiposIntervencion.subscribe(data => {
      this.tiposintervencion = data;
      this.tiposintervencionfiltradas = data;
    });

    this.favoritosService.TiposIntervencion.subscribe(data => {
      this.favoritos = data;
    });

    this.filtroChanged.pipe(debounceTime(500), distinctUntilChanged())
      .subscribe(model => {
        this.filtro = model;
        this.tiposintervencionfiltradas =
          this.tiposintervencion.filter(
            ti => {
              const tiposActuacion = this.tiposactuacion
                .filter(ta => ta.Descripcion.toLocaleLowerCase().indexOf(this.filtro.toLocaleLowerCase()) > -1)
                .map(ta => ta.Codigo);
              return ti.Descripcion.toLocaleLowerCase().indexOf(this.filtro.toLocaleLowerCase()) > -1
                || tiposActuacion.indexOf(ti.CodigoTipoActuacion) > -1
                || ti.Codigo.indexOf(this.filtro) > -1; // añadimos esta condición para poder buscar por el código
            });
      });

  }

  onFilterChange(query: string) {
    this.filtroChanged.next(query);
  }
  seleccionafrecuente($event: CodigoDescripcionAmpliado) {
    this.intervencion = this.tiposintervencion.filter(t => t.Codigo === $event.Codigo)[0];
    this.actuacion = this.tiposactuacion.filter(t => t.Codigo === $event.CodigoGrupo)[0];
    this.seleccionando = false;
    this.actuacionChange.emit(this.actuacion);
    this.intervencionChange.emit(this.intervencion);
  }

  selecciona($event: TipoIntervencion) {
    this.intervencion = $event;
    this.actuacion = this.tiposactuacion.filter(t => t.Codigo === $event.CodigoTipoActuacion)[0];
    this.seleccionando = false;

    this.actuacionChange.emit(this.actuacion);
    this.intervencionChange.emit(this.intervencion);


    // this.seleccionada.intervencion = $event.Descripcion;
    // this.seleccionada.actuacion = this.tiposactuacion.filter(t => t.Codigo === $event.CodigoTipoActuacion)[0].Descripcion;

    // this.onSelect.emit($event.Codigo);
  }


}
