import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoIntervencionComponent } from './tipo-actuacion-intervencion.component';

describe('TipoIntervencionComponent', () => {
  let component: TipoIntervencionComponent;
  let fixture: ComponentFixture<TipoIntervencionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoIntervencionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoIntervencionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
