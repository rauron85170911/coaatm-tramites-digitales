import { Component, OnInit, DoCheck, Input, Output, EventEmitter, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { throwIfEmpty } from 'rxjs/operators';


const noop = () => {
};

export const RESULTADO_DESFAVORABLE_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  // tslint:disable-next-line: no-use-before-declare
  useExisting: forwardRef(() => ResultadoDesfavorableIteComponent),
  multi: true
};

@Component({
  selector: 'app-resultado-desfavorable-ite',
  templateUrl: './resultado-desfavorable-ite.component.html',
  styleUrls: ['./resultado-desfavorable-ite.component.scss'],
  providers: [RESULTADO_DESFAVORABLE_CONTROL_VALUE_ACCESSOR]
})
export class ResultadoDesfavorableIteComponent implements ControlValueAccessor {

  @Input() valor: string;
  @Output() valorChanged: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  public innerValue: string = '';

  private onTouchedCallback: () => void = noop;
  private onChangeCallback: (_: any) => void = noop;

  opciones: any[] =
    [
      {
        titulo: 'A',
        valor: 'A',
        checked: false
      },
      {
        titulo: 'B',
        valor: 'B',
        checked: false
      },
      {
        titulo: 'C',
        valor: 'C',
        checked: false
      },
      {
        titulo: 'D',
        valor: 'D',
        checked: false
      },
      {
        titulo: 'E',
        valor: 'E',
        checked: false
      }
    ];

  get value(): string {
    return this.innerValue;
  }

  set value(v: string) {
    if (v !== this.innerValue) {
      this.innerValue = v;
      this.onChangeCallback(v);
    }
  }

  writeValue(value: any): void {
    if (value !== this.innerValue) {
      this.innerValue = value;
      // establecemos el estado
      if (this.innerValue) {
        this.opciones.forEach(o => {
          o.checked = this.innerValue.indexOf(o.valor) > -1;
        });
      }

    }
  }
  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }
  setDisabledState?(isDisabled: boolean): void {

  }


  recalcular() {
    const valor = this.opciones.filter(o => o.checked).map(o => o.valor).join('');
    this.writeValue(valor);
    this.onChangeCallback(valor);
  }

}
