import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultadoDesfavorableIteComponent } from './resultado-desfavorable-ite.component';

describe('ResultadoDesfavorableIteComponent', () => {
  let component: ResultadoDesfavorableIteComponent;
  let fixture: ComponentFixture<ResultadoDesfavorableIteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultadoDesfavorableIteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultadoDesfavorableIteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
