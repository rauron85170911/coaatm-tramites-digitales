import { Ubicacion } from './../../../models/ubicacion.model';
import { environment } from 'src/environments/environment';
import { CodigoDescripcion } from './../../../../core/models/codigodescripcion.model';
import { TablasMaestrasCacheService } from 'src/app/modules/core/services/tablas-maestras-cache.service';
import { Estadisticas } from './../../../models/estadisticas.model';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TipoIntervencion } from 'src/app/modules/core/models/models.index';


@Component({
  selector: 'app-estadisticas',
  templateUrl: './estadisticas.component.html',
  styleUrls: ['./estadisticas.component.scss']
})
export class EstadisticasComponent implements OnInit {

  @Input() readonly = false;
  @Input() tipointervencion: TipoIntervencion;
  @Input() ubicacion: Ubicacion;
  @Input() estadisticas: Estadisticas;
  @Output() estadisticasChange: EventEmitter<Estadisticas> = new EventEmitter<Estadisticas>();

  constructor(private tablasMaestrasService: TablasMaestrasCacheService) { }
  get CODIGOMADRID() {
    return environment.constantes.CODIGOMADRID;
  }
  destinos: CodigoDescripcion[] = [];
  tipospromotor: CodigoDescripcion[] = [];


  nivelesControlCalidad = [
    {
      titulo: 'Reducido',
      valor: 'R'
    },
    {
      titulo: 'Normal',
      valor: 'N'
    },
    {
      titulo: 'Alto',
      valor: 'A'
    }
  ];

  medianerias = [
    {
      titulo: 'No',
      valor: 'N'
    },
    {
      titulo: 'Un lado',
      valor: 'U'
    },
    {
      titulo: 'Más de un lado',
      valor: 'M'
    }
  ];

  usosedificio = [
    {
      titulo: 'Venta',
      valor: 'V'
    },
    {
      titulo: 'Alquiler',
      valor: 'Q'
    },
    {
      titulo: 'Autouso',
      valor: 'A'
    }
  ];


  ngOnInit() {

    this.tablasMaestrasService.DestinosPrincipalesObra.subscribe(
      {
        next: destinos => {
          this.destinos = destinos;
        }
      }
    );
    this.tablasMaestrasService.TiposPromotor.subscribe(
      {
        next: tipospromotor => {
          this.tipospromotor = tipospromotor;
        }
      }
    );




  }

}
