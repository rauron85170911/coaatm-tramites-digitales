import { FavoritosService } from './../../../../core/services/favoritos.service';
import { environment } from './../../../../../../environments/environment';
import { BackendService } from './../../../../core/services/backend.service';
import { CodigoDescripcion } from 'src/app/modules/core/models/models.index';
import { Component, OnInit, Input, Output, EventEmitter, DoCheck, OnChanges, SimpleChanges } from '@angular/core';
import { Ubicacion } from '../../../models/models.index';
import { Observable } from 'rxjs';
import { trigger, transition, style, animate } from '@angular/animations';
import { TablasMaestrasCacheService } from 'src/app/modules/core/services/tablas-maestras-cache.service';

@Component({
  selector: 'app-ubicacion-intervencion',
  templateUrl: './ubicacion-intervencion.component.html',
  styleUrls: ['./ubicacion-intervencion.component.scss'],
  animations:
    [
      trigger(
        'fadeinout',
        [
          transition(
            ':enter',
            [
              style({ opacity: 0, height: '0px', overflow: 'hidden' }),
              animate('0.3s 0.4s ease-in',
                style({ opacity: 1, height: '*' })
              )
            ]
          ),
          transition(
            ':leave',
            [
              style({ opacity: 1, overflow: 'hidden' }),
              animate('0.3s ease-in',
                style({ opacity: 0, height: '0px' })
              )
            ]
          )
        ]
      )
    ]
})
export class UbicacionIntervencionComponent implements OnInit, OnChanges {



  @Input() ubicacion: Ubicacion = null;
  @Input() readonly = false;
  @Output() ubicacionChange: EventEmitter<Ubicacion> = new EventEmitter<Ubicacion>();
  constructor(
    public tablasMaestrasService: TablasMaestrasCacheService,
    private backendService: BackendService,
    private favoritosService: FavoritosService) { }
  seleccionando = false;

  tiposVia: CodigoDescripcion[] = null;
  provincias: CodigoDescripcion[];
  municipios: CodigoDescripcion[];
  distritos: CodigoDescripcion[];
  favoritos: Ubicacion[] = null;

  get CODIGOMADRID() {
    return environment.constantes.CODIGOMADRID;
  }


  ngOnInit() {
    this.tablasMaestrasService.TiposVia.subscribe(tiposvia => { this.tiposVia = tiposvia; });
    this.tablasMaestrasService.Provincias.subscribe(provincias => { this.provincias = provincias; });
    this.tablasMaestrasService.DistritosMadrid.subscribe(distritos => { this.distritos = distritos; });
    this.favoritosService.Ubicaciones.subscribe(favoritos => {
      this.favoritos = favoritos;
    });


  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.ubicacion.currentValue.Provincia) {
      this.cargarMunicipios();
    }
  }

  toggleSeleccionando() {
    this.seleccionando = !this.seleccionando;
  }

  cargarMunicipios() {
    if (this.ubicacion.Provincia && this.ubicacion.Provincia !== '') {
      const codigoprovincia = this.ubicacion.Provincia.padStart(2, '0');
      // this.ubicacion.CodigoPostal = `${codigoprovincia}000`;
      this.tablasMaestrasService.getMuniciposProvincia(this.ubicacion.Provincia)
        .subscribe(municipios => {
          this.municipios = municipios;
          // si el municipio no empieza por el código de provincia lo establezco a null
          if (this.ubicacion.Municipio && !this.ubicacion.Municipio.startsWith(codigoprovincia)) {
            this.ubicacion.Municipio = null;
            this.ubicacion.Localidad = null;
            this.ubicacion.Distrito = null;
           }
        });
    } else {
      this.ubicacion.Municipio = null;
      this.ubicacion.Localidad = null;
      this.municipios = [];
    }

  }

  inicializarCodigoPostalPorProvincia() {
    if (this.ubicacion.Provincia && this.ubicacion.Provincia !== '') {
      const codigoprovincia = this.ubicacion.Provincia.padStart(2, '0');
      this.ubicacion.CodigoPostal = `${codigoprovincia}000`;
    } else {
      this.ubicacion.CodigoPostal = null;
    }

  }
  asignarMunicipio() {
    // limpiamos el distrito
    this.ubicacion.Distrito = null;
    // asignamos a localidad la descripción del municipio
    const municipio = this.municipios.filter(m => m.Codigo === this.ubicacion.Municipio)[0];
    this.ubicacion.Localidad = !!municipio ? municipio.Descripcion : '';
  }
  ubicacionAnteriorSeleccionada(u: Ubicacion) {

    this.ubicacion = u;
    this.seleccionando = false;
    this.ubicacionChange.emit(this.ubicacion);
    this.cargarMunicipios();
  }
}
