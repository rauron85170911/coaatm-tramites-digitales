import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UbicacionIntervencionComponent } from './ubicacion-intervencion.component';

describe('UbicacionIntervencionComponent', () => {
  let component: UbicacionIntervencionComponent;
  let fixture: ComponentFixture<UbicacionIntervencionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UbicacionIntervencionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UbicacionIntervencionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
