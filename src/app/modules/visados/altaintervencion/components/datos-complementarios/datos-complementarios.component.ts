import { DatoComplementario, DatosComplementarios } from './../../../models/models.index';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-datos-complementarios',
  templateUrl: './datos-complementarios.component.html',
  styleUrls: ['./datos-complementarios.component.scss']
})
export class DatosComplementariosComponent implements OnInit {

  @Input() readonly = false;
  @Input() ESREGISTRO = false;
  @Input() datoscomplementarios: DatosComplementarios;
  @Output() datoscomplementariosChange: EventEmitter<DatosComplementarios[]> = new EventEmitter<DatosComplementarios[]>();
  @Output() cambio: EventEmitter<null> = new EventEmitter<null>();
  constructor() { }
  dataChanged: Subject<string> = new Subject<string>();


  ngOnInit() {

    this.dataChanged.pipe(debounceTime(500))
    .subscribe(model => {
      console.log(`debounce ${model}`);
      this.cambio.emit();
      // if (!isNaN(Number(model))) {
      //   this.cambio.emit();
      // }
    });

    // this.dataChanged.pipe(debounceTime(500), distinctUntilChanged())
    //   .subscribe(model => {
    //     console.log(`debounce ${model}`);
    //     this.cambio.emit();
    //     // if (!isNaN(Number(model))) {
    //     //   this.cambio.emit();
    //     // }
    //   });

  }

  indexTracker(index: number, value: any) {
    return index;
  }

  onFilterChange(query: string) {
    console.log(query);
    this.dataChanged.next(query);
  }
}
