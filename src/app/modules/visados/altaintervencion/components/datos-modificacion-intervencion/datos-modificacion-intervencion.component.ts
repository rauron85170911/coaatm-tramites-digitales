import { CodigoDescripcion } from './../../../../core/models/codigodescripcion.model';
import { Component, OnInit, Input } from '@angular/core';
import { TablasMaestrasCacheService } from 'src/app/modules/core/services/tablas-maestras-cache.service';

@Component({
  selector: 'app-datos-modificacion-intervencion',
  templateUrl: './datos-modificacion-intervencion.component.html',
  styleUrls: ['./datos-modificacion-intervencion.component.scss']
})
export class DatosModificacionIntervencionComponent implements OnInit {

  @Input() datosmodificacion: any = {};
  @Input() readonly = false;
  constructor(private tablasmaestras: TablasMaestrasCacheService) { }

  motivos: CodigoDescripcion[] = [];
  ngOnInit() {
    this.tablasmaestras.MotivosModificacionIntervencion.subscribe(motivos => {
      this.motivos = motivos;
    });
  }

}
