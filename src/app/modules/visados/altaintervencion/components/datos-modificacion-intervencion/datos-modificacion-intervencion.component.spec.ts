import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosModificacionIntervencionComponent } from './datos-modificacion-intervencion.component';

describe('DatosModificacionIntervencionComponent', () => {
  let component: DatosModificacionIntervencionComponent;
  let fixture: ComponentFixture<DatosModificacionIntervencionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosModificacionIntervencionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosModificacionIntervencionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
