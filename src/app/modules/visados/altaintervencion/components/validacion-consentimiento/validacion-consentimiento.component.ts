import { AuthenticationService } from './../../../../core/services/authentication.service';
import { ValidacionService } from './../../../../mycommon/services/validacion.service';
import { AltaIntervencion } from './../../../models/altaintervencion.model';
import { environment } from './../../../../../../environments/environment';
import { Ubicacion } from './../../../models/ubicacion.model';

import { ColegiadoInterviniente } from './../../../../core/models/colegiadointerviniente.model';
import { TipoIntervencion } from './../../../../core/models/tipointervencion.model';
import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { BackendService as IntervinientesService } from 'src/app/modules/core/services/backend.service';
import { BackendService as AltaIntervenciondService } from '../../services/backend.service';
import { GuiUtilsService } from 'src/app/modules/mycommon/services/gui-utils.service';
@Component({
  selector: 'app-validacion-consentimiento',
  templateUrl: './validacion-consentimiento.component.html',
  styleUrls: ['./validacion-consentimiento.component.scss']
})
export class ValidacionConsentimientoComponent implements OnInit, OnChanges {


  @Input() tipointervencion: TipoIntervencion = null;
  @Input() ubicacion: Ubicacion = null;
  @Input() ESREGISTRO = false;
  @Input() ESMODIFICACION = false;
  @Input() solicitud: AltaIntervencion = null;
  @Output() solicitudChange: EventEmitter<AltaIntervencion> = new EventEmitter<AltaIntervencion>();
  constructor(
    private guiUtils: GuiUtilsService,
    private iService: IntervinientesService,
    private aService: AltaIntervenciondService,
    private validacionService: ValidacionService,
    public authService: AuthenticationService) { }

  colegiados: ColegiadoInterviniente[] = [];

  get colegiadossinvalidar(): boolean {
    return this.colegiados.filter(c => !c.SolicitudValidada).length > 0;
  }

  get colegiadohavalidado(): boolean {
    // tslint:disable-next-line: triple-equals
    return this.colegiados.filter(c => c.SolicitudValidada && c.IdColegiado == this.authService.currentUser.Id).length > 0;
  }

  ofrecerseguro = false;
  solicitarIEE = false;


  consentimientos: any = {
    Seguro: false,
    IEE: false,
    AceptacionVisado: false
  };

  ngOnInit() {
    this.cargarIntervinientes();
  }


  ngOnChanges(changes: SimpleChanges): void {

    const tiposactuacionparaseguro: string[] = [
      '4.2.3.0.0.6', '4.2.3.0.2.1', '4.2.3.0.3.5', '4.2.3.0.3.8', '4.2.3.0.4.4', , '4.3.1.0.0.1'
    ];

    if (changes.solicitud) { // && !changes.solicitud.firstChange) {

      this.cargarIntervinientes();
      this.ofrecerseguro = tiposactuacionparaseguro.indexOf(this.solicitud.TipoIntervencion.Codigo) > -1;
      this.solicitarIEE = this.solicitud.TipoIntervencion.Codigo === '4.2.3.0.4.5' &&
        this.solicitud.Ubicacion.Provincia === environment.constantes.CODIGOPROVINCIAMADRID;
      this.consentimientos.AceptacionVisado = this.solicitud.DeclaracionResponsabilidadSolicitante;
    }

  }

  cargarIntervinientes() {
    if (this.solicitud && this.solicitud.IdSolicitud) {
      this.iService.GetColegiadosIntervinientes(this.solicitud.IdSolicitud, this.ESREGISTRO).subscribe(colegiados => {
        this.colegiados = colegiados;
        
        // tslint:disable-next-line: triple-equals
        const currentcolegiado = this.colegiados.filter(c => c.IdColegiado == this.authService.currentUser.Id)[0];
        this.consentimientos.Seguro = currentcolegiado.SeguroAceptado;
        this.consentimientos.IEE = currentcolegiado.InformeEvaluacionEdificiosAceptado;
      });
    }
  }

  validar() {
    this.aService.GuardarSolicitud(this.solicitud, this.ESREGISTRO, this.ESMODIFICACION).subscribe(solicitud => {
      if (solicitud) {
        // this.solicitudChange.emit(solicitud);
        this.aService.VerificarAltaIntervencion(this.solicitud.IdSolicitud, this.ESREGISTRO, this.ESMODIFICACION).subscribe(resultado => {
          if (resultado.Valido) {
            // debugger;
            this.aService.ValidarAltaIntervencion(this.solicitud.IdSolicitud, this.consentimientos, this.ESREGISTRO, this.ESMODIFICACION).subscribe(r => {
              if (r.Ok) {
                this.cargarIntervinientes();
                // recargamos la solicitud
                this.aService.GetSolicitud(solicitud.IdSolicitud, this.ESREGISTRO, this.ESMODIFICACION).subscribe(s => {
                  this.solicitudChange.emit(s);
                });
                this.guiUtils.ShowSuccess('La solicitud se ha validado correctamente');
              } else {
                this.guiUtils.ShowError(r.Descripcion);
              }
            });
          } else {
            this.validacionService.Inicializar();
            this.guiUtils.ShowError('La solicitud presenta errores de verificación, no es posible validarla');
            resultado.Errors.forEach(e => {
              this.validacionService.AddErrorValidacion(e.Descripcion, e.Paso);
            });
          }
        });
      } else {
        this.guiUtils.ShowError('No se ha podido guardar la solicitud, no se realiza la validación');
      }

    });


  }

  revertir() {
    this.aService.RevertirAltaIntervencion(this.solicitud.IdSolicitud, this.ESREGISTRO, this.ESMODIFICACION).subscribe(r => {
      if (r.Ok) {
        this.cargarIntervinientes();
        this.guiUtils.ShowSuccess('La solicitud se ha vuelto al estado ELABORACIÓN');
        this.aService.GetSolicitud(this.solicitud.IdSolicitud, this.ESREGISTRO, this.ESMODIFICACION).subscribe(s => {
          this.solicitudChange.emit(s);
        });
      } else {
        this.guiUtils.ShowError(r.Descripcion);
      }
    });
  }

  verificar() {

    this.aService.GuardarSolicitud(this.solicitud, this.ESREGISTRO, this.ESMODIFICACION).subscribe(solicitud => {
      if (solicitud) {
        this.solicitudChange.emit(solicitud);
        this.aService.VerificarAltaIntervencion(this.solicitud.IdSolicitud, this.ESREGISTRO, this.ESMODIFICACION).subscribe(resultado => {
          this.validacionService.Inicializar();
          resultado.Errors.forEach(e => {
            this.validacionService
              .AddErrorValidacion(e.Descripcion, e.Paso);
          });
          resultado.Warnings.forEach(e => {
            this.validacionService
              .AddErrorValidacion(e.Descripcion, e.Paso);
          });
          if (resultado.Valido) {
            this.guiUtils.ShowSuccess(`No se han detectado errores de verificación,
                          la solicitud puede ser validada para su posterior envío`);
          }
        });
      } else {
        this.guiUtils.ShowError('No se ha podido guardar la solicitud, no se realiza la verificación');
      }
    },
      error => {
        this.guiUtils.ShowError(error);
      });
  }

  enviar() {
    this.aService.EnviarSolicitud(this.solicitud.IdSolicitud).subscribe(resultado => {
      if (resultado.Ok){
        this.solicitud.Estado = 'EN';
        this.guiUtils.ShowInfo('La solicitud se envió correctamente');
      } else{
        
      }
    });
  }




}
