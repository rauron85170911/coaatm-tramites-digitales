import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidacionConsentimientoComponent } from './validacion-consentimiento.component';

describe('ValidacionComponent', () => {
  let component: ValidacionConsentimientoComponent;
  let fixture: ComponentFixture<ValidacionConsentimientoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidacionConsentimientoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidacionConsentimientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
