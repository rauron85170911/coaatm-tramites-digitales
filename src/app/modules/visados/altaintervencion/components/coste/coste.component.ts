import { Component, OnInit, Input, EventEmitter, Output, OnChanges, SimpleChanges } from '@angular/core';
import { Coste } from '../../../models/models.index';

@Component({
  selector: 'app-coste',
  templateUrl: './coste.component.html',
  styleUrls: ['./coste.component.scss']
})
export class CosteComponent implements OnInit, OnChanges {


  @Input() costes: Coste[];
  @Input() importecostetotal: number;
  @Output() importecostetotalChange: EventEmitter<number> = new EventEmitter<number>();
  @Input() readonly = false;
  @Input() resumen = false;
  @Input() modificacion: boolean = null; // = false; // si es para modificación de intervencion (true) o alta (false, por defecto)
  @Input() idintervencion: number = null;
  @Output() calcular: EventEmitter<null> = new EventEmitter<null>();

  constructor() { }
  anteriora2010 = false;
  total: Coste = null;

  get Costes(): Coste[] {
    if (this.resumen) {
      return this.costes ? this.costes.filter(c => c.Seleccionado) : [];
    } else {
      return this.costes; // ? this.costes.filter(c => c.Seleccionado || !this.readonly || !this.resumen) : [];
    }

  }
  ngOnInit() {
    if (this.costes) {
      this.recalcularTotal();
    }

    if (this.modificacion && !!this.idintervencion) {
      const anio = Number(this.idintervencion.toString(10).substr(0, 4));
      this.anteriora2010 = anio < 2010;
    }

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.costes) {
      setTimeout(() => {
        this.recalcularTotal();
      });
    }
  }

  recalcularTotal() {
    
    if (this.costes) {
      this.total = new Coste();
      this.total.Bruto = this.costes
        .filter((coste: Coste) => coste.Seleccionado && !coste.Descuento)
        .reduce((a, b: Coste) => a + b.Bruto, 0);
      this.total.Neto = this.costes
        .filter((coste: Coste) => coste.Seleccionado && !coste.Descuento)
        .reduce((a, b: Coste) => a + b.Neto, 0);
      this.total.IVA = this.costes
        .filter((coste: Coste) => coste.Seleccionado && !coste.Descuento)
        .reduce((a, b: Coste) => a + b.IVA, 0);

      //ya he calculado el total, ahora miramos si hay alguna linea que sea de descuento y lo aplicamos

      this.costes.filter(c => c.Descuento).forEach(c => {
        c.Bruto = -1 * this.total.Bruto * (c.PorcentajeDescuento / 100);
        c.Neto = -1 * this.total.Neto * (c.PorcentajeDescuento / 100);
        c.IVA = -1 * this.total.IVA * (c.PorcentajeDescuento / 100);
      });

      // Vuelvo a sumar pero ahora incluyendo la linea de descuento
      this.total = new Coste();
      this.total.Bruto = this.costes
        .filter((coste: Coste) => coste.Seleccionado )
        .reduce((a, b: Coste) => a + b.Bruto, 0);
      this.total.Neto = this.costes
        .filter((coste: Coste) => coste.Seleccionado)
        .reduce((a, b: Coste) => a + b.Neto, 0);
      this.total.IVA = this.costes
        .filter((coste: Coste) => coste.Seleccionado)
        .reduce((a, b: Coste) => a + b.IVA, 0);




      this.importecostetotal = this.total.Neto;
      this.importecostetotalChange.emit(this.importecostetotal);


    }
  }


}
