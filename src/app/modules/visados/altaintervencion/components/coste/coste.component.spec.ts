import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CosteComponent } from './coste.component';

describe('CosteComponent', () => {
  let component: CosteComponent;
  let fixture: ComponentFixture<CosteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CosteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CosteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
