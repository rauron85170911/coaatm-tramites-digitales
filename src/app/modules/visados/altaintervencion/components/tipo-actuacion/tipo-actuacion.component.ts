
import { CodigoDescripcion, TipoIntervencion } from 'src/app/modules/core/models/models.index';
import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-tipo-actuacion',
  templateUrl: './tipo-actuacion.component.html',
  styleUrls: ['./tipo-actuacion.component.scss']
})
export class TipoActuacionComponent implements OnInit, OnChanges {




  @Input() tipo: CodigoDescripcion = null;
  @Input() tiposIntervencion: TipoIntervencion[] = [];
  @Input() filtro: string = null;

  // tslint:disable-next-line: no-output-on-prefix
  @Output() onSelect: EventEmitter<TipoIntervencion> = new EventEmitter<TipoIntervencion>();

  filtrados: any[] = [];
  isCollapsed = true;
  constructor() { }

  ngOnInit() {
    // this.tiposIntervencion = this.tiposIntervencion.filter(t => t.CodigoTipoActuacion === this.tipo.Codigo);
  }
  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
    if (changes.tiposIntervencion) {
      this.filtrados = changes.tiposIntervencion.currentValue ?
      changes.tiposIntervencion.currentValue.filter(t => {
        return t.CodigoTipoActuacion === this.tipo.Codigo;
      }) :
      [];
    }
    
    if (changes.filtro) {
      this.isCollapsed = changes.filtro.currentValue === '';
    }

    
    // if (!changes.tiposIntervencion.firstChange) {
    //   this.isCollapsed = this.filtrados.length === 0 || changes.filtro.currentValue === '';
    // }

  }

  select($event: TipoIntervencion) {
    this.onSelect.emit($event);
  }
}

