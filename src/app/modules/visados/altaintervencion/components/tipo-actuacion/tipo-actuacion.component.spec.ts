import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoActuacionComponent } from './tipo-actuacion.component';

describe('TipoActuacionComponent', () => {
  let component: TipoActuacionComponent;
  let fixture: ComponentFixture<TipoActuacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoActuacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoActuacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
