import { Filtro } from './../../intervenciones/models/filtro.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {


  constructor() { }
  filtro: Filtro = null;
  ngOnInit() {
  }

  setfiltro($event) {
    this.filtro = {
      Direccion: $event.Direccion,
      IdIntervencion: $event.IdIntervencion,
      EstadoObra: $event.EstadoObra
    };
  }
}
