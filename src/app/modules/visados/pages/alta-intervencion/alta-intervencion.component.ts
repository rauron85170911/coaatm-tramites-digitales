import { PasosService } from './../../../mycommon/services/pasos.service';
import { EventBusService } from './../../../mycommon/services/event-bus.service';
import { AuthenticationService } from './../../../core/services/authentication.service';
import { AltaintervencionModule } from './../../altaintervencion/altaintervencion.module';
import { GuiUtilsService } from 'src/app/modules/mycommon/services/gui-utils.service';
import { TipoIntervencion } from './../../../core/models/tipointervencion.model';
import { AltaIntervencion, Estadisticas } from '../../models/models.index';
import { BackendService } from '../../altaintervencion/services/backend.service';
import { CodigoDescripcion, TipoDocumento } from './../../../core/models/models.index';
import { ErrorsService } from './../../../mycommon/services/errors.service';
import {
  Component,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewChildren,
  AfterContentInit,
  ContentChildren,
  ContentChild,
  AfterViewInit,
  HostListener,
  ElementRef,
  Renderer2
} from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { ValidacionService } from 'src/app/modules/mycommon/services/validacion.service';
import { ErrorValidacion } from 'src/app/modules/mycommon/models/models.index';

@Component({
  selector: 'app-alta-intervencion',
  templateUrl: './alta-intervencion.component.html',
  styleUrls: ['./alta-intervencion.component.scss']
})
export class AltaIntervencionComponent implements OnInit {



  constructor(
    private backendService: BackendService,
    private errorService: ErrorsService,
    private guiUtils: GuiUtilsService,
    private route: ActivatedRoute,
    private validacionService: ValidacionService,
    private authService: AuthenticationService,
    private pasosService: PasosService,
    private eventbus: EventBusService,
    private renderer: Renderer2) { }

  modelo: AltaIntervencion = new AltaIntervencion();
  insertandodetalle = false;
  readonly = false;
  tiposdocumento: TipoDocumento[] = [];

  // true si es un alta de registro, false si es un alta de visado
  ESREGISTRO = false;
  // true si es para una modificación de intervención, false si es un alta
  ESMODIFICACION = false;

  // tipo de trámite que estamos realizando. AE, AR, ME, MR 
  TIPOTRAMITE: string = null;

  erroresvalidacion: any[] = [];
  activo = '1';
  pasos: any[] = [
    {
      indice: 0,
      id: 'paso1',
      indicador: '1',
      titulo: 'Datos de la obra',
      completado: false,
      enabled: true
    },
    {
      indice: 1,
      id: 'paso2',
      indicador: '2',
      titulo: 'Intervinientes',
      completado: false,
      enabled: false
    },
    {
      id: 'paso3',
      indicador: '3',
      titulo: 'Estadísticas',
      completado: false,
      enabled: false
    },
    {
      id: 'paso4',
      indicador: '4',
      titulo: 'Documentación',
      completado: false,
      enabled: false
    },
    {
      id: 'paso5',
      indicador: '5',
      titulo: 'Resumen pago y envío',
      completado: false,
      enabled: false
    }
  ];

  dict: any = {};

  @ViewChild('paso1', { static: true, read: TemplateRef }) paso1: TemplateRef<any>;
  @ViewChild('paso2', { static: true, read: TemplateRef }) paso2: TemplateRef<any>;
  @ViewChild('paso3', { static: true, read: TemplateRef }) paso3: TemplateRef<any>;
  @ViewChild('paso4', { static: true, read: TemplateRef }) paso4: TemplateRef<any>;
  @ViewChild('paso5', { static: true, read: TemplateRef }) paso5: TemplateRef<any>;
  list;

  ngOnInit() {



    // lo primero, decidir si es alta de visado o alta de registro
    // debugger;
    this.ESREGISTRO = this.route.snapshot.parent.url.filter(s => s.path === 'registros').length > 0;

    this.ESMODIFICACION = this.route.snapshot.url.filter(s => s.path === 'modificacionintervencion').length > 0;

    if (this.ESREGISTRO) {
      this.TIPOTRAMITE = this.ESMODIFICACION ? 'MR' : 'AR';
    } else {
      this.TIPOTRAMITE = this.ESMODIFICACION ? 'ME' : 'AE';
    }


    this.guiUtils.TituloPrincipal = this.ESREGISTRO ?
      (this.ESMODIFICACION ? 'Registro de modificación de intervención' : 'Registro de alta de intervención') :
      (this.ESMODIFICACION ? 'Visado de modificación de intervención' : 'Visado de alta de intervención');

    this.eventbus.on('solicitud:onactualizandodetalle', (data) => {
      this.insertandodetalle = data;
    });

    this.pasos.forEach(p => {
      p.plantilla = eval('this.' + p.id);
    });
    this.list = [{ type: this.paso1 }, { type: this.paso2 }];


    // this.validacionService.OnNavegar.subscribe((error: ErrorValidacion) => {
    //   debugger;
    //   this.activo = error.Paso;
    // });


    // si tenemos parámetro en la ruta llamamos al backend para cargar los datos
    const cargadatos: Observable<AltaIntervencion> = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        const idsolicitud = params.get('idsolicitud');
        if (!!idsolicitud) {
          return this.backendService.GetSolicitud(Number(idsolicitud), this.ESREGISTRO, this.ESMODIFICACION);
        } else {
          return this.backendService.GetSolicitudVacia(this.ESREGISTRO);
          //return of(new AltaIntervencion());
        }
      })
    );
    cargadatos.subscribe(solicitud => {
      this.pasosService.ActivarPaso('1');
      this.modelo = solicitud;
      this.cargarTiposDocumento();
      this.pasos.forEach(p => {
        if (p.id === 'paso3') { // estadisticas
          p.enabled = this.modelo &&
            this.modelo.IdSolicitud &&
            this.modelo.TipoIntervencion &&
            this.modelo.TipoIntervencion.MostrarEstadisticas;
        } else {
          p.enabled = p.id === 'paso1' || this.modelo && this.modelo.IdSolicitud;
        }
      });
    });
  }

  cargarTiposDocumento() {
    debugger;
    if (this.modelo.IdSolicitud && this.modelo.TipoIntervencion.Codigo) {
      this.backendService.GetTiposDocumento(this.modelo.IdSolicitud, this.modelo.TipoIntervencion.Codigo, this.ESREGISTRO, this.ESMODIFICACION)
        .subscribe(tipos => { this.tiposdocumento = tipos; }
        );
    }
  }
  onIntervencionChange(tipointervencion: TipoIntervencion) {
    this.modelo.DatosComplementarios.TipoIntervencion = tipointervencion.Codigo;
    this.obtenerparametros(tipointervencion);
    // establecemos el estado del paso de las estadísticas
    this.pasos.filter(p => p.id === 'paso3')[0].enabled = tipointervencion.MostrarEstadisticas;
    if (!tipointervencion.MostrarEstadisticas) {
      // limpiamos las estadísticas
      this.modelo.Estadisticas = new Estadisticas();
    }

    // Recargamos los tipos de documentos
    this.cargarTiposDocumento();
  }
  obtenerparametros(tipoIntervencion: TipoIntervencion) {
    this.modelo.TipoIntervencion = tipoIntervencion;
    this.backendService.GetParametrosDatosComplementarios$(tipoIntervencion.Codigo, this.ESREGISTRO, this.ESMODIFICACION).subscribe(parametros => {
      // debugger;
      this.modelo.DatosComplementarios.Parametros = parametros;
      if (this.modelo.DatosComplementarios && this.modelo.DatosComplementarios.Parametros.length) {
        this.modelo.Costes = [];
      } else {
        this.calcularCostes();
      }
    });
  }

  calcularCostes() {
    if (!this.modelo.TipoIntervencion.Codigo) {
      this.guiUtils.ShowError('Para poder calcular el coste hay que seleccionar un tipo de actuación/intervención');
    } else {
      // miramos todos los datos complementarios a ver si tienen algún valor y es válido
      const sinrrellenar = this.modelo.DatosComplementarios.Parametros.filter(d => !d.Valor || isNaN(Number(d.Valor))).length;
      if (sinrrellenar > 0) {
        this.modelo.Costes = [];
        // this.guiUtils.ShowError('Para poder calcular el coste hay que rellenar todos los datos complementarios');
      } else {
        this.backendService.CalcularCostes(
          this.modelo.TipoIntervencion.Codigo,
          this.modelo.DatosComplementarios,
          this.ESREGISTRO, this.ESMODIFICACION).subscribe(
            costes => {
              this.modelo.Costes = costes;
            }
          );
      }
    }
  }

  verProgreso() {
    this.errorService.DepurarObjeto(this.modelo);
  }
  guardarProgreso() {
    if (this.modelo.IdSolicitud) {
      this.backendService.GuardarSolicitud(this.modelo, this.ESREGISTRO, this.ESMODIFICACION).subscribe(resultado => {
        if (resultado) {
          this.guiUtils.ShowSuccess(`Se ha guardado la solicitud`);
          this.modelo = resultado;
        } else {
          this.guiUtils.ShowError('No se ha podido guardar la solicitud');
        }
      },
        error => {
          this.guiUtils.ShowError(error);
        });
    } else {
      this.backendService.CrearSolicitud(this.modelo, this.ESREGISTRO).subscribe((resultado: AltaIntervencion) => {
        this.guiUtils.ShowSuccess(`Se ha creado la solicitud con el id ${resultado.IdSolicitud}`);
        this.modelo = resultado;
        this.cargarTiposDocumento();
        // this.modelo.IdSolicitud = idSolicitud;
        this.pasos.forEach(p => { p.enabled = true; });
        this.pasos.filter(p => p.id === 'paso3')[0].enabled = this.modelo.TipoIntervencion.MostrarEstadisticas;
      },
        error => {
          this.guiUtils.ShowError(error);
        });
    }
  }

  validar() {
    this.backendService.GuardarSolicitud(this.modelo, this.ESREGISTRO, this.ESMODIFICACION).subscribe(solicitud => {
      if (solicitud) {
        this.backendService.VerificarAltaIntervencion(
          this.modelo.IdSolicitud,
          this.ESREGISTRO,
          this.ESMODIFICACION).subscribe(resultado => {
            if (resultado.Valido) {
              this.guiUtils.ShowSuccess(`No se han detectado errores en la solicitud`);
            } else {
              this.validacionService.Inicializar();
              resultado.Errors.forEach(e => {
                this.validacionService
                  .AddErrorValidacion(e.Descripcion, e.Paso);
              });
            }
          });
      } else {
        this.guiUtils.ShowError('No se ha podido guardar la solicitud, no se realiza la validación');
      }
    },
      error => {
        this.guiUtils.ShowError(error);
      });
  }

}
