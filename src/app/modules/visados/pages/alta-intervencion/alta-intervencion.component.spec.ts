import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AltaIntervencionComponent } from './alta-intervencion.component';

describe('AltaIntervencionComponent', () => {
  let component: AltaIntervencionComponent;
  let fixture: ComponentFixture<AltaIntervencionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AltaIntervencionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AltaIntervencionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
