import { ErrorsService } from './../../../mycommon/services/errors.service';
import { Component, OnInit, ViewChild, ElementRef, TemplateRef } from '@angular/core';
import { CosteComponent } from '../../altaintervencion/components/coste/coste.component';
import { PasosService } from 'src/app/modules/mycommon/services/pasos.service';
import { Observable } from 'rxjs';
import { GuiUtilsService } from 'src/app/modules/mycommon/services/gui-utils.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ValidacionService } from 'src/app/modules/mycommon/services/validacion.service';
import { AuthenticationService } from 'src/app/modules/core/services/authentication.service';
import { switchMap } from 'rxjs/operators';
import { CambioGestionCobroBackendService } from '../../otrostramitesintervencion/services/cambiogestioncobrobackend.service';
import { TipoDocumento } from 'src/app/modules/core/models/models.index';

@Component({
  selector: 'app-cambio-gestion-cobro',
  templateUrl: './cambio-gestion-cobro.component.html',
  styleUrls: ['./cambio-gestion-cobro.component.scss']
})
export class CambioGestionCobroComponent implements OnInit {


  constructor(
    private errorService: ErrorsService,
    private guiUtils: GuiUtilsService,
    private route: ActivatedRoute,
    private validacionService: ValidacionService,
    private authService: AuthenticationService,
    private pasosService: PasosService,
    private backendService: CambioGestionCobroBackendService
  ) { }

  idintervencion: string = null;
  modelo: any = {
    Solicitud: {}
  };
  readonly = false;
  tiposdocumento: TipoDocumento[] = [];
  TIPOTRAMITE = 'GC';
  erroresvalidacion: any[] = [];
  activo = '1';
  pasos: any[] = [
    {
      indice: 0,
      id: 'paso1',
      indicador: '1',
      titulo: 'Datos de la obra',
      completado: false,
      enabled: true
    },
    {
      indice: 1,
      id: 'paso2',
      indicador: '2',
      titulo: 'Documentación',
      completado: false,
      enabled: true
    },
    {
      id: 'paso3',
      indicador: '3',
      titulo: 'Validación y envío',
      completado: false,
      enabled: true
    }
  ];
  dict: any = {};

  @ViewChild('marcadorPasos', null) marcadorPasos: ElementRef;
  @ViewChild('paso1', { static: true, read: TemplateRef }) paso1: TemplateRef<any>;
  @ViewChild('paso2', { static: true, read: TemplateRef }) paso2: TemplateRef<any>;
  @ViewChild('paso3', { static: true, read: TemplateRef }) paso3: TemplateRef<any>;
  @ViewChild('paso4', { static: true, read: TemplateRef }) paso4: TemplateRef<any>;
  @ViewChild(CosteComponent, null) child: CosteComponent;
  list;

  ngOnInit() {
    this.pasos.forEach(p => {
      p.plantilla = eval('this.' + p.id);
    });
    // TODO: Quitar el settimeout
    setTimeout(() => {
      this.pasosService.ActivarPaso('1');
    }, 10);

    const cargadatos: Observable<any> = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        const idsolicitud = params.get('idsolicitud');
        if (!!idsolicitud) {
          return this.backendService.GetSolicitud(Number(idsolicitud));
        }
      })
    );

    cargadatos.subscribe(datos => {
      this.pasosService.ActivarPaso('1');
      this.modelo = datos;

      this.pasos.forEach(p => {
        p.enabled = p.id === 'paso1' || this.modelo && this.modelo.Solicitud;
      });
      this.idintervencion = datos.Solicitud.IdIntervencion;
      this.backendService.GetTiposDocumento(this.modelo.Solicitud.IdSolicitud)
        .subscribe(tipos => { this.tiposdocumento = tipos; }
        );
      // if (this.modelo.Intervencion && this.modelo.Intervencion.TipoIntervencion) {
      //   this.cambiotipoanexo();
      // }
    });

  }

  guardarProgreso() {
    this.backendService.GuardarSolicitud(this.modelo).subscribe(resultado => {
      if (resultado) {
        this.guiUtils.ShowSuccess(`Se ha guardado la solicitud`);
        this.modelo = resultado;
      } else {
        this.guiUtils.ShowError('No se ha podido guardar la solicitud');
      }
    },
      error => {
        this.guiUtils.ShowError(error);
      });


  }

  verificar() {

    this.backendService.GuardarSolicitud(this.modelo).subscribe(solicitud => {
      if (solicitud) {
        this.backendService.Verificar(this.modelo.Solicitud.IdSolicitud).subscribe(resultado => {
          this.validacionService.Inicializar();
          resultado.Errors.forEach(e => {
            this.validacionService
              .AddErrorValidacion(e.Descripcion, e.Paso);
          });
          resultado.Warnings.forEach(e => {
            this.validacionService
              .AddErrorValidacion(e.Descripcion, e.Paso);
          });
          if (resultado.Valido) {
            this.guiUtils.ShowSuccess(`No se han detectado errores de verificación,
                          la solicitud puede ser validada para su posterior envío`);
          }
        });
      } else {
        this.guiUtils.ShowError('No se ha podido guardar la solicitud, no se realiza la verificación');
      }
    },
      error => {
        this.guiUtils.ShowError(error);
      });
  }


  validar() {
    this.backendService.GuardarSolicitud(this.modelo).subscribe(solicitud => {
      if (solicitud) {
        // this.solicitudChange.emit(solicitud);
        this.backendService.Verificar(this.modelo.Solicitud.IdSolicitud).subscribe(resultado => {
          if (resultado.Valido) {
            // debugger;
            this.backendService.Validar(this.modelo.Solicitud.IdSolicitud).subscribe(r => {
              if (r.Ok) {
                // recargamos la solicitud
                this.backendService.GetSolicitud(this.modelo.Solicitud.IdSolicitud).subscribe(s => {
                  this.modelo = s; // solicitudChange.emit(s);
                });
                this.guiUtils.ShowSuccess('La solicitud se ha validado correctamente');
              } else {
                this.guiUtils.ShowError(r.Descripcion);
              }
            });
          } else {
            this.validacionService.Inicializar();
            this.guiUtils.ShowError('La solicitud presenta errores de verificación, no es posible validarla');
            resultado.Errors.forEach(e => {
              this.validacionService.AddErrorValidacion(e.Descripcion, e.Paso);
            });
          }
        });
      } else {
        this.guiUtils.ShowError('No se ha podido guardar la solicitud, no se realiza la validación');
      }

    });


  }

  revertir() {
    this.backendService.Revertir(this.modelo.Solicitud.IdSolicitud).subscribe(r => {
      if (r.Ok) {
        this.guiUtils.ShowSuccess('La solicitud se ha vuelto al estado ELABORACIÓN');
        this.backendService.GetSolicitud(this.modelo.Solicitud.IdSolicitud).subscribe(s => {
          this.modelo = s;
        });
      } else {
        this.guiUtils.ShowError(r.Descripcion);
      }
    });
  }
  verProgreso() {
    this.errorService.DepurarObjeto(this.modelo);
  }

  // para controlar y actuar en consecuencia cuando se cambia el check de Gestión de cobro
  cambioSeleccion() {
    if (this.modelo.Solicitud.GestionCobro) {
      if (!this.modelo.Solicitud.ColegiadoInterviniente.GestionCobro) {
        // si el colegiado no tenía la gestión de cobro y ahora sí, establecemos como obligatorio el tipo de documento TDCT
        this.tiposdocumento.map(td => {
          if (td.Codigo === 'TDCT') {
            td.Obligatorio = true;
          }
        });
      }
    } else {
      this.modelo.Solicitud.Honorarios = null;
      this.tiposdocumento.map(td => {
        if (td.Codigo === 'TDCT') {
          td.Obligatorio = false;
        }
      });
    }
  }



}
