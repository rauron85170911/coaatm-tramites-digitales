import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CambioGestionCobroComponent } from './cambio-gestion-cobro.component';

describe('CambioGestionCobroComponent', () => {
  let component: CambioGestionCobroComponent;
  let fixture: ComponentFixture<CambioGestionCobroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CambioGestionCobroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CambioGestionCobroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
