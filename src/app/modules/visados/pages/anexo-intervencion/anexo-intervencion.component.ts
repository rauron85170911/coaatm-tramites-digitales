import { DatosComplementarios } from './../../models/datoscomplementarios.model';
import { CosteComponent } from './../../altaintervencion/components/coste/coste.component';
import { Component, OnInit, Renderer2, ViewChild, ElementRef, TemplateRef } from '@angular/core';
import { ErrorsService } from 'src/app/modules/mycommon/services/errors.service';
import { GuiUtilsService } from 'src/app/modules/mycommon/services/gui-utils.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ValidacionService } from 'src/app/modules/mycommon/services/validacion.service';
import { AuthenticationService } from 'src/app/modules/core/services/authentication.service';
import { PasosService } from 'src/app/modules/mycommon/services/pasos.service';
import { EventBusService } from 'src/app/modules/mycommon/services/event-bus.service';
import { TipoDocumento } from 'src/app/modules/core/models/models.index';
import { AnexoBackendService } from '../../otrostramitesintervencion/services/anexobackend.service';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-anexo-intervencion',
  templateUrl: './anexo-intervencion.component.html',
  styleUrls: ['./anexo-intervencion.component.scss']
})
export class AnexoIntervencionComponent implements OnInit {

  constructor(
    private backendService: AnexoBackendService,
    private errorService: ErrorsService,
    private guiUtils: GuiUtilsService,
    private route: ActivatedRoute,
    private validacionService: ValidacionService,
    private authService: AuthenticationService,
    private pasosService: PasosService,
    private eventbus: EventBusService,
    private renderer: Renderer2

  ) { }
  modelo: any = {
    Solicitud: {},
    Intervencion: {}
  }; // new RenunciaIntervencion(); // new AltaIntervencion();
  insertandodetalle = false;
  readonly = false;
  tiposdocumento: TipoDocumento[] = [];
  importetotal: number;
  // true si es registro, false si es visado
  ESREGISTRO = false;

  TIPOTRAMITE: string = null;
  erroresvalidacion: any[] = [];
  activo = '1';
  pasos: any[] = [
    {
      indice: 0,
      id: 'paso1',
      indicador: '1',
      titulo: 'Datos de la obra',
      completado: false,
      enabled: true
    },
    {
      indice: 1,
      id: 'paso2',
      indicador: '2',
      titulo: 'Intervinientes',
      completado: false,
      enabled: false
    },
    {
      id: 'paso3',
      indicador: '3',
      titulo: 'Documentación',
      completado: false,
      enabled: false
    },
    {
      id: 'paso4',
      indicador: '4',
      titulo: 'Resumen pago y envío',
      completado: false,
      enabled: false
    }
  ];

  dict: any = {};

  @ViewChild('marcadorPasos', null) marcadorPasos: ElementRef;
  @ViewChild('paso1', { static: true, read: TemplateRef }) paso1: TemplateRef<any>;
  @ViewChild('paso2', { static: true, read: TemplateRef }) paso2: TemplateRef<any>;
  @ViewChild('paso3', { static: true, read: TemplateRef }) paso3: TemplateRef<any>;
  @ViewChild('paso4', { static: true, read: TemplateRef }) paso4: TemplateRef<any>;
  @ViewChild(CosteComponent, null) child: CosteComponent;
  list;
  ngOnInit() {
    this.ESREGISTRO = this.route.snapshot.parent.url.filter(s => s.path === 'registros').length > 0;

    this.TIPOTRAMITE = this.ESREGISTRO ? 'RX' : 'AX';

    this.guiUtils.TituloPrincipal = this.ESREGISTRO ? 'Registro de renuncia de intervención' : 'Visado de renuncia de intervención';

    this.pasos.forEach(p => {
      p.plantilla = eval('this.' + p.id);
    });


    // TODO: Quitar el settimeout
    setTimeout(() => {
      this.pasosService.ActivarPaso('1');
    }, 10);

    const cargadatos: Observable<any> = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        const idsolicitud = params.get('idsolicitud');
        if (!!idsolicitud) {
          return this.backendService.GetSolicitud(Number(idsolicitud), this.ESREGISTRO);
          // } else {
          //   return this.backendService.GetSolicitudVacia(this.ESREGISTRO);
        }
      })
    );

    cargadatos.subscribe(datos => {
      this.pasosService.ActivarPaso('1');
      this.modelo = datos;
      this.pasos.forEach(p => {
        p.enabled = p.id === 'paso1' || this.modelo && this.modelo.Solicitud;
      });
      this.cambiotipoanexo();
    });
  }

  cambiotipoanexo() {
    this.modelo.DatosComplementarios.Interaccion = this.modelo.Solicitud.TipoAnexo === 'Anexo' ? 8 : 10;
    this.cargarTiposDocumento(this.modelo.DatosComplementarios.TipoIntervencion, this.modelo.Solicitud.TipoAnexo);

    this.backendService.CalcularCostes(this.modelo.DatosComplementarios).subscribe(response => {

      this.modelo.Costes[0] = response.GG;
      if (response.Urgente) {
        this.modelo.Costes[1] = response.Urgente;
      }
      this.child.recalcularTotal();
    });

  }

  cargarTiposDocumento(tipointervencion: string, tipoanexo: string) {
    this.backendService.GetTiposDocumento(tipointervencion, tipoanexo)
      .subscribe(tipos => { this.tiposdocumento = tipos; }
      );
  }

  guardarProgreso() {
    this.backendService.GuardarSolicitud(this.modelo, this.ESREGISTRO).subscribe(resultado => {
      if (resultado) {
        this.guiUtils.ShowSuccess(`Se ha guardado la solicitud`);
        this.modelo = resultado;
      } else {
        this.guiUtils.ShowError('No se ha podido guardar la solicitud');
      }
    },
      error => {
        this.guiUtils.ShowError(error);
      });


  }

  verificar() {

    this.backendService.GuardarSolicitud(this.modelo, this.ESREGISTRO).subscribe(solicitud => {
      if (solicitud) {
        this.backendService.VerificarAnexoIntervencion(this.modelo.Solicitud.IdSolicitud, this.ESREGISTRO).subscribe(resultado => {
          this.validacionService.Inicializar();
          resultado.Errors.forEach(e => {
            this.validacionService
              .AddErrorValidacion(e.Descripcion, e.Paso);
          });
          resultado.Warnings.forEach(e => {
            this.validacionService
              .AddErrorValidacion(e.Descripcion, e.Paso);
          });
          if (resultado.Valido) {
            this.guiUtils.ShowSuccess(`No se han detectado errores de verificación,
                          la solicitud puede ser validada para su posterior envío`);
          }
        });
      } else {
        this.guiUtils.ShowError('No se ha podido guardar la solicitud, no se realiza la verificación');
      }
    },
      error => {
        this.guiUtils.ShowError(error);
      });
  }


  validar() {
    if (this.authService.currentUser.Id == this.modelo.Solicitud.IdColegiado) {
      this.validarPorPropietario();
    } else {
      this.validarPorOtroInterviniente();
    }
  }

  private validarPorPropietario() {
    this.backendService.GuardarSolicitud(this.modelo, this.ESREGISTRO).subscribe(solicitud => {
      if (solicitud) {
        this.backendService.VerificarAnexoIntervencion(this.modelo.Solicitud.IdSolicitud, this.ESREGISTRO).subscribe(resultado => {
          if (resultado.Valido) {
            this.backendService.ValidarAnexoIntervencion(
              this.modelo.Solicitud.IdSolicitud, this.ESREGISTRO).subscribe(r => {
                if (r.Ok) {
                  // recargamos la solicitud
                  this.backendService.GetSolicitud(this.modelo.Solicitud.IdSolicitud, this.ESREGISTRO).subscribe(s => {
                    this.modelo = s; // solicitudChange.emit(s);
                  });
                  this.guiUtils.ShowSuccess('La solicitud se ha validado correctamente');
                } else {
                  this.guiUtils.ShowError(r.Descripcion);
                }
              });
          } else {
            this.validacionService.Inicializar();
            this.guiUtils.ShowError('La solicitud presenta errores de verificación, no es posible validarla');
            resultado.Errors.forEach(e => {
              this.validacionService.AddErrorValidacion(e.Descripcion, e.Paso);
            });
          }
        });
      } else {
        this.guiUtils.ShowError('No se ha podido guardar la solicitud, no se realiza la validación');
      }

    });
  }
  private validarPorOtroInterviniente() {
    this.backendService.ValidarAnexoIntervencion(
      this.modelo.Solicitud.IdSolicitud, this.ESREGISTRO).subscribe(r => {
        if (r.Ok) {
          // recargamos la solicitud
          this.backendService.GetSolicitud(this.modelo.Solicitud.IdSolicitud, this.ESREGISTRO).subscribe(s => {
            this.modelo = s; // solicitudChange.emit(s);
          });
          this.guiUtils.ShowSuccess('La solicitud se ha validado correctamente');
        } else {
          this.guiUtils.ShowError(r.Descripcion);
        }
      });
  }


  revertir() {
    this.backendService.RevertirAnexoIntervencion(this.modelo.Solicitud.IdSolicitud, this.ESREGISTRO).subscribe(r => {
      if (r.Ok) {
        this.guiUtils.ShowSuccess('La solicitud se ha vuelto al estado ELABORACIÓN');
        this.backendService.GetSolicitud(this.modelo.Solicitud.IdSolicitud, this.ESREGISTRO).subscribe(s => {
          this.modelo = s;
        });
      } else {
        this.guiUtils.ShowError(r.Descripcion);
      }
    });
  }
  verProgreso() {
    this.errorService.DepurarObjeto(this.modelo);
  }

}
