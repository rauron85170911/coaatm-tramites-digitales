import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnexoIntervencionComponent } from './anexo-intervencion.component';

describe('AnexoComponent', () => {
  let component: AnexoIntervencionComponent;
  let fixture: ComponentFixture<AnexoIntervencionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnexoIntervencionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnexoIntervencionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
