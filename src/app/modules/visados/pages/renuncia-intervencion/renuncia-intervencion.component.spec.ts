import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenunciaIntervencionComponent } from './renuncia-intervencion.component';

describe('RenunciaIntervencionComponent', () => {
  let component: RenunciaIntervencionComponent;
  let fixture: ComponentFixture<RenunciaIntervencionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenunciaIntervencionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenunciaIntervencionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
