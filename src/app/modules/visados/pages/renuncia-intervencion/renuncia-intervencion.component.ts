import { ValidacionService } from './../../../mycommon/services/validacion.service';
import { Component, OnInit, ViewChild, ElementRef, TemplateRef, Renderer2 } from '@angular/core';
import { TipoDocumento } from 'src/app/modules/core/models/models.index';
import { RenunciaBackendService } from '../../otrostramitesintervencion/services/renunciabackend.service';
import { ErrorsService } from 'src/app/modules/mycommon/services/errors.service';
import { GuiUtilsService } from 'src/app/modules/mycommon/services/gui-utils.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { AuthenticationService } from 'src/app/modules/core/services/authentication.service';
import { PasosService } from 'src/app/modules/mycommon/services/pasos.service';
import { EventBusService } from 'src/app/modules/mycommon/services/event-bus.service';
import { RenunciaIntervencion } from '../../models/models.index';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-renuncia-intervencion',
  templateUrl: './renuncia-intervencion.component.html',
  styleUrls: ['./renuncia-intervencion.component.scss']
})
export class RenunciaIntervencionComponent implements OnInit {

  constructor(
    private backendService: RenunciaBackendService,
    private errorService: ErrorsService,
    private guiUtils: GuiUtilsService,
    private route: ActivatedRoute,
    private validacionService: ValidacionService,
    private authService: AuthenticationService,
    private pasosService: PasosService,
    private eventbus: EventBusService,
    private renderer: Renderer2

  ) { }

  intervencion: any = {
    IdIntervencion: 125658545,
    Nombre: 'CERTIFICADOS CON INFORME',
    Localizacion: 'C/ Prueba Nº 1 Esc Bustarviejo,, MADRID',
    Estado: 'ACTIVA',
    PorcentajePendiente: 100
  };
  modelo: any = {
    Solicitud: {},
    Intervencion: {}
  }; // new RenunciaIntervencion(); // new AltaIntervencion();
  insertandodetalle = false;
  readonly = false;
  tiposdocumento: TipoDocumento[] = [];
  importetotal: number;
  // true si es registro, false si es visado
  ESREGISTRO = false;
  // tipo de trámite que estamos realizando. RR, RE
  TIPOTRAMITE: string = null;
  erroresvalidacion: any[] = [];
  activo = '1';
  pasos: any[] = [
    {
      indice: 0,
      id: 'paso1',
      indicador: '1',
      titulo: 'Datos de la obra',
      completado: false,
      enabled: true
    },
    {
      indice: 1,
      id: 'paso2',
      indicador: '2',
      titulo: 'Intervinientes',
      completado: false,
      enabled: false
    },
    {
      id: 'paso3',
      indicador: '3',
      titulo: 'Documentación',
      completado: false,
      enabled: false
    },
    {
      id: 'paso4',
      indicador: '4',
      titulo: 'Resumen pago y envío',
      completado: false,
      enabled: false
    }
  ];

  dict: any = {};

  @ViewChild('marcadorPasos', null) marcadorPasos: ElementRef;
  @ViewChild('paso1', { static: true, read: TemplateRef }) paso1: TemplateRef<any>;
  @ViewChild('paso2', { static: true, read: TemplateRef }) paso2: TemplateRef<any>;
  @ViewChild('paso3', { static: true, read: TemplateRef }) paso3: TemplateRef<any>;
  @ViewChild('paso4', { static: true, read: TemplateRef }) paso4: TemplateRef<any>;
  list;
  ngOnInit() {
    this.ESREGISTRO = this.route.snapshot.parent.url.filter(s => s.path === 'registros').length > 0;
    this.guiUtils.TituloPrincipal = this.ESREGISTRO ? 'Registro de renuncia de intervención' : 'Visado de renuncia de intervención';

    this.TIPOTRAMITE = this.ESREGISTRO ? 'RR' : 'RE';

    this.pasos.forEach(p => {
      p.plantilla = eval('this.' + p.id);
    });


    // TODO: Quitar el settimeout
    setTimeout(() => {
      this.pasosService.ActivarPaso('1');
    }, 10);

    const cargadatos: Observable<any> = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        const idsolicitud = params.get('idsolicitud');
        if (!!idsolicitud) {
          return this.backendService.GetSolicitud(Number(idsolicitud), this.ESREGISTRO);
          // } else {
          //   return this.backendService.GetSolicitudVacia(this.ESREGISTRO);
        }
      })
    );

    cargadatos.subscribe(datos => {
      this.pasosService.ActivarPaso('1');
      this.modelo = datos;
      this.pasos.forEach(p => {
        p.enabled = p.id === 'paso1' || this.modelo && this.modelo.Solicitud;
      });

      if (this.modelo.Intervencion && this.modelo.Intervencion.TipoIntervencion) {
        this.backendService.GetTiposDocumento(this.modelo.Solicitud.IdSolicitud,  this.modelo.Intervencion.TipoIntervencion)
          .subscribe(tipos => { this.tiposdocumento = tipos; }
          );
      }
    });
  }

  guardarProgreso() {
    this.backendService.GuardarSolicitud(this.modelo, this.ESREGISTRO).subscribe(resultado => {
      if (resultado) {
        this.guiUtils.ShowSuccess(`Se ha guardado la solicitud`);
        this.modelo = resultado;
      } else {
        this.guiUtils.ShowError('No se ha podido guardar la solicitud');
      }
    },
      error => {
        this.guiUtils.ShowError(error);
      });


  }

  verificar() {

    this.backendService.GuardarSolicitud(this.modelo, this.ESREGISTRO).subscribe(solicitud => {
      if (solicitud) {
        this.backendService.VerificarRenunciaIntervencion(this.modelo.Solicitud.IdSolicitud, this.ESREGISTRO).subscribe(resultado => {
          this.validacionService.Inicializar();
          resultado.Errors.forEach(e => {
            this.validacionService
              .AddErrorValidacion(e.Descripcion, e.Paso);
          });
          resultado.Warnings.forEach(e => {
            this.validacionService
              .AddErrorValidacion(e.Descripcion, e.Paso);
          });
          if (resultado.Valido) {
            this.guiUtils.ShowSuccess(`No se han detectado errores de verificación,
                          la solicitud puede ser validada para su posterior envío`);
          }
        });
      } else {
        this.guiUtils.ShowError('No se ha podido guardar la solicitud, no se realiza la verificación');
      }
    },
      error => {
        this.guiUtils.ShowError(error);
      });
  }


  validar() {
    this.backendService.GuardarSolicitud(this.modelo, this.ESREGISTRO).subscribe(solicitud => {
      if (solicitud) {
        // this.solicitudChange.emit(solicitud);
        this.backendService.VerificarRenunciaIntervencion(this.modelo.Solicitud.IdSolicitud, this.ESREGISTRO).subscribe(resultado => {
          if (resultado.Valido) {
            // debugger;
            this.backendService.ValidarRenunciaIntervencion(
              this.modelo.Solicitud.IdSolicitud, this.ESREGISTRO).subscribe(r => {
                if (r.Ok) {
                  // recargamos la solicitud
                  this.backendService.GetSolicitud(this.modelo.Solicitud.IdSolicitud, this.ESREGISTRO).subscribe(s => {
                    this.modelo = s; // solicitudChange.emit(s);
                  });
                  this.guiUtils.ShowSuccess('La solicitud se ha validado correctamente');
                } else {
                  this.guiUtils.ShowError(r.Descripcion);
                }
              });
          } else {
            this.validacionService.Inicializar();
            this.guiUtils.ShowError('La solicitud presenta errores de verificación, no es posible validarla');
            resultado.Errors.forEach(e => {
              this.validacionService.AddErrorValidacion(e.Descripcion, e.Paso);
            });
          }
        });
      } else {
        this.guiUtils.ShowError('No se ha podido guardar la solicitud, no se realiza la validación');
      }
    });
  }

  revertir() {
    this.backendService.RevertirRenunciaIntervencion(this.modelo.Solicitud.IdSolicitud, this.ESREGISTRO).subscribe(r => {
      if (r.Ok) {
        this.guiUtils.ShowSuccess('La solicitud se ha vuelto al estado ELABORACIÓN');
        this.backendService.GetSolicitud(this.modelo.Solicitud.IdSolicitud, this.ESREGISTRO).subscribe(s => {
          this.modelo = s;
        });
      } else {
        this.guiUtils.ShowError(r.Descripcion);
      }
    });
  }

  verProgreso() {
    this.errorService.DepurarObjeto(this.modelo);
  }


}
