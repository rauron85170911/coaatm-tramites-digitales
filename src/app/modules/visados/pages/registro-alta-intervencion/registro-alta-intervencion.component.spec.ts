import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroAltaIntervencionComponent } from './registro-alta-intervencion.component';

describe('RegistroAltaIntervencionComponent', () => {
  let component: RegistroAltaIntervencionComponent;
  let fixture: ComponentFixture<RegistroAltaIntervencionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistroAltaIntervencionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroAltaIntervencionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
