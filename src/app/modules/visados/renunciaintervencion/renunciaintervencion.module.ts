import { FormsModule } from '@angular/forms';
import { MyCommonModule } from './../../mycommon/mycommon.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ColegiadosRenunciaComponent } from './components/colegiados-renuncia/colegiados-renuncia.component';
import { AutoresRenunciaComponent } from './components/autores-renuncia/autores-renuncia.component';

@NgModule({
  declarations: [ColegiadosRenunciaComponent, AutoresRenunciaComponent],
  imports: [
    CommonModule,
    MyCommonModule,
    FormsModule
  ],
  exports: [ColegiadosRenunciaComponent, AutoresRenunciaComponent]
})
export class RenunciaintervencionModule { }
