import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-colegiados-renuncia',
  templateUrl: './colegiados-renuncia.component.html',
  styleUrls: ['./colegiados-renuncia.component.scss']
})
export class ColegiadosRenunciaComponent implements OnInit, OnChanges {
  @Input() readonly = false;
  @Input() colegiados: any[] = [];
  @Input() costes: any[] = [];
  @Input() importecostetotal: number = 0;
  constructor() { }
  dataChanged: Subject<string> = new Subject<string>();
  ngOnInit() {
    this.dataChanged.pipe(debounceTime(500), distinctUntilChanged())
      .subscribe(model => {
        this.recalcularImportes();
      });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.importecostetotal) {
      this.recalcularImportes();
    }
  }

  onFilterChange(query: string) {
    console.log(query);
    this.dataChanged.next(query);
  }

  recalcularImportes() {
    this.colegiados.forEach(autor => {
      if (!isNaN(Number(autor.PorcentajeGastosGestion))) {
        autor.ImporteGastosGestion = this.importecostetotal * Number(autor.PorcentajeGastosGestion) / 100;
      }
    });

  }

}
