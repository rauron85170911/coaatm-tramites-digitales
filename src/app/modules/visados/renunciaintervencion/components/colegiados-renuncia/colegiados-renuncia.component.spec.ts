import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColegiadosRenunciaComponent } from './colegiados-renuncia.component';

describe('ColegiadosRenunciaComponent', () => {
  let component: ColegiadosRenunciaComponent;
  let fixture: ComponentFixture<ColegiadosRenunciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColegiadosRenunciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColegiadosRenunciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
