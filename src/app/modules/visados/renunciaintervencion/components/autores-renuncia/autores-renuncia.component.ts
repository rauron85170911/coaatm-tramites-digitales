import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-autores-renuncia',
  templateUrl: './autores-renuncia.component.html',
  styleUrls: ['./autores-renuncia.component.scss']
})
export class AutoresRenunciaComponent implements OnInit, OnChanges {
  
  @Input() readonly = false;
  @Input() autores: any[] = [];
  @Input() costes: any[] = [];
  constructor() { }

  dataChanged: Subject<string> = new Subject<string>();

  @Input() importecostetotal: number = 25.69;
  ngOnInit() {

    this.dataChanged.pipe(debounceTime(500), distinctUntilChanged())
      .subscribe(model => {
        this.recalcularImportes();        
      });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.importecostetotal) {
      this.recalcularImportes();
    }
  }


  onFilterChange(query: string) {
    console.log(query);
    this.dataChanged.next(query);
  }


  recalcularImportes(){
    this.autores.forEach(autor => {
      if (!isNaN(Number(autor.PorcentajeGastosGestion))) {
        autor.ImporteGastosGestion = this.importecostetotal * Number(autor.PorcentajeGastosGestion) / 100;
      }
    });

  }


}
