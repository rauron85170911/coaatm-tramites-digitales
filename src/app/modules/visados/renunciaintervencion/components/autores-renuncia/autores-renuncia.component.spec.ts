import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoresRenunciaComponent } from './autores-renuncia.component';

describe('AutoresRenunciaComponent', () => {
  let component: AutoresRenunciaComponent;
  let fixture: ComponentFixture<AutoresRenunciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoresRenunciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoresRenunciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
