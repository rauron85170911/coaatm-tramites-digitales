import { AltaintervencionModule } from './../altaintervencion/altaintervencion.module';

import { ErrorsService } from './../../mycommon/services/errors.service';
import { environment } from 'src/environments/environment';
import { Ubicacion, Coste, AltaIntervencion, DatoComplementario, Estadisticas } from './../../visados/models/models.index';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { TipoDocumento } from '../../core/models/models.index';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  constructor(private http: HttpClient, private errorService: ErrorsService) { }


  public GetDatosComplementarios$(tipointervencion: string): Observable<DatoComplementario[]> {


    const url = `${environment.apiUrlBase}/altaintervencion/${tipointervencion}/parametros`;
    return this.httpget(url).pipe(
      map(response => {
        return response;
      })
    );
  }


  public GetTiposDocumento(idSolicitud: number, idTipoIntervencion: string): Observable<TipoDocumento[]> {
    const url = `${environment.apiUrlBase}/altaintervencion/${idSolicitud}/${idTipoIntervencion}/tiposdocumento`;
    return this.httpget(url).pipe(
      map(response => {
        return response;
      })
    );
  }


  public CalcularCostes(tipointervencion: string, parametros: DatoComplementario[]): Observable<Coste[]> {
    const url = `${environment.apiUrlBase}/altaintervencion/${tipointervencion}/costes`;

    return this.http.post(url, parametros).pipe(
      map((response: Coste[]) => {
        return response;
      })
    );
  }

  public CrearSolicitud(datos: AltaIntervencion): Observable<AltaIntervencion> {
    const url = `${environment.apiUrlBase}/altaintervencion`;
    return this.http.post(url, datos).pipe(
      map(response => response),
      catchError(error => {
        this.errorService.RegistrarErrorHttp(error);
        return of(null);
      })
    );
  }

  public GuardarSolicitud(datos: AltaIntervencion): Observable<AltaIntervencion> {

    const url = `${environment.apiUrlBase}/altaintervencion/${datos.IdSolicitud}`;
    return this.http.put(url, datos).pipe(
      map((response: AltaIntervencion) => response),
      catchError(error => {
        this.errorService.RegistrarErrorHttp(error);
        return of(null);
      })
    );

  }

  public VerificarAltaIntervencion(idSolicitud: number): Observable<any> {

    const url = `${environment.apiUrlBase}/altaintervencion/${idSolicitud}/verificar`;
    return this.http.get(url).pipe(
      map((response) => {
        return response;
      })
    );
  }

  public ValidarAltaIntervencion(idSolicitud: number, consentimientos: any): Observable<any> {
    const url = `${environment.apiUrlBase}/altaintervencion/${idSolicitud}/validar`;

    return this.http.put(url, consentimientos).pipe(
      map((response) => {
        return response;
      })
    );
  }
  public RevertirAltaIntervencion(idSolicitud: number): Observable<any> {
    const url = `${environment.apiUrlBase}/altaintervencion/${idSolicitud}/revertir`;
    return this.http.put(url, null).pipe(
      map((response) => {
        return response;
      })
    );
  }

  public EnviarSolicitud(idSolicitud: number): Observable<AltaIntervencion> {
    const url = `${environment.apiUrlBase}/altaintervencion/${idSolicitud}/enviar`;
    return this.http.put(url, null).pipe(
      map((response: AltaIntervencion) => response)
    );

  }



  public GetSolicitud(idsolicitud: number): Observable<AltaIntervencion> {
    const url = `${environment.apiUrlBase}/altaintervencion/${idsolicitud}`;
    return this.http.get(url).pipe(
      map((response: AltaIntervencion) => {
        response.DocumentacionAportada = [];
        if (!response.Estadisticas) {
          response.Estadisticas = new Estadisticas();
        }
        return response;
      })
    );
  }


  public handleError(error: any) {
    return of(null);
  }
  private httpget(url: string): Observable<any> {
    // const url = `${environment.apiUrlBase}/maestros/${tabla}`;
    return this.http.get(url);
  }



}
