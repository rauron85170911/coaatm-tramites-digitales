import { GuiUtilsService } from './../../../../mycommon/services/gui-utils.service';
import { BackendTramitesService } from './../../../../tramites/services/backend-tramites.service';
import { ColegiadoInterviniente } from './../../../../core/models/colegiadointerviniente.model';
import { AuthenticationService } from './../../../../core/services/authentication.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-colegiados-validacion',
  templateUrl: './colegiados-validacion.component.html',
  styleUrls: ['./colegiados-validacion.component.scss']
})
export class ColegiadosValidacionComponent implements OnInit {


  @Input() solicitud: any = {};
  // @Input() colegiados: any[] = [];
  @Output() validar: EventEmitter<null> = new EventEmitter();
  @Output() revertir: EventEmitter<null> = new EventEmitter();
  @Output() verificar: EventEmitter<null> = new EventEmitter();
  //@Output() enviar: EventEmitter<null> = new EventEmitter();
  constructor(
    public authService: AuthenticationService,
    private tramitesService: BackendTramitesService,
    private guiUtils: GuiUtilsService
    ) { }

  ngOnInit() {
  }

  get colegiados() {
    if (!!this.solicitud.ColegiadoInterviniente) {
      let c = [];
      c.push(this.solicitud.ColegiadoInterviniente);
      return c;
    } else {
      return this.solicitud.ColegiadosIntervinientes;
    }

  }
  get ColegiadoHaValidado(): boolean {

    const idcolegiadologado = Number(this.authService.currentUser.Id);
    if (!!this.solicitud.ColegiadoInterviniente) {
      // si solo hay un colegiado y nos viene en la propiedad ColegiadoInterviniente
      return this.solicitud.ColegiadoInterviniente.SolicitudValidada;
    } else {
      return this.solicitud.ColegiadosIntervinientes.filter(
        c => c.IdColegiado === idcolegiadologado && c.SolicitudValidada).length !== 0;
    }

  }

  get HanValidadoTodos(): boolean {
    if (!!this.solicitud.ColegiadoInterviniente) {
      // si solo hay un colegiado y nos viene en la propiedad ColegiadoInterviniente
      return this.solicitud.ColegiadoInterviniente.SolicitudValidada;
    } else {
      return this.solicitud.ColegiadosIntervinientes.filter(
        c => !c.SolicitudValidada).length === 0;
    }

  }

  enviar() {
    this.tramitesService.EnviarTramite(this.solicitud.IdSolicitud).subscribe(resultado => {
      if (resultado.Ok){
        this.solicitud.Estado = 'EN';
        this.guiUtils.ShowInfo('La solicitud se envió correctamente');
      } else {
        this.guiUtils.ShowError(resultado.Descripcion);
      }
    });
  }


}
