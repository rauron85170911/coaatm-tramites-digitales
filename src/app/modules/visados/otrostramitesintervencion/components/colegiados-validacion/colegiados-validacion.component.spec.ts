import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColegiadosValidacionComponent } from './colegiados-validacion.component';

describe('ColegiadosValidacionComponent', () => {
  let component: ColegiadosValidacionComponent;
  let fixture: ComponentFixture<ColegiadosValidacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColegiadosValidacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColegiadosValidacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
