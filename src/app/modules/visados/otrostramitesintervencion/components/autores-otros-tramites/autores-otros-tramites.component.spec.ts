import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoresOtrosTramitesComponent } from './autores-otros-tramites.component';

describe('AutoresRenunciaComponent', () => {
  let component: AutoresOtrosTramitesComponent;
  let fixture: ComponentFixture<AutoresOtrosTramitesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoresOtrosTramitesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoresOtrosTramitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
