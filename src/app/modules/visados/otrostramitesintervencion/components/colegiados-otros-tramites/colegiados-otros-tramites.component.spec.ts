import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColegiadosOtrosTramitesComponent } from './colegiados-otros-tramites.component';

describe('ColegiadosRenunciaComponent', () => {
  let component: ColegiadosOtrosTramitesComponent;
  let fixture: ComponentFixture<ColegiadosOtrosTramitesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColegiadosOtrosTramitesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColegiadosOtrosTramitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
