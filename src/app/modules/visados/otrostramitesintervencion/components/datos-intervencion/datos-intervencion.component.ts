import { BackendService } from './../../../intervenciones/services/backend.service';
import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-datos-intervencion',
  templateUrl: './datos-intervencion.component.html',
  styleUrls: ['./datos-intervencion.component.scss']
})
export class DatosIntervencionComponent implements OnInit, OnChanges {

  @Input() intervencion: any = null;
  @Input() idintervencion: any = null;
  constructor(private servicio: BackendService) { }
  // intervencion: any = null;
  ngOnInit() {


  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.idintervencion && changes.idintervencion.currentValue) {
      this.servicio.GetIntervencion(changes.idintervencion.currentValue).subscribe(intervencion => {
        this.intervencion = intervencion;
      });
    }

  }

}
