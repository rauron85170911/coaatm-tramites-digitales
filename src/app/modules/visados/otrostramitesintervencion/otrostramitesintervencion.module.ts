import { FormsModule } from '@angular/forms';
import { MyCommonModule } from '../../mycommon/mycommon.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ColegiadosOtrosTramitesComponent } from './components/colegiados-otros-tramites/colegiados-otros-tramites.component';
import { AutoresOtrosTramitesComponent } from './components/autores-otros-tramites/autores-otros-tramites.component';
import { ColegiadosValidacionComponent } from './components/colegiados-validacion/colegiados-validacion.component';

@NgModule({
  declarations: [ColegiadosOtrosTramitesComponent, AutoresOtrosTramitesComponent, ColegiadosValidacionComponent],
  imports: [
    CommonModule,
    MyCommonModule,
    FormsModule
  ],
  exports: [ColegiadosOtrosTramitesComponent, AutoresOtrosTramitesComponent, ColegiadosValidacionComponent]
})
export class OtrosTramitesIntervencionModule { }
