import { TestBed } from '@angular/core/testing';

import { RenunciaBackendService } from './renunciabackend.service';

describe('BackendService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RenunciaBackendService = TestBed.get(RenunciaBackendService);
    expect(service).toBeTruthy();
  });
});
