import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ErrorsService } from 'src/app/modules/mycommon/services/errors.service';
import { Observable, of } from 'rxjs';
import { TipoDocumento } from 'src/app/modules/core/models/models.index';
import { environment } from 'src/environments/environment';
import { map, catchError } from 'rxjs/operators';
import { DatosComplementarios, Coste } from '../../models/models.index';

@Injectable({
  providedIn: 'root'
})
export class RenunciaBackendService {

  constructor(private http: HttpClient, private errorService: ErrorsService) { }


  public GetSolicitud(idsolicitud: number, registro: boolean): Observable<any> {

    const url = `${environment.apiUrlBase}/renunciaintervencion/${idsolicitud}?registro=${registro}`;
    return this.http.get(url).pipe(
      map((response: any) => {
        
        return response;
      })
    );
  }





  public GetTiposDocumento(idSolicitud: number, idTipoIntervencion: string): Observable<TipoDocumento[]> {
    const url = `${environment.apiUrlBase}/renunciaintervencion/${idSolicitud}/${idTipoIntervencion}/tiposdocumento`;
    return this.httpget(url).pipe(
      map(response => {
        return response;
      })
    );
  }


  public CalcularCostes(tipointervencion: string,
                        datoscomplementarios: DatosComplementarios,
                        registro: boolean): Observable<Coste[]> {
    const url = `${environment.apiUrlBase}/costes/gastosgestion`;

    return this.http.post(url, datoscomplementarios).pipe(
      map((response: Coste[]) => {
        return response;
      })
    );
  }

  public GuardarSolicitud(datos: any, registro: boolean): Observable<any> {

    const url = `${environment.apiUrlBase}/renunciaintervencion/${datos.Solicitud.IdSolicitud}?registro=${registro}`;
    return this.http.put(url, datos).pipe(
      map((response: any) => response),
      catchError(error => {
        this.errorService.RegistrarErrorHttp(error);
        return of(null);
      })
    );

  }

  public VerificarRenunciaIntervencion(idSolicitud: number, registro: boolean): Observable<any> {

    const url = `${environment.apiUrlBase}/renunciaintervencion/${idSolicitud}/verificar?registro=${registro}`;
    return this.http.get(url).pipe(
      map((response) => {
        return response;
      })
    );
  }

  public ValidarRenunciaIntervencion(idSolicitud: number, registro: boolean): Observable<any> {
    const url = `${environment.apiUrlBase}/renunciaintervencion/${idSolicitud}/validar?registro=${registro}`;

    return this.http.put(url, null).pipe(
      map((response) => {
        return response;
      })
    );
  }
  public RevertirRenunciaIntervencion(idSolicitud: number, registro: boolean): Observable<any> {
    const url = `${environment.apiUrlBase}/renunciaintervencion/${idSolicitud}/revertir?registro=${registro}`;
    return this.http.put(url, null).pipe(
      map((response) => {
        return response;
      })
    );
  }

  public EnviarSolicitud(idSolicitud: number): Observable<any> {
    const url = `${environment.apiUrlBase}/tramites/${idSolicitud}/enviar`;
    return this.http.put(url, null).pipe(
      map((response: any) => response)
    );

  }



  

  public GetSolicitudVacia(registro: boolean): Observable<any> {
    const url = `${environment.apiUrlBase}/altaintervencion/nueva?registro=${registro}`;
    return this.http.get(url).pipe(
      map((response: any) => {
        response.IdSolicitud = null;
        response.DocumentacionAportada = [];
        return response;
      })
    );
  }




  public handleError(error: any) {
    return of(null);
  }
  private httpget(url: string): Observable<any> {
    // const url = `${environment.apiUrlBase}/maestros/${tabla}`;
    return this.http.get(url);
  }


}
