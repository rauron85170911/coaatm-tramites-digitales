import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ErrorsService } from 'src/app/modules/mycommon/services/errors.service';
import { Observable, of } from 'rxjs';
import { TipoDocumento } from 'src/app/modules/core/models/models.index';
import { environment } from 'src/environments/environment';
import { map, catchError } from 'rxjs/operators';
import { DatosComplementarios, Coste } from '../../models/models.index';

@Injectable({
  providedIn: 'root'
})
export class CambioGestionCobroBackendService {

  constructor(private http: HttpClient, private errorService: ErrorsService) { }


  public GetSolicitud(idsolicitud: number): Observable<any> {

    
    const url = `${environment.apiUrlBase}/cambiogestioncobro/${idsolicitud}`;
    return this.http.get(url).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  public GetTiposDocumento(idSolicitud: string): Observable<TipoDocumento[]> {
    const url = `${environment.apiUrlBase}/cambiogestioncobro/${idSolicitud}/tiposdocumento/`;
    return this.httpget(url).pipe(
      map(response => {
        return response;
      })
    );
  }

  public GuardarSolicitud(datos: any): Observable<any> {

    const url = `${environment.apiUrlBase}/cambiogestioncobro/${datos.Solicitud.IdSolicitud}`;
    return this.http.put(url, datos).pipe(
      map((response: any) => response),
      catchError(error => {
        this.errorService.RegistrarErrorHttp(error);
        return of(null);
      })
    );

  }

  public Verificar(idSolicitud: number): Observable<any> {

    const url = `${environment.apiUrlBase}/cambiogestioncobro/${idSolicitud}/verificar`;
    return this.http.get(url).pipe(
      map((response) => {
        return response;
      })
    );
  }

  public Validar(idSolicitud: number): Observable<any> {
    const url = `${environment.apiUrlBase}/cambiogestioncobro/${idSolicitud}/validar`;

    return this.http.put(url, null).pipe(
      map((response) => {
        return response;
      })
    );
  }
  public Revertir(idSolicitud: number): Observable<any> {
    const url = `${environment.apiUrlBase}/cambiogestioncobro/${idSolicitud}/revertir`;
    return this.http.put(url, null).pipe(
      map((response) => {
        return response;
      })
    );
  }

  public EnviarSolicitud(idSolicitud: number): Observable<any> {
    const url = `${environment.apiUrlBase}/tramites/${idSolicitud}/enviar`;
    return this.http.put(url, null).pipe(
      map((response: any) => response)
    );

  }

  public handleError(error: any) {
    return of(null);
  }
  private httpget(url: string): Observable<any> {
    // const url = `${environment.apiUrlBase}/maestros/${tabla}`;
    return this.http.get(url);
  }


}
