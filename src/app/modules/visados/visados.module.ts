import { RenunciaIntervencion } from './models/renunciaintervencion.model';
import { FormsModule } from '@angular/forms';
import { CoreModule } from './../core/core.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexComponent } from './pages/index/index.component';
import { VisadosRoutingModule } from './visados-routing.module';
import { AltaintervencionModule } from './altaintervencion/altaintervencion.module';
import { OtrosTramitesIntervencionModule } from './otrostramitesintervencion/otrostramitesintervencion.module';
import { RouterModule } from '@angular/router';
import { AltaIntervencionComponent } from './pages/alta-intervencion/alta-intervencion.component';
import { MyCommonModule } from '../mycommon/mycommon.module';
import { RenunciaIntervencionComponent } from './pages/renuncia-intervencion/renuncia-intervencion.component';
import { DatosIntervencionComponent } from './otrostramitesintervencion/components/datos-intervencion/datos-intervencion.component';
import { HomeComponent } from './pages/home/home.component';
import { IntervencionesModule } from './intervenciones/intervenciones.module';
import { AnexoIntervencionComponent } from './pages/anexo-intervencion/anexo-intervencion.component';
import { CambioGestionCobroComponent } from './pages/cambio-gestion-cobro/cambio-gestion-cobro.component';



@NgModule({
  declarations: [
    IndexComponent,
    AltaIntervencionComponent,
    RenunciaIntervencionComponent,
    DatosIntervencionComponent,
    HomeComponent,
    AnexoIntervencionComponent,
    CambioGestionCobroComponent],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    MyCommonModule,
    VisadosRoutingModule,
    AltaintervencionModule,
    OtrosTramitesIntervencionModule,
    IntervencionesModule,
    CoreModule
  ],
  exports: [IndexComponent]

})
export class VisadosModule { }
