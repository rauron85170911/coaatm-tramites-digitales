import { CambioGestionCobroComponent } from './pages/cambio-gestion-cobro/cambio-gestion-cobro.component';
import { RenunciaIntervencionComponent } from './pages/renuncia-intervencion/renuncia-intervencion.component';
import { AltaIntervencionComponent } from './pages/alta-intervencion/alta-intervencion.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './pages/index/index.component';
import { HomeComponent } from './pages/home/home.component';
// import { IndexComponent as IndexAltaIntervencionComponent} from './altaintervencion/index/index.component';
import { AuthGuard } from 'src/app/guards/auth.guard';
import { AnexoIntervencionComponent } from './pages/anexo-intervencion/anexo-intervencion.component';


const routes: Routes = [
  {
    path: 'visados', component: IndexComponent, canActivate: [AuthGuard], canActivateChild: [AuthGuard],
    children: [
      {
        path: '',
        component: HomeComponent,
        data: {titulo: 'Visados/Registros'}
      },
      {
        path: 'altaintervencion',
        component: AltaIntervencionComponent,
        data: { titulo: 'Nuevo visado de alta de intervención' }
      },
      {
        path: 'altaintervencion/:idsolicitud',
        component: AltaIntervencionComponent,
        data: { titulo: 'Visado de alta de intervención' }
      },
      {
        path: 'renuncia/:idsolicitud',
        component: RenunciaIntervencionComponent,
        data: { titulo: 'Visado de Renuncia de intervención' }
      },
      {
        path: 'modificacionintervencion/:idsolicitud',
        component: AltaIntervencionComponent,
        data: { titulo: 'Modificación de visado de intervención' }
      },
      {
        path: 'anexointervencion/:idsolicitud',
        component: AnexoIntervencionComponent,
        data: { titulo: 'Anexo de intervención' }
      },
      {
        path: 'cambiogc/:idsolicitud',
        component: CambioGestionCobroComponent,
        data: { titulo: 'Cambio de gestión de cobro' }
      }

    ]
  }, {
    path: 'registros', component: IndexComponent, canActivate: [AuthGuard], canActivateChild: [AuthGuard],
    children: [
      {
        path: '',
        component: HomeComponent,
        data: {titulo: 'Visados/Registros'}
        // redirectTo: 'altaintervencion',
        // pathMatch: 'full'
      },
      {
        path: 'altaintervencion',
        component: AltaIntervencionComponent,
        data: { titulo: 'Nuevo registro de alta de intervención' }
      },
      {
        path: 'altaintervencion/:idsolicitud',
        component: AltaIntervencionComponent,
        data: { titulo: 'Registro de alta de intervención' }
      },
      {
        path: 'renuncia/:idsolicitud',
        component: RenunciaIntervencionComponent,
        data: { titulo: 'Registro de Renuncia de intervención' }
      },
      {
        path: 'modificacionintervencion/:idsolicitud',
        component: AltaIntervencionComponent,
        data: { titulo: 'Modificación de registro de intervención' }
      },
      {
        path: 'anexointervencion/:idsolicitud',
        component: AnexoIntervencionComponent,
        data: { titulo: 'Anexo de registro de intervención' }
      }
    ]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VisadosRoutingModule { }
