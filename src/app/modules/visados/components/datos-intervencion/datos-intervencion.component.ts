import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-datos-intervencion',
  templateUrl: './datos-intervencion.component.html',
  styleUrls: ['./datos-intervencion.component.scss']
})
export class DatosIntervencionComponent implements OnInit {

  constructor() { }
  @Input() intervencion: any = null;
  ngOnInit() {
  }

}
