import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosIntervencionComponent } from './datos-intervencion.component';

describe('DatosIntervencionComponent', () => {
  let component: DatosIntervencionComponent;
  let fixture: ComponentFixture<DatosIntervencionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosIntervencionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosIntervencionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
