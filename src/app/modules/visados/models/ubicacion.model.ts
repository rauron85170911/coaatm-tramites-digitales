import { CodigoDescripcion } from './../../core/models/codigodescripcion.model';
export class Ubicacion {
    TipoVia: string;
    Via: string;
    Numero: string;
    Escalera: string;
    Piso: string;
    Puerta: string;
    Provincia: string;
    Municipio: string;
    Localidad: string;
    Urbanizacion: string;
    Distrito: string;
    CodigoPostal: string;
    ReferenciaCatastral: string;
}
