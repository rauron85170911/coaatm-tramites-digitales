import { CodigoDescripcion } from '../../core/models/models.index';
import { Coste } from './coste.model';

export class RenunciaIntervencion {
    FormaPago: string;
    Estado: string;
    Readonly: boolean;
    IdSolicitud: number;
    IdColegiado: number;
    NuevoPorcentajeObra: number;
    Motivo: string;
    Costes: Coste[];
}
