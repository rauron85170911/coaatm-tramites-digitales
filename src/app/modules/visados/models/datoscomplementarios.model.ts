import { DatoComplementario } from './datocomplementario.model';
export class DatosComplementarios {
    constructor() { }

    Parametros: DatoComplementario[] = [];
    Comunicacion = true;
    DiligenciaLibro = false;
    Custodia = false;
    TipoIntervencion: string = null;
    EsRegistro = false;
    Interaccion: number;

}
