import { CodigoDescripcion } from './../../core/models/codigodescripcion.model';
export class Estadisticas {
    Destino: string;
    ResultadoITE: string;
    IncumplimientoITE: string;
    EstudioGeotecnico: boolean;
    NumeroObras: number;
    TipoPromotor: string;
    AlturaSobreRasante: number;
    // ReferenciaCatastral: string;
    NumeroEdificios: number;
    NumeroViviendas: number;
    NumeroVPO: number;
    SuperficieViviendas: number;
    SuperficieGaraje: number;
    SuperficieOtrosUsos: number;
    PlantasSobreRasante: number;
    SuperficieConstruidaSobreRasante: number;
    PlantasBajoRasante: number;
    SuperficieConstruidaBajoRasante: number;
    ControlCalidadExterno: boolean;
    NivelControlCalidad: string;
    Medianerias: string;
    UsoEdificio: string;
    CodigoEstadisticaMaterial?: any;
    IEE: string;
}
