import { Estadisticas } from './estadisticas.model';
import { DatosComplementarios } from './datoscomplementarios.model';
import { CodigoDescripcion, TipoIntervencion, Documento } from 'src/app/modules/core/models/models.index';
import { Ubicacion } from './ubicacion.model';
import { Coste } from './models.index';
export class AltaIntervencion {
    constructor() {
        this.Readonly = false;
        this.TipoActuacion = {
            Codigo: null,
            Descripcion: null
        };
        this.TipoIntervencion = {
            Codigo: null,
            Descripcion: null,
            CodigoTipoActuacion: null,
            LIE: false,
            PrecioLIE: 0,
            EsInformeEvaluacionEdificiosTE: false,
            EsITE: false,
            EstadisticaControlCalidadObligatorio: false,
            EstadisticaMaterialesObligatorio: false,
            MostrarEstadisticas: false,
            AdmiteAutorProyecto: false

        };
        this.Ubicacion = {
            CodigoPostal: '',
            Escalera: '',
            Municipio: '',
            Numero: '',
            Piso: '',
            Provincia: null,
            Puerta: '',
            TipoVia: null,
            Via: '',
            Localidad: '',
            Urbanizacion: '',
            Distrito: '',
            ReferenciaCatastral: ''

        };
        this.DatosComplementarios = new DatosComplementarios();
        this.DocumentacionAportada = [];
        this.Costes = [];
        this.Estadisticas = new Estadisticas();
        this.DeclaracionResponsabilidadSolicitante = false;
    }
    IdIntervencion: number;
    FormaPago: string;
    Estado: string;
    Readonly: boolean;
    IdSolicitud: number;
    IdColegiado: number;
    TipoActuacion: CodigoDescripcion;
    TipoIntervencion: TipoIntervencion;
    Ubicacion: Ubicacion;
    DatosComplementarios: DatosComplementarios;
    DocumentacionAportada: Documento[];
    Costes: Coste[];
    Estadisticas: Estadisticas;
    DeclaracionResponsabilidadSolicitante: boolean;
    DatosModificacion: any = {};
}
