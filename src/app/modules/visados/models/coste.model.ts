export class Coste {
    constructor() { }
    Id: string;
    Concepto: string;
    Bruto: number;
    IVA: number;
    Neto: number;
    Seleccionable: boolean;
    Seleccionado: boolean;
    Descuento: boolean;
    PorcentajeDescuento: number;
}
