import { FormasPagoService } from './../../services/formas-pago.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IfStmt } from '@angular/compiler';

@Component({
  selector: 'app-forma-pago',
  templateUrl: './forma-pago.component.html',
  styleUrls: ['./forma-pago.component.scss']
})
export class FormaPagoComponent implements OnInit {

  @Input() readonly = false;
  @Input() tipotramite = null;
  @Input() idcolegiado = null;
  @Input() formapago: string = null;
  @Output() formapagoChange: EventEmitter<any> = new EventEmitter<any>();

  constructor(private formaspagoservice: FormasPagoService) { }

  FORMAPAGOCARGOCUENTA = 'CU';
  formasPago: any[] = [
    // {
    //   id: 'CU', titulo: 'Cargo en cuenta', habilitado: true,
    //   imagen: './assets/images/forma-pago/ico_euro.svg',
    //   descripcion: `Pago mediante la cuenta registrada en el Colegio para el módulo de visados.`
    // },
    // {
    //   id: 'TJ', titulo: 'Tarjeta', habilitado: true,
    //   imagen: './assets/images/forma-pago/ico_tarjeta.svg',
    //   descripcion: `Accediendo a la pasarela de pago (TPV virtual) desde la opción 'Mis trámites' y pinchando
    //   sobre el icono de la tarjeta bancaria que aparece a la izquierda.` },
    // {
    //   id: 'TR', titulo: 'Transferencia', habilitado: true,
    //   imagen: './assets/images/forma-pago/ico_transferencia.svg',
    //   descripcion: `Mediante transferencia bancaria o ingreso en cuenta, al número de cuenta ES6300491892632213261428
    //   , indicando en el concepto el número de trámite digital y el número de solicitud.` },
    // {
    //   id: 'ME', titulo: 'Metálico', habilitado: true,
    //   imagen: './assets/images/forma-pago/ico_metalico.svg',
    //   descripcion: `Efectuando presencialmente el pago en la Caja del Colegio, indicando también
    //   el número de trámite digital y el número de solicitud.` }
  ];

  get FormasPagoHabilitadas(): any[] {
    return this.formasPago; // .filter(f => f.habilitado);
  }

  // formaPagoSeleccionada: string = null;

  ngOnInit() {
    // leemos las formas de pago del servicio
    this.formaspagoservice.GetFormasPagoHabilitadas(this.idcolegiado, this.tipotramite).subscribe(
      formaspago => {
        this.formasPago = formaspago;
        if (!this.formapago && this.formasPago.filter(f => f.Codigo === this.FORMAPAGOCARGOCUENTA).length > 0) {
          // this.formapago = this.FORMAPAGOCARGOCUENTA;
          this.formapagoChange.emit(this.FORMAPAGOCARGOCUENTA);
        }
      }
    );
  }

}
