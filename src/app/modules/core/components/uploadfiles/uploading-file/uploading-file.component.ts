import { Documento } from './../../../models/models.index';
import { UploadService } from './../../../services/upload.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-uploading-file',
  templateUrl: './uploading-file.component.html',
  styleUrls: ['./uploading-file.component.scss']
})
export class UploadingFileComponent implements OnInit {



  @Input() idgrupo: string;
  @Input() documento: Documento;
  @Output() finished: EventEmitter<Documento> = new EventEmitter<Documento>();
  @Output() finishedwitherror: EventEmitter<Documento> = new EventEmitter<Documento>();
  constructor(private uploadService: UploadService) { }

  estado: any = { status: 'progress', message: '', filePath: '' };
  ngOnInit() {

    this.uploadService.UploadDocumento(this.idgrupo, this.documento).subscribe(estado => {
      this.estado = estado;
      if (this.estado.status === 'finished') {
        
        if (this.estado.message.Ok) {
          setTimeout(() => {
            this.documento.Id = '' + this.estado.message.IdAsociado;
            this.finished.emit(this.documento);
          }, 2000);
        } else {
          this.documento.Id = null; // '' + this.estado.message.IdAsociado;
          this.finished.emit(this.documento);
        }
        // dejamos pasar un segundo para que al menos se vea el progreso
      } else if (this.estado.status === 'error') {
        this.finishedwitherror.emit(this.documento);
      }
    }, (error) => {
      this.finishedwitherror.emit(this.documento);
      // this.estado = { status: 'error', message: error, filePath: this.documento.Nombre };
    });

  }

}
