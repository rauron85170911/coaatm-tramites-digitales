import { environment } from './../../../../../../environments/environment';
import { UploadService } from './../../../services/upload.service';
import { CodigoDescripcion, Documento, TipoDocumento } from '../../../models/models.index';
import { GuiUtilsService } from 'src/app/modules/mycommon/services/gui-utils.service';
import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-uploader',
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.component.scss']
})
export class UploaderComponent implements OnChanges {



  @Input() idgrupo: string = null; //id de grupo (idsolicitud o similar) con el que se relacionan los ficheros que se suban
  @Input() tipo: TipoDocumento; // tipo de fichero que se van a subir
  @Output() startuploading: EventEmitter<Documento> = new EventEmitter<Documento>();
  // @Output() uploaded: EventEmitter<Documento> = new EventEmitter<Documento>();
  constructor(private guiUtils: GuiUtilsService, private uploadService: UploadService) { }
  descripcion: string = null;
  isHovering: boolean;
  files: File[] = [];
  uploading: Documento[] = [];

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.tipo && changes.tipo.currentValue) {
      this.descripcion = this.tipo.Descripcion;
    }
  }

  toggleHover(event: boolean) {
    this.isHovering = event;
  }

  onDrop(files: FileList) {
    this.empezarASubir(files);
  }

  handleFileInput($event: any) {
    this.empezarASubir($event.target.files);
  }

  private empezarASubir(files) {
    const r: RegExp = new RegExp('.*\.pdf$', 'i');
    let avisarformato = false;

    //const docs: Documento[] = [];
    // tslint:disable-next-line: prefer-for-of
    const maxfilesize = 1024 * 1024 * environment.documentacion.maxfilesize;
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < files.length; i++) {
      if (r.test(files[i].name) && files[i].size < maxfilesize) {
        const documento: Documento = {
          Id: null,
          Contenido: files[i],
          Nombre: files[i].name,
          Descripcion: this.descripcion,
          Tipo: this.tipo.Codigo,
          VColegio: false,
          VColegiado: true
        };
        //  this.uploading.push(documento);
        // docs.push(documento);
        this.startuploading.emit(documento);
      } else {
        avisarformato = true;
      }
    }
    if (avisarformato) {
      this.guiUtils.ShowError(`Solo se subirán los archivos
       seleccionados en formato pdf y con un tamaño de menos de ${environment.documentacion.maxfilesize} Mb`);
    }
  }



  // uploadFinished($event: Documento) {
  //   this.uploaded.emit($event);
  // }
}
