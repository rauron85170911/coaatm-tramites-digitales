// import { TipoDocumento } from './../../../../models/tipo-documento';
import { DocumentacionService } from './../../services/documentacion.service';
import { GuiUtilsService } from './../../../mycommon/services/gui-utils.service';
import { FileSaverService } from 'ngx-filesaver';
import { DownloadService } from './../../services/download.service';
import { CodigoDescripcion, TipoDocumento } from 'src/app/modules/core/models/models.index';
import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Documento } from '../../models/models.index';
import { trigger, transition, style, animate } from '@angular/animations';

@Component({
  selector: 'app-documentacion',
  templateUrl: './documentacion.component.html',
  styleUrls: ['./documentacion.component.scss'],
  animations:
    [
      trigger(
        'fadeinout',
        [
          transition(
            ':enter',
            [
              style({ opacity: 0, height: '0px', overflow: 'hidden' }),
              animate('0.3s 10ms ease-in',
                style({ opacity: 1, height: '*' })
              )
            ]
          ),
          transition(
            ':leave',
            [
              style({ opacity: 1, overflow: 'hidden' }),
              animate('0.3s ease-in',
                style({ opacity: 0, height: '0px' })
              )
            ]
          )
        ]
      )
    ]
})
export class DocumentacionComponent implements OnChanges {


  @Input() readonly = false;
  @Input() idgrupo = '12345678';
  @Input() permitirsubir = true;
  @Input() permitirdescargaroriginal = true;
  @Input() permitirdescargarcolegio = false;
  @Input() tipos: TipoDocumento[] = [];
  // @Output() documentosChange: EventEmitter<Documento[]> = new EventEmitter<Documento[]>();


  constructor(
    private documentacionService: DocumentacionService,
    private fileSaverService: FileSaverService,
    private utils: GuiUtilsService) { }



  documentos: Documento[] = [];
  insertando = false;
  tipo: TipoDocumento = {
    Codigo: '0',
    Descripcion: 'Indeterminado',
    Obligatorio: false
  };

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.idgrupo) {
      this.documentacionService.GetDocumentosSolicitud(this.idgrupo).subscribe(
        response => {
          this.documentos = response.Documentos;
          this.readonly = response.ReadOnly;
          this.permitirdescargarcolegio = response.PermitirDescargaColegio;
        }
      );
    }
    

  }

  setInsertando(tipo: TipoDocumento) {
    this.tipo = tipo;
    this.insertando = true;
  }

  uploaded(fichero: Documento) {
    this.documentos.push(fichero);
    // this.documentosChange.emit(this.documentos);
  }



  borrar(documento: Documento) {
    this.documentacionService.DeleteDocumento(this.idgrupo, documento.Tipo, documento.Id).subscribe(borrado => {
      if (borrado) {
        this.documentos = this.documentos.filter(d => d.Id !== documento.Id);
        // this.documentosChange.emit(this.documentos);
        this.utils.ShowInfo(`Se ha borrado el documento correctamente`);
      } else {
        this.utils.ShowError('El documento no se ha borrado');
      }

    });
  }
  descargar(documento: Documento, colegio: boolean) {
    this.documentacionService.DownloadDocumento(this.idgrupo, documento.Tipo, documento.Id, colegio).subscribe(
      blob => {
        if (blob) {
          const sufijo = colegio ? '1' : '0';
          const nombre = `TD${this.idgrupo}${documento.Tipo}${documento.Id}_${sufijo}.pdf`;
          this.fileSaverService.save(blob, nombre);
          this.utils.ShowInfo(`Se ha descargado el documento correctamente`);
        }
      }
    );
  }

  uploadFinished($event) {

  }

  uploadFinishedwithError() {
    setTimeout(() => {
      
      this.documentos = this.documentos.filter(d => !!d.Id);
    }, 5000);
  }

}
