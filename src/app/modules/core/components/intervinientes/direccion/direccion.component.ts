import { TablasMaestrasCacheService } from 'src/app/modules/core/services/tablas-maestras-cache.service';
import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { CodigoDescripcion } from '../../../models/models.index';
import { ControlContainer, NgForm } from '@angular/forms';

@Component({
  selector: 'app-direccion',
  templateUrl: './direccion.component.html',
  styleUrls: ['./direccion.component.scss'],
  viewProviders: [{ provide: ControlContainer, useExisting: NgForm }]
})
export class DireccionComponent implements OnInit, OnChanges {

  @Input() ubicacion: any = null;
  constructor(private tablasMaestrasService: TablasMaestrasCacheService) { }

  tiposVia: CodigoDescripcion[] = null;
  provincias: CodigoDescripcion[];
  municipios: CodigoDescripcion[];

  ngOnInit() {
    this.tablasMaestrasService.TiposVia.subscribe(tiposvia => { this.tiposVia = tiposvia; });
    this.tablasMaestrasService.Provincias.subscribe(provincias => { this.provincias = provincias; });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.ubicacion.currentValue.Provincia) {
      this.cargarMunicipios();
    }
  }

  // ngDoCheck(): void {
  //   this.ubicacionChange.emit(this.ubicacion);
  // }  

  cargarMunicipios() {
    
    const codigoprovincia = this.ubicacion.Provincia.padStart(2, '0');
    this.ubicacion.CodigoPostal = `${codigoprovincia}000`;
    this.tablasMaestrasService.getMuniciposProvincia(this.ubicacion.Provincia)
      .subscribe(municipios => { this.municipios = municipios; });
  }

}
