import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipacionEnSociedadComponent } from './participacion-en-sociedad.component';

describe('ParticipacionEnSociedadComponent', () => {
  let component: ParticipacionEnSociedadComponent;
  let fixture: ComponentFixture<ParticipacionEnSociedadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipacionEnSociedadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipacionEnSociedadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
