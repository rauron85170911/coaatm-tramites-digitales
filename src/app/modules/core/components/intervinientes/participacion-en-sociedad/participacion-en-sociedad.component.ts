import { Component, OnInit, Input, DoCheck } from '@angular/core';
import { ControlContainer, NgForm } from '@angular/forms';

@Component({
  selector: 'app-participacion-en-sociedad',
  templateUrl: './participacion-en-sociedad.component.html',
  styleUrls: ['./participacion-en-sociedad.component.scss'],
  viewProviders: [{ provide: ControlContainer, useExisting: NgForm }]
})
export class ParticipacionEnSociedadComponent implements DoCheck {


  @Input() sociedad: string = null;
  @Input() data: any[] = null;
  @Input() ESMODIFICACION = false;
  constructor() { }
  total = 0;

  ngDoCheck(): void {
    //   .reduce((a, b: Coste) => a + b.Bruto, 0);
    if (this.data) {
      this.total = this.data.reduce((a, b: any) => a + b.PorcentajeParticipacion, 0);
    }

  }

}
