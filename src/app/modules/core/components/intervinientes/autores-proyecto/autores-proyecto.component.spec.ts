import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoresProyectoComponent } from './autores-proyecto.component';

describe('AutoresProyectoComponent', () => {
  let component: AutoresProyectoComponent;
  let fixture: ComponentFixture<AutoresProyectoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoresProyectoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoresProyectoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
