import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { BackendService } from '../../../services/backend.service';
import { GuiUtilsService } from 'src/app/modules/mycommon/services/gui-utils.service';
import { trigger, transition, style, animate } from '@angular/animations';
import { EventBusService } from 'src/app/modules/mycommon/services/event-bus.service';
import { EventData } from 'src/app/modules/mycommon/models/eventdata.model';

@Component({
  selector: 'app-autores-proyecto',
  templateUrl: './autores-proyecto.component.html',
  styleUrls: ['./autores-proyecto.component.scss'],
  animations:
    [
      trigger(
        'fadeinout',
        [
          transition(
            ':enter',
            [
              style({ opacity: 0, height: '0px', overflow: 'hidden' }),
              animate('0.3s 0.4s ease-in',
                style({ opacity: 1, height: '*' })
              )
            ]
          ),
          transition(
            ':leave',
            [
              style({ opacity: 1, overflow: 'hidden' }),
              animate('0.3s ease-in',
                style({ opacity: 0, height: '0px' })
              )
            ]
          )
        ]
      )
    ]
})
export class AutoresProyectoComponent implements OnInit,OnChanges {
  

  @Input() readonly = false;
  @Input() idsolicitud = null;
  @Input() ESREGISTRO = false;

  constructor(private backend: BackendService, private guiUtils: GuiUtilsService, private eventbus: EventBusService) { }

  titulaciones: string[] = ['Arquitecto', 'Arquitecto técnico', 'Ingeniero', 'Ingeniero técnico'];

  // tslint:disable-next-line: variable-name
  private _listando = true;
  set Listando(value: boolean) {
    this._listando = value;
    this._insertando = value ? false : this._insertando;
    this._modificando = value ? false : this._modificando;
    this.eventbus.emit(new EventData('solicitud:onactualizandodetalle', !value));
  }
  get Listando() {
    return this._listando;
  }
  // tslint:disable-next-line: variable-name
  _insertando = false;
  set Insertando(value: boolean) {
    this._insertando = value;
    this._listando = value ? false : this._listando;
    this.eventbus.emit(new EventData('solicitud:onactualizandodetalle', true));
  }
  get Insertando() {
    return this._insertando;
  }

  // tslint:disable-next-line: variable-name
  _modificando = false;
  set Modificando(value: boolean) {
    this._modificando = value;
    this._listando = value ? false : this._listando;
    this.eventbus.emit(new EventData('solicitud:onactualizandodetalle', true));
  }
  get Modificando() {
    return this._modificando;
  }
  intervinientes: any[] = [];
  modelo: any = null;

  ngOnInit(): void {
    this.eventbus.on('solicitud:oncolegiadointervinientemodificado', () => {
      this.cargarIntervinientes();
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.idsolicitud.currentValue) {
      this.cargarIntervinientes();
    }
  }

  private cargarIntervinientes() {
    this.backend.GetAutoresProyecto(this.idsolicitud, this.ESREGISTRO).subscribe(d => {
      this.intervinientes = d;
    });
  }
  empezarModificacion(interviniente) {
    this.backend.GetAutorProyecto(this.idsolicitud, interviniente.Id, this.ESREGISTRO).subscribe(i => {
      this.modelo = i;
      this.Modificando = true;
    });
  }
  empezarInsercion() {
    this.modelo = {
    };
    this.Insertando = true;
  }

  cancelar() {
    this.Listando = true;
  }
  borrar(interviniente: any) {
    this.backend.DeleteAutorProyecto(this.idsolicitud, interviniente.Id, this.ESREGISTRO).subscribe(
      resul => {
        if (resul) {
          this.guiUtils.ShowSuccess('El autor del proyecto se ha eliminado correctamente');
          this.cargarIntervinientes();
        } else {
          this.guiUtils.ShowError('El autor del encargo NO se ha podido eliminar');
        }
      }
    );
  }
  onSubmit() {
    if (this.Modificando) {
      this.backend.UpdateAutorProyecto(this.idsolicitud, this.modelo, this.ESREGISTRO)
        .subscribe(r => {
          if (!!r) {
            this.guiUtils.ShowSuccess('El autor del proyecto se ha actualizado correctamente');
            this.cargarIntervinientes();
            this.Listando = true;
          } else {
            this.guiUtils.ShowError('El autor del proyecto NO se ha actualizado');
          }
        });
    } else if (this.Insertando) {
      this.backend.CreateAutorProyecto(this.idsolicitud, this.modelo, this.ESREGISTRO)
        .subscribe(r => {
          if (!!r) {
            this.guiUtils.ShowSuccess('El autor del proyecto se ha creado correctamente');
            this.cargarIntervinientes();
            this.Listando = true;
          } else {
            this.guiUtils.ShowError('El autor del proyecto NO se ha creado');
          }
        });
    }
  }

}
