import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromotoresAutoresEncargoComponent } from './promotores-autores-encargo.component';

describe('PromotoresAutoresEncargoComponent', () => {
  let component: PromotoresAutoresEncargoComponent;
  let fixture: ComponentFixture<PromotoresAutoresEncargoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromotoresAutoresEncargoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromotoresAutoresEncargoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
