import { FavoritosService } from './../../../services/favoritos.service';
import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';
import { BackendService } from '../../../services/backend.service';
import { GuiUtilsService } from 'src/app/modules/mycommon/services/gui-utils.service';
import { ColegiadoInterviniente, CodigoDescripcionAmpliado } from '../../../models/models.index';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { EventBusService } from 'src/app/modules/mycommon/services/event-bus.service';
import { EventData } from 'src/app/modules/mycommon/models/eventdata.model';

@Component({
  selector: 'app-promotores-autores-encargo',
  templateUrl: './promotores-autores-encargo.component.html',
  styleUrls: ['./promotores-autores-encargo.component.scss'],
  animations:
    [
      trigger(
        'fadeinout',
        [
          transition(
            ':enter',
            [
              style({ opacity: 0, height: '0px', overflow: 'hidden' }),
              animate('0.3s 0.4s ease-in',
                style({ opacity: 1, height: '*' })
              )
            ]
          ),
          transition(
            ':leave',
            [
              style({ opacity: 1, overflow: 'hidden' }),
              animate('0.3s ease-in',
                style({ opacity: 0, height: '0px' })
              )
            ]
          )
        ]
      )
    ]
})
export class PromotoresAutoresEncargoComponent implements OnInit, OnChanges {


  @Input() readonly = null;
  @Input() idsolicitud = null;
  @Input() ESREGISTRO = false;

  constructor(private backend: BackendService, private favoritosService: FavoritosService, private guiUtils: GuiUtilsService, private eventbus: EventBusService) { }

  // tslint:disable-next-line: variable-name
  private _listando = true;

  // tslint:disable-next-line: variable-name
  private _operacion = 'listando'; // listando, editando ó seleccionando

  set Operacion(value: string) {
    this._operacion = value;
  }
  get Operacion() {
    return this._operacion;
  }

  // tslint:disable-next-line: variable-name
  private _nuevo = false;
  set Nuevo(value: boolean) {
    this._nuevo = value;
  }
  get Nuevo() {
    return this._nuevo;
  }


  // set Listando(value: boolean) {
  //   this._listando = value;
  //   this._insertando = value ? false : this._insertando;
  //   this._modificando = value ? false : this._modificando;
  //   this.eventbus.emit(new EventData('solicitud:onactualizandodetalle', !value));
  // }
  // get Listando() {
  //   return this._listando;
  // }
  // // tslint:disable-next-line: variable-name
  // _insertando = false;
  // set Insertando(value: boolean) {
  //   this._insertando = value;
  //   this._listando = value ? false : this._listando;
  //   this.eventbus.emit(new EventData('solicitud:onactualizandodetalle', true));
  // }
  // get Insertando() {
  //   return this._insertando;
  // }

  // // tslint:disable-next-line: variable-name
  // _modificando = false;
  // set Modificando(value: boolean) {
  //   this._modificando = value;
  //   this._listando = value ? false : this._listando;
  //   this.eventbus.emit(new EventData('solicitud:onactualizandodetalle', true));
  // }
  // get Modificando() {
  //   return this._modificando;
  // }

  favoritos: any[] = [];
  intervinientes: any[] = [];
  modelo: any = null;

  // set Actualizando(valor: boolean) {

  // }

  ngOnInit(): void {

    this.favoritosService.AutoresEncargo.subscribe(favoritos => {
      this.favoritos = favoritos;
    });

    this.eventbus.on('solicitud:oncolegiadointervinientemodificado', () => {
      this.cargarIntervinientes();
    });
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.idsolicitud.currentValue) {
      this.cargarIntervinientes();
    }
  }


  empezarModificacion(interviniente) {
    this.backend.GetAutorEncargo(this.idsolicitud, interviniente.Id, this.ESREGISTRO).subscribe(i => {
      this.modelo = i;
      this.Operacion = 'editando';
      this.Nuevo = false;
      //this.Modificando = true;
    });
  }
  empezarInsercion(interviniente) {
    this.modelo = interviniente || {
      Direccion: {}
    };
    this.Operacion = 'editando';
    this.Nuevo = true;

    // this.Insertando = true;
  }
  cancelar() {
    this.Operacion = 'listando';
    //this.Listando = true;
  }
  borrar(interviniente: any) {
    this.backend.DeleteAutorEncargo(this.idsolicitud, interviniente.Id, this.ESREGISTRO).subscribe(
      resul => {
        if (resul.Ok) {
          this.guiUtils.ShowSuccess('El autor/promotor del encargo se ha eliminado correctamente');
          this.cargarIntervinientes();
        } else {
          this.guiUtils.ShowError(resul.Descripcion);
        }
      }
    );
  }
  onSubmit() {
    if (!this.Nuevo) {
      this.backend.UpdateAutorEncargo(this.idsolicitud, this.modelo, this.ESREGISTRO)
        .subscribe(r => {
          if (r.Ok) {
            this.guiUtils.ShowSuccess('El autor/promotor del encargo se ha actualizado correctamente');
            this.cargarIntervinientes();
            // this.Listando = true;
            this.Operacion = 'listando';
          } else {
            this.guiUtils.ShowError(r.Descripcion);
          }
        });
    } else if (this.Nuevo) {
      this.backend.CreateAutorEncargo(this.idsolicitud, this.modelo, this.ESREGISTRO)
        .subscribe(r => {
          if (r.Ok) {
            this.guiUtils.ShowSuccess('El autor/promotor del encargo se ha creado correctamente');
            this.cargarIntervinientes();
            this.Operacion = 'listando';
            // this.Listando = true;
          } else {
            this.guiUtils.ShowError(r.Descripcion);
          }
        });
    }

  }

  private cargarIntervinientes() {
    this.backend.GetAutoresEncargo(this.idsolicitud, this.ESREGISTRO).subscribe(d => {
      this.intervinientes = d;
    });
  }
}
