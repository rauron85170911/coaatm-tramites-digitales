import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ColegiadoInterviniente } from '../../../models/models.index';
import { BackendService } from '../../../services/backend.service';

@Component({
  selector: 'app-localizador-colegiado',
  templateUrl: './localizador-colegiado.component.html',
  styleUrls: ['./localizador-colegiado.component.scss']
})
export class LocalizadorColegiadoComponent implements OnInit {

  @Output() encontrado: EventEmitter<any> = new EventEmitter<any>();
  constructor(private backend: BackendService) { }

  numerocolegiado: string = '2760';
  nif: string = '00015052X';
  mensaje = 'Seleccione un colegiado por Id+Nif';
  colegiado: ColegiadoInterviniente = null;
  ngOnInit() {

  }

  buscar() {

    this.backend.LocalizarColegiado(this.numerocolegiado, this.nif).subscribe(colegiado => {
      this.colegiado = colegiado;
      if (this.colegiado) {
        this.mensaje = null;
        this.encontrado.emit(this.colegiado);
      } else {
        this.mensaje = 'No se han encontrado ningún colegiado que coincida';
      }
    });

  }
}
