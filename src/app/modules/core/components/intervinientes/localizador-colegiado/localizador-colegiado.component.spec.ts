import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalizadorColegiadoComponent } from './localizador-colegiado.component';

describe('LocalizadorColegiadoComponent', () => {
  let component: LocalizadorColegiadoComponent;
  let fixture: ComponentFixture<LocalizadorColegiadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocalizadorColegiadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalizadorColegiadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
