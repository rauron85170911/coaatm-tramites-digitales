import { ActivatedRoute } from '@angular/router';
import {
  animate,
  style,
  transition,
  trigger
  } from '@angular/animations';
import { BackendService } from '../../../services/backend.service';
import { CodigoDescripcion, CodigoDescripcionAmpliado } from './../../../models/models.index';
import { ColegiadoInterviniente } from '../../../models/models.index';
import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges
  } from '@angular/core';
import { EventBusService } from 'src/app/modules/mycommon/services/event-bus.service';
import { EventData } from 'src/app/modules/mycommon/models/eventdata.model';
import { GuiUtilsService } from 'src/app/modules/mycommon/services/gui-utils.service';

@Component({
  selector: 'app-colegiados-intervinientes',
  templateUrl: './colegiados-intervinientes.component.html',
  styleUrls: ['./colegiados-intervinientes.component.scss'],
  animations:
    [
      trigger(
        'fadeinout',
        [
          transition(
            ':enter',
            [
              style({ opacity: 0, height: '0px', overflow: 'hidden' }),
              animate('0.3s 0.4s ease-in',
                style({ opacity: 1, height: '*' })
              )
            ]
          ),
          transition(
            ':leave',
            [
              style({ opacity: 1, overflow: 'hidden' }),
              animate('0.3s ease-in',
                style({ opacity: 0, height: '0px' })
              )
            ]
          )
        ]
      )
    ]
})
export class ColegiadosIntervinientesComponent implements OnInit, OnChanges {

  @Input() readonly = null;
  @Input() idsolicitud = null;
  @Input() ESREGISTRO = false;
  @Input() ESMODIFICACION = false;
  constructor(
    private backend: BackendService,
    private guiUtils: GuiUtilsService,
    private eventbus: EventBusService,
    private route: ActivatedRoute) { }
  // tslint:disable-next-line: variable-name  
  private _listando = true;
  set Listando(value: boolean) {
    this._listando = value;
    this._insertando = value ? false : this._insertando;
    this._modificando = value ? false : this._modificando;
    this.eventbus.emit(new EventData('solicitud:onactualizandodetalle', !value));
  }
  get Listando() {
    return this._listando;
  }
  // tslint:disable-next-line: variable-name
  _insertando = false;
  set Insertando(value: boolean) {
    this._insertando = value;
    this._listando = value ? false : this._listando;
    this.eventbus.emit(new EventData('solicitud:onactualizandodetalle', true));
  }
  get Insertando() {
    return this._insertando;
  }

  // tslint:disable-next-line: variable-name
  _modificando = false;
  set Modificando(value: boolean) {
    this._modificando = value;
    this._listando = value ? false : this._listando;
    this.eventbus.emit(new EventData('solicitud:onactualizandodetalle', true));
  }
  get Modificando() {
    return this._modificando;
  }
  colegiados: ColegiadoInterviniente[] = [];
  modelo: ColegiadoInterviniente = null;
  empresas: CodigoDescripcionAmpliado[] = [];
  ngOnInit() {


  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.idsolicitud.currentValue) {
      this.cargarColegiados();
    }
  }
  private cargarColegiados() {
    // const registro: boolean = this.route.snapshot.parent.url.filter(s => s.path === 'registro').length > 0;
    this.backend.GetColegiadosIntervinientes(this.idsolicitud, this.ESREGISTRO).subscribe(d => {
      this.colegiados = d;
    });
  }

  empezarInsercion(colegiado) {

    // solo es válido para empezar a insertar el colegiado si no está ya dado de alta. Miramos por nif
    if (this.colegiados.filter(c => c.IdColegiado === colegiado.NumeroColegiado).length > 0) {
      this.guiUtils.ShowError('El colegiado ya existe como interviniente de la solicitud');
      return;
    }
    this.modelo = new ColegiadoInterviniente();
    this.modelo.Nif = colegiado.Nif;
    this.modelo.Nombre = colegiado.Nombre;
    this.modelo.IdColegiado = colegiado.NumeroColegiado;
    this.modelo.Sociedad = colegiado.Sociedad;
    this.modelo.PorcentajeObra = 0;
    this.modelo.PorcentajeGastosGestion = 0;
    this.Insertando = true;
    this.backend.GetEmpresasPagoColegiado(colegiado.NumeroColegiado).subscribe(empresas => {
      this.empresas = empresas;
      if (this.empresas.length < 1) {
        this.modelo.Asalariado = false;
      }
    });

    if (this.modelo.Sociedad) {
      this.backend.GetParticipaciones(this.idsolicitud, this.modelo.IdColegiado, this.ESREGISTRO).subscribe(participaciones => {
        this.modelo.Participaciones = participaciones;

      });
    }

  }
  empezarModificacion(colegiado) {
    this.modelo = colegiado;
    this.Modificando = true;
    this.backend.GetEmpresasPagoColegiado(colegiado.IdColegiado).subscribe(empresas => {
      this.empresas = empresas;
      if (this.empresas.length < 1) {
        this.modelo.Asalariado = false;
      }
    });

    if (this.modelo.Sociedad) {
      this.backend.GetParticipaciones(this.idsolicitud, this.modelo.IdColegiado, this.ESREGISTRO).subscribe(participaciones => {
        this.modelo.Participaciones = participaciones;

      });
    }

  }

  cancelar() {
    this.Listando = true;
  }
  borrar(colegiado: ColegiadoInterviniente) {
    this.backend.BorrarColegiadoInterviniente(this.idsolicitud, colegiado.IdColegiado, this.ESREGISTRO).subscribe(response => {
      if (response) {
        this.guiUtils.ShowSuccess('Se eliminó el colegiado como interviniente');
        this.cargarColegiados();
      } else {
        this.guiUtils.ShowError('No se pudo eliminar el colegiado como colegiado interviniente');
      }
    });
  }

  seleccionaridasalariado() {
    this.modelo.IdAsalariado = this.empresas.filter(e => e.Codigo === this.modelo.IdEmpresaPago)[0].CodigoGrupo;
  }

  onSubmit() {
    // llamamos al backend
    this.backend.ActualizarColegiadoInterviniente(this.idsolicitud, this.modelo, this.ESREGISTRO).subscribe(response => {
      this.guiUtils.ShowSuccess('El colegiado se actualizó correctamente');
      this.eventbus.emit(new EventData('solicitud:oncolegiadointervinientemodificado', response));
      this.cargarColegiados();
      this.Listando = true;
    });
  }


}
