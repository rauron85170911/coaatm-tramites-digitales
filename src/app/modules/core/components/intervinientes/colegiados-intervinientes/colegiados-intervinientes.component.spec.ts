import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColegiadosIntervinientesComponent } from './colegiados-intervinientes.component';

describe('ColegiadosIntervinientesComponent', () => {
  let component: ColegiadosIntervinientesComponent;
  let fixture: ComponentFixture<ColegiadosIntervinientesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColegiadosIntervinientesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColegiadosIntervinientesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
