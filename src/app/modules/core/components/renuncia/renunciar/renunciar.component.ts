import { Router } from '@angular/router';
import { GuiUtilsService } from './../../../../mycommon/services/gui-utils.service';

import { Component, OnInit } from '@angular/core';
import { BackendService } from '../../../services/backend.service';

@Component({
  selector: 'app-renunciar',
  templateUrl: './renunciar.component.html',
  styleUrls: ['./renunciar.component.scss']
})
export class RenunciarComponent implements OnInit {

  constructor(private backend: BackendService, private guiutils: GuiUtilsService, private route: Router) { }

  intervencion: number = null;
  registro = false;
  ngOnInit() {
  }


  renunciar() {
    // this.backend.RenunciarIntervencion(this.intervencion, this.registro).subscribe((resultado: any) => {
    //   if (!resultado.Ok) {
    //     this.guiutils.ShowError(resultado.Descripcion);
    //   } else {
    //     // se ha creado bien la solicitud de renuncia y redirigimos a la página correspondiente para su modificación
    //     if (this.registro) {
    //       this.route.navigate(['registros', 'renuncia', resultado.IdAsociado]);
    //     } else {
    //       this.route.navigate(['visados', 'renuncia', resultado.IdAsociado]);
    //     }
    //   }
    // });
  }
}
