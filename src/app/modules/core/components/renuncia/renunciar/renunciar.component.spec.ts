import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenunciarComponent } from './renunciar.component';

describe('RenunciarComponent', () => {
  let component: RenunciarComponent;
  let fixture: ComponentFixture<RenunciarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenunciarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenunciarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
