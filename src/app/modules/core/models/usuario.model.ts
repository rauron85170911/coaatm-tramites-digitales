export interface Usuario {
    Id: number;
    Username: string;
    Perfiles: Array<string>;
    Email: string;
}
