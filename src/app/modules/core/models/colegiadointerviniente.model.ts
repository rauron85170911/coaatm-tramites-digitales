
export class ColegiadoInterviniente {
    constructor() { }

    IdColegiado: number;
    Nif: string;
    Nombre: string;
    Asalariado: boolean;
    IdEmpresaPago?: any;
    IdAsalariado?: any;
    GestionCobro: boolean;
    Honorarios: number;
    PorcentajeObra: number;
    PorcentajeObraAnterior: number;
    PorcentajeGastosGestion: number;
    Sociedad: boolean;
    EsColegiadoCreador: boolean;
    Participaciones: any[];
    SeguroAceptado: boolean;
    InformeEvaluacionEdificiosAceptado: boolean;
    VisadoAceptado: boolean;
    SolicitudValidada: boolean;
    FechaValidacion: Date;
}
