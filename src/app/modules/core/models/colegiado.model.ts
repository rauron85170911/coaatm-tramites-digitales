export interface Colegiado {
    FechaAlta: Date;
    Situacion: string;
    NumeroColegiado: string;
    Nombre: string;
    Nif: string;
    Sociedad: boolean;
}
