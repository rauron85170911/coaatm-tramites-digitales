export class Documento {
    constructor() { }

    Id: string;
    Nombre: string;
    Descripcion: string;
    Tipo: string;
    Contenido: any;
    VColegiado: boolean;
    VColegio: boolean;
}
