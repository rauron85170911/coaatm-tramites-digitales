import { CodigoDescripcion } from './codigodescripcion.model';

export class TipoDocumento extends CodigoDescripcion {
    Obligatorio: boolean;
}
