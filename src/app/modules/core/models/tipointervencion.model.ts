export interface TipoIntervencion {
    Codigo: string;
    Descripcion: string;
    CodigoTipoActuacion: string;
    LIE: boolean;
    PrecioLIE: number;
    MostrarEstadisticas: boolean;
    EstadisticaMaterialesObligatorio: boolean;
    EstadisticaControlCalidadObligatorio: boolean;
    EsITE: boolean;
    EsInformeEvaluacionEdificiosTE: boolean;
    AdmiteAutorProyecto: boolean;
}
