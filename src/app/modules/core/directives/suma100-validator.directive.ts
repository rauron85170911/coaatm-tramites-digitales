import { Directive } from '@angular/core';
import { Validator, AbstractControl, ValidationErrors, FormGroup, NG_VALIDATORS } from '@angular/forms';

@Directive({
  selector: '[appSuma100Validator]',
  providers: [{ provide: NG_VALIDATORS, useExisting: Suma100ValidatorDirective, multi: true }]
})
export class Suma100ValidatorDirective implements Validator {
  validate(form: FormGroup): ValidationErrors {
    const claves = Object.keys(form.controls);
    let suma = 0;
    
    claves.forEach(k => {
      suma += form.get(k).value;
    });
    
    const error = {
      suma100: {
        message: 'LA suma debe ser 100'
      }
    };

    return suma === 100 ? null : error;

  }
  registerOnValidatorChange?(fn: () => void): void {
    throw new Error('Method not implemented.');
  }

  constructor() { }

}
