
import { Directive, Renderer2, ElementRef, Input, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';

@Directive({
  selector: '[appOcultarSiNoAdherido]'
})
export class OcultarSiNoAdheridoDirective implements OnInit {
  

  @Input('appOcultarSiNoAdherido') ocultarSiNoAdherido: boolean;
  constructor(private el: ElementRef, private renderer: Renderer2, private authService: AuthenticationService) {

  }

  ngOnInit(): void {
    if (this.ocultarSiNoAdherido && !this.authService.UsuarioEstaAdherido) {
      const capa = this.renderer.createElement('div');
      this.renderer.addClass(capa, 'card-body');
      const parrafo = this.renderer.createElement('p');
      const texto = this.renderer.createText(`Funcionalidad NO DISPONIBLE. El Contrato de adhesión no se encuentra vigente`);
      this.renderer.appendChild(parrafo, texto);
      this.renderer.appendChild(capa, parrafo);

      this.renderer.appendChild(this.el.nativeElement.parentElement, capa);

      // elimino el contenido
      this.renderer.removeChild(this.el.nativeElement.parentElement, this.el.nativeElement);
      // añado una capa con el mensaje correspondiente

    }
  }

}
