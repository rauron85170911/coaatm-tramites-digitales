
import { Directive, Renderer2, ElementRef, Input, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';

@Directive({
  selector: '[appOcultarSiSuspendido]'
})
export class OcultarSiSuspendidoDirective implements OnInit {


  @Input('appOcultarSiSuspendido') ocultarSiSuspendido: boolean;
  constructor(private el: ElementRef, private renderer: Renderer2, private authService: AuthenticationService) {

  }

  ngOnInit(): void {
    if (this.ocultarSiSuspendido && this.authService.UsuarioEstaSuspendido) {

      const capa = this.renderer.createElement('div');
      this.renderer.addClass(capa, 'card-body');
      const parrafo = this.renderer.createElement('p');
      const texto = this.renderer.createText(`Este trámite digital se haya bloqueado,
       al  tener Ud. limitado el pago mediante cargo en cuenta colegial,
        en virtud de acuerdo de junta de gobierno de fecha 17/09/2013.
         El trámite podrá realizarse presencialmente abonando su importe en efectivo
          o mediante tarjeta bancaria. Para más información consulte nuestra web o contacte con el tf 91.701.4500`);
      this.renderer.appendChild(parrafo, texto);
      this.renderer.appendChild(capa, parrafo);

      this.renderer.appendChild(this.el.nativeElement.parentElement, capa);

      // elimino el contenido
      this.renderer.removeChild(this.el.nativeElement.parentElement, this.el.nativeElement);
      // añado una capa con el mensaje correspondiente

    }
  }

}
