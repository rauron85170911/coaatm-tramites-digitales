import { Directive, ElementRef, Renderer2, OnInit, Input } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';

@Directive({
  selector: '[appRestringirAcceso]'
})
export class RestringirAccesoDirective implements OnInit {

  @Input('suspendidos') restringirASuspendidos: boolean = true;
  @Input('noadheridos') restringirANoAdheridos: boolean = true;
  @Input('nosociedad') restringirANoSociedad: boolean = false;
  @Input('sociedad') restringirASociedad: boolean = false;
  constructor(private el: ElementRef, private renderer: Renderer2, private authService: AuthenticationService) {

  }

  ngOnInit(): void {

    let restringir = false;
    let mensaje: string = null;
    if (this.restringirANoAdheridos && !this.authService.UsuarioEstaAdherido) {
      restringir = true;
      mensaje = `Funcionalidad NO DISPONIBLE. El Contrato de adhesión no se encuentra vigente`;
    } else if (this.restringirASuspendidos && this.authService.UsuarioEstaSuspendido) {
      restringir = true;
      mensaje = `Este trámite digital se haya bloqueado,
      al  tener Ud. limitado el pago mediante cargo en cuenta colegial,
       en virtud de acuerdo de junta de gobierno de fecha 17/09/2013.
        El trámite podrá realizarse presencialmente abonando su importe en efectivo
         o mediante tarjeta bancaria. Para más información consulte nuestra web o contacte con el tf 91.701.4500`;
    } else if (this.restringirANoSociedad && !this.authService.UsuarioEsSociedadColegiada) {
      restringir = true;
      mensaje = 'Este trámite solo está disponible para Sociedades Profesionales';
    } else if (this.restringirASociedad && this.authService.UsuarioEsSociedadColegiada) {
      restringir = true;
      mensaje = 'Este trámite solo está disponible para Colegiados que no sean una Sociedad Profesional';
    }
    if (restringir) {
      // añado una capa con el mensaje correspondiente
      const capa = this.renderer.createElement('div');
      this.renderer.addClass(capa, 'card-body');
      const parrafo = this.renderer.createElement('div');
      this.renderer.addClass(parrafo, 'alert');
      this.renderer.addClass(parrafo, 'alert-warning');
      const texto = this.renderer.createText(mensaje);
      this.renderer.appendChild(parrafo, texto);
      this.renderer.appendChild(capa, parrafo);

      this.renderer.appendChild(this.el.nativeElement.parentElement, capa);

      // elimino el contenido
      this.renderer.removeChild(this.el.nativeElement.parentElement, this.el.nativeElement);

    }


  }
}

