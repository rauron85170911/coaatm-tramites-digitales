import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MyCommonModule } from './../mycommon/mycommon.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OcultarSiNoAdheridoDirective } from './directives/ocultar-si-no-adherido.directive';
import { OcultarSiSuspendidoDirective } from './directives/ocultar-si-suspendido.directive';
import { RestringirAccesoDirective } from './directives/restringir-acceso.directive';
import { DireccionPipe } from './pipes/direccion.pipe';
import { DocumentacionComponent } from './components/documentacion/documentacion.component';
import { UploaderComponent } from './components/uploadfiles/uploader/uploader.component';
import { UploadingFileComponent } from './components/uploadfiles/uploading-file/uploading-file.component';
import { ColegiadosIntervinientesComponent }from './components/intervinientes/colegiados-intervinientes/colegiados-intervinientes.component';
import { LocalizadorColegiadoComponent } from './components/intervinientes/localizador-colegiado/localizador-colegiado.component';
import { ParticipacionEnSociedadComponent } from './components/intervinientes/participacion-en-sociedad/participacion-en-sociedad.component';
import { Suma100ValidatorDirective } from './directives/suma100-validator.directive';
import { PromotoresAutoresEncargoComponent } from './components/intervinientes/promotores-autores-encargo/promotores-autores-encargo.component';
import { DireccionComponent } from './components/intervinientes/direccion/direccion.component';
import { AutoresProyectoComponent } from './components/intervinientes/autores-proyecto/autores-proyecto.component';
import { RenunciarComponent } from './components/renuncia/renunciar/renunciar.component';
import { TipoIntervencionPipe } from './pipes/tipo-intervencion.pipe';
import { EstadoIntervencionPipe } from './pipes/estado-intervencion.pipe';
import { EstadoTramiteDirective } from './directives/estado-tramite.directive';
import { EstadoTramitePipe } from './pipes/estado-tramite.pipe';
import { TipoTramitePipe } from './pipes/tipo-tramite.pipe';
import { FormaPagoComponent } from './components/forma-pago/forma-pago.component';
import { TipoDocumentoPipe } from './pipes/tipo-documento.pipe';

@NgModule({
  declarations: [OcultarSiNoAdheridoDirective,
    OcultarSiSuspendidoDirective,
    RestringirAccesoDirective,
    DireccionPipe,
    DocumentacionComponent,
    UploaderComponent,
    UploadingFileComponent,
    ColegiadosIntervinientesComponent,
    LocalizadorColegiadoComponent,
    ParticipacionEnSociedadComponent,
    Suma100ValidatorDirective,
    PromotoresAutoresEncargoComponent,
    DireccionComponent,
    AutoresProyectoComponent,
    RenunciarComponent,
    TipoIntervencionPipe,
    EstadoIntervencionPipe,
    EstadoTramiteDirective,
    EstadoTramitePipe,
    TipoTramitePipe,
    FormaPagoComponent,
    TipoDocumentoPipe],
  imports: [
    CommonModule, FormsModule, MyCommonModule, NgbModule
  ],
  exports: [OcultarSiNoAdheridoDirective,
    OcultarSiSuspendidoDirective,
    RestringirAccesoDirective,
    DireccionPipe,
    DocumentacionComponent,
    ColegiadosIntervinientesComponent,
    PromotoresAutoresEncargoComponent,
    AutoresProyectoComponent,
    RenunciarComponent,
    TipoIntervencionPipe,
    EstadoIntervencionPipe,
    EstadoTramiteDirective,
    EstadoTramitePipe,
    TipoTramitePipe,
    FormaPagoComponent,
    TipoDocumentoPipe],
    providers: [EstadoTramitePipe]
})
export class CoreModule { }
