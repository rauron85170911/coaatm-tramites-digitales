import { CodigoDescripcion } from './../models/codigodescripcion.model';
import { Pipe, PipeTransform } from '@angular/core';
import { Observable, of } from 'rxjs';
import { TablasMaestrasCacheService } from '../services/tablas-maestras-cache.service';
import { map } from 'rxjs/operators';

@Pipe({
  name: 'estadoIntervencion'
})
export class EstadoIntervencionPipe implements PipeTransform {

  constructor(private tablasMaestrasService: TablasMaestrasCacheService) { }

  transform(value: any, args?: any): Observable<any> {

    return this.tablasMaestrasService.EstadosIntervencion.pipe(
      map((estados: CodigoDescripcion[]) => {
        const estado = estados.filter(e => e.Codigo === value)[0];
        return estado ? estado.Descripcion : value;
      })
    );

    return of(value);
  }

}
