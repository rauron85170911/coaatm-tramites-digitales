import { mergeMap, map } from 'rxjs/operators';
import { Pipe, PipeTransform } from '@angular/core';
import { forkJoin, Observable, of } from 'rxjs';
import { TablasMaestrasCacheService } from '../services/tablas-maestras-cache.service';

@Pipe({
  name: 'direccion'
})
export class DireccionPipe implements PipeTransform {

  constructor(private tablasMaestrasService: TablasMaestrasCacheService) { }
  transform(value: any, args?: any): Observable<string> {
    if (!value) { return of(''); }
    return forkJoin(this.tablasMaestrasService.TiposVia,
      this.tablasMaestrasService.Provincias).pipe(
        map(results => {
          const tipovia = results[0].filter(t => t.Codigo === value.TipoVia)[0];
          const dtipovia = tipovia ? tipovia.Descripcion : '';
          const provincia = results[1].filter(p => p.Codigo === value.Provincia)[0];
          const dprovincia = provincia ? provincia.Descripcion : '';

          return `${dtipovia || ''} ${value.Via || ''}
                 ${value.Numero || ''}, ${value.Piso || ''}${value.Puerta || ''} ${value.Localidad || ''}(${dprovincia})`;
        }));

  }

}
