import { Pipe, PipeTransform } from '@angular/core';
import { TablasMaestrasCacheService } from '../services/tablas-maestras-cache.service';
import { Observable, of, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';

@Pipe({
  name: 'tipoIntervencion'
})
export class TipoIntervencionPipe implements PipeTransform {
  constructor(private tablasMaestrasService: TablasMaestrasCacheService) { }
  transform(value: any, args?: any): Observable<string> {
    if (!value) { return of(''); }

    return forkJoin(this.tablasMaestrasService.TiposIntervencion,
      this.tablasMaestrasService.TiposActuacion).pipe(
        map(results => {
          const tipointervencion = results[0].filter(t => t.Codigo === value)[0];
          let tipoactuacion = null;
          if (tipointervencion) {
            tipoactuacion = results[1].filter(t => t.Codigo === tipointervencion.CodigoTipoActuacion)[0];
          }
          return tipoactuacion ? `${tipoactuacion.Descripcion}` : '';
        }));


    return this.tablasMaestrasService.TiposIntervencion.pipe(
      map((tipos: any[]) => {

        return tipos.filter(t => t.Codigo === value)[0].Descripcion;
      }));
  }

}
