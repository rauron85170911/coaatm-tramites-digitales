import { Pipe, PipeTransform } from '@angular/core';
import { TablasMaestrasCacheService } from '../../core/services/tablas-maestras-cache.service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Pipe({
  name: 'estadoTramite'
})
export class EstadoTramitePipe implements PipeTransform {

  constructor(private tablasMaestrasService: TablasMaestrasCacheService) { }
  transform(value: any, args?: any): Observable<string> {
    if (!value) { return of(''); }
    return this.tablasMaestrasService.EstadosTramite.pipe(
      map(tipos => {
        // return tipos.filter(t => t.Codigo === value)[0].Descripcion;
        const descripcion = tipos.filter(t => t.Codigo === value)[0].Descripcion;
        return descripcion || value; //`${descripcion}`;
      })
    );
  }

}
