import { mergeMap, map, tap } from 'rxjs/operators';
import { Pipe, PipeTransform } from '@angular/core';
import { forkJoin, Observable, of } from 'rxjs';
import { TablasMaestrasCacheService } from '../../core/services/tablas-maestras-cache.service';

@Pipe({
  name: 'tipoTramite'
})
export class TipoTramitePipe implements PipeTransform {

  constructor(private tablasMaestrasService: TablasMaestrasCacheService) { }
  transform(value: any, args?: any): Observable<string> {
    if (!value) { return of(''); }
    return this.tablasMaestrasService.TiposTramite.pipe(
      map(tipos => {
        // return tipos.filter(t => t.Codigo === value)[0].Descripcion;
        const descripcion = tipos.filter(t => t.Codigo === value)[0].Descripcion;
        return descripcion || value;
        // return `(${value}) ${descripcion}`;
      })
    );
  }

}
