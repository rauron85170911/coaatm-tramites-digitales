import { Pipe, PipeTransform } from '@angular/core';
import { TablasMaestrasCacheService } from '../services/tablas-maestras-cache.service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Pipe({
  name: 'tipoDocumento'
})
export class TipoDocumentoPipe implements PipeTransform {

  constructor(private tablasMaestrasService: TablasMaestrasCacheService) { }
  transform(value: any, args?: any): Observable<string> {
    if (!value) { return of('empty'); }
    return this.tablasMaestrasService.TiposDocumento.pipe(
      map(tipos => {
        // return tipos.filter(t => t.Codigo === value)[0].Descripcion;
        const item = tipos.filter(t => t.Codigo === value)[0];
        const descripcion = !!item ? item.Descripcion : value;
        return descripcion || value;
        // return `(${value}) ${descripcion}`;
      })
    );
  }
}
