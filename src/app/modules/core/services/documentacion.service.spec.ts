import { TestBed } from '@angular/core/testing';

import { DocumentacionService } from './documentacion.service';

describe('DocumentacionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DocumentacionService = TestBed.get(DocumentacionService);
    expect(service).toBeTruthy();
  });
});
