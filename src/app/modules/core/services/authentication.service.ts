import { CodigoDescripcion } from './../models/codigodescripcion.model';
import { ErrorsService } from './../../mycommon/services/errors.service';
import { GuiUtilsService } from 'src/app/modules/mycommon/services/gui-utils.service';
import { environment } from './../../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Usuario } from '../models/models.index';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    public currentUser$: BehaviorSubject<Usuario>;
    public get currentUser(): Usuario {
        return this.currentUser$.value;
    }

    /**
     * @ignore
     */
    private _token: string = null;
    get Token(): string {
        return this._token;
    }



    // tslint:disable-next-line: variable-name
    private _tramitesDisponibles: CodigoDescripcion[] = null;

    get TramitesDisponibles(): Observable<CodigoDescripcion[]> {
        if (!!this._tramitesDisponibles) {
            return of(this._tramitesDisponibles);
        } else {
            return this.http.get(`${environment.apiUrlBase}/tramites/disponibles`).pipe(
                map((respuesta: any[]) => {
                    this._tramitesDisponibles = [];
                    respuesta.forEach(t => {
                        const tramite: CodigoDescripcion = {
                            Codigo: t.Codigo,
                            Descripcion: t.Descripcion
                        };
                        this._tramitesDisponibles.push(tramite);
                    });
                    return this._tramitesDisponibles;
                }),
                catchError(error => {
                    this.errorService.RegistrarErrorHttp(error);
                    return of([]);
                })
            );
        }
    }

    constructor(private http: HttpClient, private utils: GuiUtilsService, private errorService: ErrorsService) {
        const token = localStorage.getItem('token');
        if (token) {
            this._token = token;
            const usuario = this.Token2User(this._token);
            this.currentUser$ = new BehaviorSubject<Usuario>(usuario);
        } else {
            this.currentUser$ = new BehaviorSubject<Usuario>(null);
        }
        // this.currentUser = this.currentUserSubject.asObservable();
    }

    get UsuarioEstaAdherido(): boolean {
        return this.currentUser.Perfiles.indexOf('adherido') > -1;
    }
    get UsuarioEstaSuspendido(): boolean {
        return !(this.currentUser.Perfiles.indexOf('nosuspendido') > -1);
    }

    get UsuarioEsSociedadColegiada(): boolean {
        return this.currentUser.Perfiles.indexOf('sociedad') > -1;
    }

    get UsuarioEstaAutenticado(): boolean {
        // de momento solo comprobamos que el usuario sea distinto de null
        // tslint:disable-next-line: no-non-null-assertion
        return !!this.currentUser;
    }




    checkInicial() {
        if (this._token && environment.refreshToken) {
            this.refreshToken(this._token).subscribe(ok => {
                if (!ok) {
                    this.logout();
                }
            });
        } else {
            // no hago nada
        }
    }

    /**
     * Método para hace login. Conecta con el backend y si el resultado es correcto almacena en el 
     * servicio el token devuelto y los datos del usuario para dejarlos disponibles para el resto de la aplicación.
     * @param  username Identificador del usuario que intenta hacer login
     * @param  password Password del usuario que intenta hace login
     * @returns True si el login es correcto o false en caso contrario
     */
    login(username: string, password: string): Observable<boolean> {
        // return this.http.post<any>(`${config.apiUrl}/users/authenticate`, { username, password });
        const url = `${environment.apiUrlBase}/usuarios/login`;
        return this.http.post<any>(url, { Username: username, Password: password })
            .pipe(
                map(autorizacion => {
                    this._token = autorizacion.Token;
                    localStorage.setItem('token', autorizacion.Token);
                    // const helper = new JwtHelperService();
                    const usuario = this.Token2User(this._token);
                    this.currentUser$.next(usuario);
                    return true;
                }),
                catchError(error => {
                    // tslint:disable-next-line: triple-equals
                    if (error.status == '401') {
                        this.utils.ShowError('Usuario y/o contraseña no válidos');
                    } else {
                        this.errorService.RegistrarErrorHttp(error);
                    }

                    return of(false);
                })
            );
    }

    private refreshToken(token): Observable<boolean> {
        
        const url = `${environment.apiUrlBase}/token/refresh`;
        return this.http.get<any>(url)
            .pipe(
                map(autorizacion => {
                    
                    this._token = autorizacion.Token;
                    localStorage.setItem('token', autorizacion.Token);
                    // const helper = new JwtHelperService();
                    const usuario = this.Token2User(this._token);
                    this.currentUser$.next(usuario);
                    // saco del token la información
                    // localStorage.setItem('currentUser', JSON.stringify(token));
                    return true;
                }),
                catchError(error => {
                    
                    return of(false);
                })
            );

    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('token');
        this._token = null;
        this._tramitesDisponibles = null;
        this.currentUser$.next(null);

    }


    private Token2User(token: string): Usuario {
        const helper = new JwtHelperService();
        const decodedToken = helper.decodeToken(token);
        // const expirationDate = helper.getTokenExpirationDate(token);
        // const isExpired = helper.isTokenExpired(token);
        const usuario: Usuario = {
            Id: decodedToken.unique_name,
            Username: decodedToken.given_name,
            Perfiles: decodedToken.role,
            Email: decodedToken.email
        };

        return usuario;

        // usuario.id = decodedToken.unique_name;
        // usuario.username = decodedToken.given_name;
        // usuario.perfiles = decodedToken.role;
        // return usuario;
    }

}
