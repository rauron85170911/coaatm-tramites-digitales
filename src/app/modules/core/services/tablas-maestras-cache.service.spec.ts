import { TestBed } from '@angular/core/testing';

import { TablasMaestrasCacheService } from './tablas-maestras-cache.service';

describe('TablasMaestrasCacheService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TablasMaestrasCacheService = TestBed.get(TablasMaestrasCacheService);
    expect(service).toBeTruthy();
  });
});
