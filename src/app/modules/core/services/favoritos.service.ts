import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map, share } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FavoritosService {

  constructor(private http: HttpClient) { }

  private tablaCache: { [key: string]: any[] } = {};
  private observableCache: { [key: string]: Observable<any[]> } = {};


  public get Ubicaciones(): Observable<any[]> {
    return this.pedirFavoritos('ubicaciones');
  }

  public get AutoresEncargo(): Observable<any[]> {
    return this.pedirFavoritos('autoresencargo');
  }

  public get TiposIntervencion(): Observable<any[]> {
    return this.pedirFavoritos('tiposintervencion');
  }

  private pedirFavoritos(favoritos: string, filtro?: string): Observable<any> {

    const key: string = favoritos; //this.construirClave(tabla, filtro);
    // if (filtro) {
    //   key += '#' + filtro;
    // }
    if (this.tablaCache[key]) {
      return of(this.tablaCache[key]);
    } else if (this.observableCache[key]) {
      return this.observableCache[key];
    } else {
      this.observableCache[key] = this.fetchFavoritos(favoritos);
      return this.observableCache[key];

    }
  }

  private fetchFavoritos(favoritos: string): Observable<any> {
    const url = `${environment.apiUrlBase}/colegiados/favoritos/${favoritos}`;
    const key: string = favoritos; // this.construirClave(tabla, filtro);
    return this.http.get<any[]>(url).pipe(
      map(rawData => {
        this.observableCache[key] = null;
        this.tablaCache[key] = rawData;
        return this.tablaCache[key];
      }),
      share()
    );
  }




}
