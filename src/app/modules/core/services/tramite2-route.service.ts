import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class Tramite2RouteService {

  constructor(private route: Router) { }

  segmentos: any = {
    AR: ['/registros', 'altaintervencion'],
    AE: ['/visados', 'altaintervencion'],
    RR: ['/registros', 'renuncia'],
    RE: ['/visados', 'renuncia'],
    MR: ['/registros', 'modificacionintervencion'],
    ME: ['/visados', 'modificacionintervencion'],
    RX: ['/registros', 'anexointervencion'],
    AX: ['/visados', 'anexointervencion'],
    GC: ['/visados', 'cambiogc']
  };

  // const fragmento2: any = {
  //   AR: 'altaintervencion',
  //   AE: 'altaintervencion',
  //   RR: 'renuncia',
  //   RE: 'renuncia',
  //   MR: 'modificacionintervencion',
  //   ME: 'modificacionintervencion'
  // };

  GetRoute(tipotramite: string, idSolicitud: number | string): string[] {

    if (this.segmentos[tipotramite]) {
      const completo = this.segmentos[tipotramite].concat(idSolicitud);
      return completo;
    } else {
      return null;
    }
  }

  Navigate(tipotramite: string, idSolicitud: number | string): Promise<boolean> {
    const paths = this.GetRoute(tipotramite, idSolicitud);
    if (paths) {
      return this.route.navigate(paths);
    } else {
      return null;
    }
  }



}
