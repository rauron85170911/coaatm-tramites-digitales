import { HttpClient, HttpErrorResponse, HttpEvent, HttpEventType } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Documento } from '../models/models.index';
import { Observable, of, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  constructor(private http: HttpClient) { }

  

  UploadDocumento(solicitudAsociada: string, documento: Documento): Observable<any> {
    const endpoint = `${environment.apiUrlBase}/documentos/${solicitudAsociada}`;
    const formData: FormData = new FormData();
    if (documento.Contenido) {
      formData.append('documento', documento.Contenido, documento.Nombre);
      formData.append('tipo', documento.Tipo);
      formData.append('descripcion', documento.Descripcion);
      formData.append('nombre', documento.Nombre);
    }
    return this.http
      .post<any>(
        endpoint,
        formData,
        {
          reportProgress: true,
          observe: 'events'
        })
      .pipe(
        map(event => {
          console.log('Evento recibido');
          console.log(event);
          return this.getEventMessage(event, formData);
        }),
        catchError(this.handleError)
      );
  }

  private getEventMessage(event: HttpEvent<any>, formData) {
    console.log('analizando mensaje');
    console.log(event.type.toString());
    switch (event.type) {
      case HttpEventType.Sent:
        return { status: 'progress', message: 0 };
      case HttpEventType.UploadProgress:
        return this.fileUploadProgress(event);
      // break;
      case HttpEventType.ResponseHeader:
        return { status: 'progress', message: 100 };
      case HttpEventType.DownloadProgress:
        return { status: 'progress', message: 100 };
      case HttpEventType.Response:
        return this.apiResponse(event);
      // break;
      default:
        return `File "${formData.get('profile').name}" surprising upload event: ${event.type}.`;
    }
  }

  private fileUploadProgress(event) {
    event.total = event.total || 1;
    const percentDone = Math.round(100 * event.loaded / event.total);
    return { status: 'progress', message: percentDone };
  }

  private apiResponse(event) {
    console.log(`Respuesta final:${event.body}`);
    return { status: 'finished', message: event.body };
  }

  private handleError(error: HttpErrorResponse) {
    
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    //return throwError('Ha ocurrido un error, por favor, inténtelo más tarde');
    return of({ status: 'error', message: 'Se produjo un error al subir el documento' });
  }
}
