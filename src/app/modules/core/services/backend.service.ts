import { ResultadoAccion } from './../models/resultadoaccion.model';

import { environment } from 'src/environments/environment';
import { Ubicacion } from './../../visados/models/models.index';
import { Injectable, Input } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, from, combineLatest } from 'rxjs';
import { CodigoDescripcion, Documento, ColegiadoInterviniente, CodigoDescripcionAmpliado, Colegiado } from '../models/models.index';
import { map, catchError, switchMap, mergeMap, flatMap, mergeAll, tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class BackendService {

  constructor(private http: HttpClient) { }

  /**
   *
   * @param idSolicitud Identificador de la solicitud de la que queremos recuperar los colegiados intervinientes
   */
  GetColegiadosIntervinientes(idSolicitud: number, registro: boolean): Observable<ColegiadoInterviniente[]> {
    const url = `${environment.apiUrlBase}/solicitudes/${idSolicitud}/colegiados?registro=${registro}`;
    return this.httpget(url).pipe(
      switchMap((intervinientes: any[]) => {
        if (intervinientes.length > 0) {
          const internal = intervinientes.map(interviniente => {
            return this.GetColegiado(interviniente.IdColegiado).pipe(
              map(cservidor => {
                const c: ColegiadoInterviniente = {
                  Nombre: cservidor.Nombre,
                  Nif: cservidor.Nif,
                  IdColegiado: interviniente.IdColegiado,
                  Asalariado: interviniente.Asalariado,
                  PorcentajeObra: interviniente.PorcentajeObra,
                  PorcentajeObraAnterior: interviniente.PorcentajeObraAnterior,
                  PorcentajeGastosGestion: interviniente.PorcentajeGastosGestion,
                  IdAsalariado: interviniente.IdAsalariado,
                  IdEmpresaPago: interviniente.IdEmpresaPago,
                  GestionCobro: interviniente.GestionCobro,
                  Honorarios: interviniente.Honorarios,
                  EsColegiadoCreador: interviniente.EsColegiadoCreador,
                  Sociedad: cservidor.Sociedad,
                  Participaciones: null,
                  SeguroAceptado: interviniente.SeguroAceptado,
                  InformeEvaluacionEdificiosAceptado: interviniente.InformeEvaluacionEdificiosAceptado,
                  VisadoAceptado: interviniente.VisadoAceptado,
                  SolicitudValidada: interviniente.SolicitudValidada,
                  FechaValidacion: interviniente.SolicitudValidada ? interviniente.FechaValidacion : null
                };
                return c;
              })
            );
          });
          return combineLatest(internal);
        } else {
          return of([]);
        }
      })
    );
  }






  /**
   *
   * @param idColegiado Identificador del colegiado a recuperar
   */
  GetColegiado(idColegiado: string): Observable<Colegiado> {
    const url = `${environment.apiUrlBase}/colegiados/${idColegiado}`;
    return this.httpget(url);
  }



  GetParticipaciones(idSolicitud: number, idSociedadColegiada: number, registro: boolean): Observable<any[]> {
    const url = `${environment.apiUrlBase}/colegiados/sociedad/${idSociedadColegiada}/colegiados`;
    return this.http.get(url).pipe(
      switchMap((colegiados: any[]) => {
        if (colegiados.length > 0) {
          const sources = colegiados.map(item => {
            return this.GetParticipacionColegiado(idSolicitud, item.NumeroColegiado, registro).pipe(
              map((p: number[]) => {
                return {
                  IdColegiado: item.NumeroColegiado,
                  Nombre: item.Nombre,
                  PorcentajeParticipacion: p === null ? item.PorcentajeParticipacion : p[0],
                  PorcentajeParticipacionAnterior: p === null ? item.PorcentajeParticipacion : p[1]
                };
              })
            );
          });
          return combineLatest(sources);
        } else {
          return of([]);
        }
      })
    );
  }

  GetAutoresEncargo(idSolicitud: number, registro: boolean): Observable<any[]> {
    const url = `${environment.apiUrlBase}/intervinientes/${idSolicitud}/autoresencargo?registro=${registro}`;
    return this.http.get(url).pipe(
      map((response: any[]) => {
        return response;
      })
    );
  }

  GetAutorEncargo(idSolicitud: number, id: number, registro: boolean): Observable<any> {
    const url = `${environment.apiUrlBase}/intervinientes/${idSolicitud}/autoresencargo/${id}?registro=${registro}`;
    return this.http.get(url).pipe(
      map((response: any[]) => {
        return response;
      })
    );
  }

  CreateAutorEncargo(idSolicitud: number, autor: any, registro: boolean): Observable<ResultadoAccion> {
    const url = `${environment.apiUrlBase}/intervinientes/${idSolicitud}/autoresencargo?registro=${registro}`;
    return this.http.post(url, autor).pipe(
      map((response: any) => {
        return response;
      })
    );
  }
  UpdateAutorEncargo(idSolicitud: number, autor: any, registro: boolean): Observable<ResultadoAccion> {
    const url = `${environment.apiUrlBase}/intervinientes/${idSolicitud}/autoresencargo?registro=${registro}`;
    return this.http.put(url, autor).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  DeleteAutorEncargo(idSolicitud: number, id: number, registro: boolean): Observable<ResultadoAccion> {
    const url = `${environment.apiUrlBase}/intervinientes/${idSolicitud}/autoresencargo/${id}?registro=${registro}`;
    return this.http.delete(url).pipe(
      map((response: any) => {
        return response;
      })
    );
  }


  GetAutoresProyecto(idSolicitud: number, registro: boolean): Observable<any[]> {
    const url = `${environment.apiUrlBase}/intervinientes/${idSolicitud}/autoresproyecto?registro=${registro}`;
    return this.http.get(url).pipe(
      map((response: any[]) => {
        return response;
      })
    );
  }
  GetAutorProyecto(idSolicitud: number, id: number, registro: boolean): Observable<any> {
    const url = `${environment.apiUrlBase}/intervinientes/${idSolicitud}/autoresproyecto/${id}?registro=${registro}`;
    return this.http.get(url).pipe(
      map((response: any[]) => {
        return response;
      })
    );
  }

  CreateAutorProyecto(idSolicitud: number, autor: any, registro: boolean) {
    const url = `${environment.apiUrlBase}/intervinientes/${idSolicitud}/autoresproyecto?registro=${registro}`;
    return this.http.post(url, autor).pipe(
      map((response: any[]) => {
        return response;
      })
    );
  }
  UpdateAutorProyecto(idSolicitud: number, autor: any, registro: boolean) {
    const url = `${environment.apiUrlBase}/intervinientes/${idSolicitud}/autoresproyecto?registro=${registro}`;
    return this.http.put(url, autor).pipe(
      map((response: any[]) => {
        return response;
      })
    );
  }

  DeleteAutorProyecto(idSolicitud: number, id: number, registro: boolean): Observable<any> {
    const url = `${environment.apiUrlBase}/intervinientes/${idSolicitud}/autoresproyecto/${id}?registro=${registro}`;
    return this.http.delete(url).pipe(
      map((response: any[]) => {
        return response;
      })
    );
  }

  GetParticipacionColegiado(idSolicitud: number, idColegiado: string, registro: boolean): Observable<number[]> {
    const url = `${environment.apiUrlBase}/solicitudes/${idSolicitud}/participacion/${idColegiado}?registro=${registro}`;
    return this.http.get(url).pipe(
      map((participacion: number[]) => {
        return participacion;
      }),
      catchError(e => {
        return of(null);
      })
    );
  }



  ActualizarColegiadoInterviniente(
    idSolicitud: number, datos: ColegiadoInterviniente, registro: boolean): Observable<ColegiadoInterviniente> {
    const url = `${environment.apiUrlBase}/solicitudes/${idSolicitud}/colegiados?registro=${registro}`;
    return this.http.put(url, datos).pipe(
      tap(response => {
        console.log(response);
      }),
      map((response: ColegiadoInterviniente) => {
        return response;
      })
    );
  }

  BorrarColegiadoInterviniente(idSolicitud: number, idColegiadoInterviniente: number, registro: boolean): Observable<boolean> {
    const url = `${environment.apiUrlBase}/solicitudes/${idSolicitud}/colegiados/${idColegiadoInterviniente}?registro=${registro}`;
    return this.http.delete(url).pipe(
      tap(response => {
        console.log(response);
      }),
      map(response => {
        return response as boolean;
      })
    );
  }






  LocalizarColegiado(idColegiado: string, nif: string): Observable<any> {
    if (idColegiado && nif) {
      const url = `${environment.apiUrlBase}/colegiados/localizar`;
      const data = {
        IdColegiado: idColegiado,
        Nif: nif
      };
      return this.http.post(url, data).pipe(
        map((response: any) => {
          return response;
        })
      );
    } else {
      return of(null);
    }

  }

  GetEmpresasPagoColegiado(idColegiado: number): Observable<CodigoDescripcionAmpliado[]> {
    const url = `${environment.apiUrlBase}/colegiados/${idColegiado}/empresas`;
    return this.httpget(url);
  }





  private httpget(url: string): Observable<any> {
    // const url = `${environment.apiUrlBase}/maestros/${tabla}`;
    return this.http.get(url);
  }



}
