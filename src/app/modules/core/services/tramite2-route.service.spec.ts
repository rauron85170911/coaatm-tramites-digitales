import { TestBed } from '@angular/core/testing';

import { Tramite2RouteService } from './tramite2-route.service';

describe('Tramite2RouteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Tramite2RouteService = TestBed.get(Tramite2RouteService);
    expect(service).toBeTruthy();
  });
});
