import { HttpClient, HttpEvent, HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Documento } from '../models/models.index';
import { Observable, throwError, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map, catchError } from 'rxjs/operators';
import { ErrorsService } from '../../mycommon/services/errors.service';

@Injectable({
  providedIn: 'root'
})
export class DocumentacionService {

  constructor(private http: HttpClient, private errorService: ErrorsService) { }


  GetDocumentosSolicitud(idSolicitud: string): Observable<any> {
    const url = `${environment.apiUrlBase}/documentos/${idSolicitud}`;
    return this.http.get(url).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  // UploadDocumento(solicitudAsociada: string, documento: Documento): Observable<any> {
  //   const endpoint = `${environment.apiUrlBase}/documentos/upload/${solicitudAsociada}`;
  //   const formData: FormData = new FormData();
  //   if (documento.Contenido) {
  //     formData.append('documento', documento.Contenido, documento.Nombre);
  //     formData.append('tipo', documento.Tipo);
  //     formData.append('descripcion', documento.Descripcion);
  //     formData.append('nombre', documento.Nombre);
  //   }
  //   return this.http
  //     .post<any>(
  //       endpoint,
  //       formData,
  //       {
  //         reportProgress: true,
  //         observe: 'events'
  //       })
  //     .pipe(
  //       map(event => {
  //         console.log('Evento recibido');
  //         console.log(event);
  //         return this.getEventMessage(event, formData);
  //       }),
  //       catchError(this.handleError)
  //     );
  // }

  public DownloadDocumento(solicitudAsociada: string, tipo: string, iddocumento: string, colegio: boolean): Observable<Blob> {
    const urlCompleta = `${environment.apiUrlBase}/documentos/${solicitudAsociada}/${iddocumento}?colegio=${colegio}`;
    return this.http.get<Blob>(urlCompleta, { responseType: 'blob' as 'json' }).pipe(
      catchError(error => {
        this.errorService.RegistrarErrorHttp(error);
        return of(null);
      })
    );
  }


  public DeleteDocumento(solicitudAsociada: string, tipo: string, iddocumento: string): Observable<boolean> {
    const urlCompleta = `${environment.apiUrlBase}/documentos/${solicitudAsociada}/${iddocumento}`;
    return this.http.delete(urlCompleta).pipe(
      map(response => true),
      catchError(error => {
        this.errorService.RegistrarErrorHttp(error);
        return of(false);
      })
    );
  }




  private getEventMessage(event: HttpEvent<any>, formData) {
    console.log('analizando mensaje');
    console.log(event.type.toString());
    switch (event.type) {
      case HttpEventType.Sent:
        return { status: 'progress', message: 0 };
      case HttpEventType.UploadProgress:
        return this.fileUploadProgress(event);
      // break;
      case HttpEventType.ResponseHeader:
        return { status: 'progress', message: 100 };
      case HttpEventType.DownloadProgress:
        return { status: 'progress', message: 100 };
      case HttpEventType.Response:
        return this.apiResponse(event);
      // break;
      default:
        return `File "${formData.get('profile').name}" surprising upload event: ${event.type}.`;
    }
  }

  private fileUploadProgress(event) {
    event.total = event.total || 1;
    const percentDone = Math.round(100 * event.loaded / event.total);
    return { status: 'progress', message: percentDone };
  }

  private apiResponse(event) {
    console.log(`Respuesta final:${event.body}`);
    return { status: 'finished', message: event.body };
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('Ha ocurrido un error, por favor, inténtelo más tarde');
  }
}
