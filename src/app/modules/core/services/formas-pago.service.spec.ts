import { TestBed } from '@angular/core/testing';

import { FormasPagoService } from './formas-pago.service';

describe('FormasPagoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FormasPagoService = TestBed.get(FormasPagoService);
    expect(service).toBeTruthy();
  });
});
