import { CodigoDescripcionAmpliado } from './../models/codigodescripcionampliado.model';
import { CodigoDescripcion } from './../models/codigodescripcion.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map, share, catchError } from 'rxjs/operators';
import { TipoIntervencion } from '../models/models.index';

@Injectable({
  providedIn: 'root'
})
export class TablasMaestrasCacheService {

  constructor(private http: HttpClient) { }

  private tablaCache: { [key: string]: CodigoDescripcion[] } = {};
  private observableCache: { [key: string]: Observable<CodigoDescripcion[]> } = {};


  public get TiposVia(): Observable<Array<CodigoDescripcion>> {
    return this.pedirTabla('maestros/tiposvia');
  }


  public get Provincias(): Observable<Array<CodigoDescripcion>> {
    return this.pedirTabla('maestros/provincias');
  }
  public get DistritosMadrid(): Observable<Array<CodigoDescripcion>> {
    return this.pedirTabla('maestros/distritosmadrid');
  }
  public getMuniciposProvincia(idprovincia: string): Observable<CodigoDescripcion[]> {
    return this.pedirTabla('maestros/municipios', idprovincia);
  }

  public get TiposActuacion(): Observable<Array<CodigoDescripcion>> {
    return this.pedirTabla('maestros/tiposactuacion');
  }

  public get TiposIntervencion(): Observable<TipoIntervencion[]> {
    return this.pedirTabla('maestros/tiposintervencion');
  }

  get DestinosPrincipalesObra(): Observable<CodigoDescripcion[]> {
    return this.pedirTabla('maestros/destinosobra');
  }

  get TiposCertificado(): Observable<CodigoDescripcion[]> {
    return this.pedirTabla('certificados/tipos');
  }

  get TiposPromotor(): Observable<CodigoDescripcion[]> {
    return this.pedirTabla('maestros/tipospromotor');
  }

  get GruposTiposTramite(): Observable<CodigoDescripcion[]> {
    return this.pedirTabla('maestros/grupostipostramite');
  }
  get TiposTramite(): Observable<CodigoDescripcionAmpliado[]> {
    return this.pedirTabla('maestros/tipostramite');
  }

  get EstadosTramite(): Observable<CodigoDescripcion[]> {
    return this.pedirTabla('maestros/estadostramite');
  }

  get EstadosIntervencion(): Observable<CodigoDescripcion[]> {
    return this.pedirTabla('maestros/estadosintervencion');
  }

  get MotivosModificacionIntervencion(): Observable<CodigoDescripcionAmpliado[]> {
    return this.pedirTabla('maestros/motivosmodificacionintervencion');
  }

  get TiposDocumento(): Observable<CodigoDescripcion[]> {
    return this.pedirTabla('maestros/tiposdocumento');
  }
  private pedirTabla(tabla: string, filtro?: string): Observable<any> {

    const key: string = this.construirClave(tabla, filtro);
    // if (filtro) {
    //   key += '#' + filtro;
    // }
    if (this.tablaCache[key]) {
      return of(this.tablaCache[key]);
    } else if (this.observableCache[key]) {
      return this.observableCache[key];
    } else {
      this.observableCache[key] = this.fetchTabla(tabla, filtro);
      return this.observableCache[key];

    }
  }

  private construirClave(tabla: string, filtro?: string) {
    let key: string = tabla.replace('/', '#');
    if (filtro) {
      key += '#' + filtro;
    }
    return key;
  }

  private fetchTabla(tabla: string, filtro?: string): Observable<any> {
    let url = `${environment.apiUrlBase}/${tabla}`;
    if (filtro) {
      url += `/${filtro}`;
    }
    const key: string = this.construirClave(tabla, filtro);
    return this.http.get<CodigoDescripcion[]>(url).pipe(
      map(rawData => {
        this.observableCache[key] = null;
        this.tablaCache[key] = rawData;
        return this.tablaCache[key];
      }),
      share(),
      catchError(error => {
        return of([]);
      })
    );
  }

  // private mapCachedTabla(body: ApiData.iClub) {
  //   this.observableCache[body.id] = null;
  //   this.clubCache[body.id] = new Club(body);
  //   return this.clubCache[body.id];
  // } 




}
