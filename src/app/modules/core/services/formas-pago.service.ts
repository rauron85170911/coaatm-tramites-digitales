import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map, share, catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class FormasPagoService {

  constructor(private http: HttpClient) { }



  public GetFormasPagoHabilitadas(idColegiado: number, idTipoTramite: string): Observable<any[]> {

    const url = `${environment.apiUrlBase}/public/formasdepago/colegiado/${idColegiado}/${idTipoTramite}`;

    const imagenes: any = {
      CU: './assets/images/forma-pago/ico_euro.svg',
      TJ: './assets/images/forma-pago/ico_tarjeta.svg',
      TR: './assets/images/forma-pago/ico_transferencia.svg',
      ME: './assets/images/forma-pago/ico_metalico.svg'
    };

    return this.http.get(url).pipe(
      map((response: any[]) => {

        const fp: any[] = response.map(f => {
          // le añadimos la ruta a la imagen
          f.UrlImagen = imagenes[f.Codigo];
          return f;
        });
        return fp;
      })
    );

    


  }

}
