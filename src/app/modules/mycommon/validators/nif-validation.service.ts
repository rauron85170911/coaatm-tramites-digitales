import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NifValidationService {

  constructor() { }

  DNI_REGEX = /^(\d{8})([A-Z])$/;
  CIF_REGEX = /^([ABCDEFGHJKLMNPQRSUVW])(\d{7})([0-9A-J])$/;
  NIE_REGEX = /^[XYZ]\d{7,8}[A-Z]$/;


   ValidateSpanishID( str ) {
    // Ensure upcase and remove whitespace
    str = str.toUpperCase().replace(/\s/, '');

    let valid = false;
    const type = this.spainIdType( str );

    switch (type) {
      case 'dni':
        valid = this.validDNI( str );
        break;
      case 'nie':
        valid = this.validNIE( str );
        break;
      case 'cif':
        valid = this.validCIF( str );
        break;
    }

    return {
      type,
      valid
    };
  }

  spainIdType( str ) {
    if ( str.match( this.DNI_REGEX ) ) {
      return 'dni';
    }
    if ( str.match( this.CIF_REGEX ) ) {
      return 'cif';
    }
    if ( str.match( this.NIE_REGEX ) ) {
      return 'nie';
    }
  }

  validDNI( dni ) {
    const dniletters = 'TRWAGMYFPDXBNJZSQVHLCKE';
    const letter = dniletters.charAt( parseInt( dni, 10 ) % 23 );    
    return letter === dni.charAt(8);
  }

  validNIE( nie ) {

    // Change the initial letter for the corresponding number and validate as DNI
    let nieprefix = nie.charAt( 0 );

    switch (nieprefix) {
      case 'X': nieprefix = 0; break;
      case 'Y': nieprefix = 1; break;
      case 'Z': nieprefix = 2; break;
    }

    return this.validDNI( nieprefix + nie.substr(1) );

  }


  validCIF( cif ) {

    const match = cif.match( this.CIF_REGEX );
    const letter = match[1];
    const numero  = match[2];
    const control = match[3];
        

    let evensum = 0;
    let oddsum = 0;
    let n;

    for ( let i = 0; i < numero.length; i++) {
      n = parseInt( numero[i], 10 );
      // Odd positions (Even index equals to odd position. i=0 equals first position)
      if ( i % 2 === 0 ) {
        // Odd positions are multiplied first.
        n *= 2;

        // If the multiplication is bigger than 10 we need to adjust
        oddsum += n < 10 ? n : n - 9;

      // Even positions
      // Just sum them
      } else {
        evensum += n;
      }
    }

    const sumaparacalculo = +(evensum + oddsum).toString().substr(-1);
    const controldigit = (10 - sumaparacalculo );
    const controlletter = 'JABCDEFGHI'.substr( controldigit, 1 );

    // Control must be a digit
    if ( letter.match( /[ABEH]/ ) ) {
      return control === controldigit;

    // Control must be a letter
    } else if ( letter.match( /[KPQS]/ ) ) {
      return control === controlletter;

    // Can be either
    } else {
      return control === controldigit || control === controlletter;
    }

  }







}
