import { TestBed, getTestBed } from '@angular/core/testing';

import { NifValidationService } from './nif-validation.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

describe('NifValidationService', () => {
  let injector: TestBed;
  let servicio: NifValidationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [NifValidationService]
    });

    injector = getTestBed();
    servicio = injector.get(NifValidationService);
  });

  it('should be created', () => {
    expect(servicio).toBeTruthy();
  });

  it('Valida como bueno un nif válido', () => {
    expect(servicio).toBeTruthy();
    
    const resul = servicio.ValidateSpanishID('30808300F');
    expect(resul.valid).toBeTruthy();
    expect(resul.type).toBe('dni');

  });

  it('Valida como erroneo un nif erroneo', () => {
    expect(servicio).toBeTruthy();
    
    const resul = servicio.ValidateSpanishID('3080300F');
    expect(resul.valid).toBeFalsy();
    expect(resul.type).toBeUndefined();

  });

  it('Valida como bueno un cif válido', () => {
    expect(servicio).toBeTruthy();

    const resul = servicio.ValidateSpanishID('S7830489F');
    expect(resul.valid).toBeTruthy();
    expect(resul.type).toBe('cif');

  });
  it('Valida como erroneo un cif no válido', () => {
    expect(servicio).toBeTruthy();

    const resul = servicio.ValidateSpanishID('S7830489H');
    expect(resul.valid).toBeFalsy();
    expect(resul.type).toBe('cif');

  });

  it('Valida como bueno un nie válido', () => {
    expect(servicio).toBeTruthy();

    const resul = servicio.ValidateSpanishID('Y4196872F');
    expect(resul.valid).toBeTruthy();
    expect(resul.type).toBe('nie');

  });
  it('Valida como erroneo un nie no válido', () => {
    expect(servicio).toBeTruthy();

    const resul = servicio.ValidateSpanishID('Y4197872F');
    expect(resul.valid).toBeFalsy();
    expect(resul.type).toBe('nie');

  });



});
