import { Directive, AfterViewInit, ElementRef, Renderer2, OnInit, TemplateRef, ViewContainerRef, DoCheck } from '@angular/core';

@Directive({
  selector: '[appTableResponsive]'
})
export class TableResponsiveDirective implements AfterViewInit, OnInit,DoCheck {
  


  constructor(
    private el: ElementRef,
    private renderer: Renderer2) {
  }


  ngOnInit(): void {

    const capa = this.renderer.createElement('div');
    this.renderer.addClass(this.el.nativeElement, 'no-more-tables');

  }

  ngAfterViewInit(): void {
    // la recubro con un <div class="no-more-tables">
    this.completaFilas();
  }
  ngDoCheck(): void {
    this.completaFilas();
  }
  /** Este método completa los td del tbody para añadirle el atributo data-title = titulo del
   *  th correspondiente de la cabecera
   *
   ***/

  completaFilas() {
    // busco la tabla
    const tabla = this.el.nativeElement.querySelector('table');
    if (!!tabla) {
      // saco de la tabla los th

      const titulos: string[] = [];
      tabla.querySelectorAll('thead>tr>th').forEach(i => {

        titulos.push(i.innerText);
      });
      //console.log(titulos);
      // ya tengo los títulos, ahora se lo pongo como atrituvo a cada elemento td de cada fila del tbody de la tabla

      tabla.querySelectorAll('tbody>tr').forEach(tr => {
        let indice = 0;
        tr.querySelectorAll('td').forEach(td => {
          
          this.renderer.setAttribute(td, 'data-title', titulos[indice]);
          indice++;
        });
      });
    }



    // this.el.nativeElement.querySelectorAll('input').forEach(e => {
    //   this.renderer.setStyle(e, 'background-color', color);
    // });

    // this.el.nativeElement.querySelectorAll('select').forEach(e => {
    //   this.renderer.setStyle(e, 'background-color', color);
    // });

    // this.el.nativeElement.querySelectorAll('.form-check').forEach(e => {
    //   this.renderer.setStyle(e, 'background-color', color);
    // });



  }


}
