import { Directive, Input, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { AnimationPlayer, AnimationBuilder, AnimationMetadata, style, animate } from '@angular/animations';

@Directive({
  selector: '[appFadeInOutAnimation]'
})
export class FadeInOutAnimationDirective implements OnInit, OnDestroy {

  player: AnimationPlayer;

  // @Input()
  // set show(show: boolean) {
  //   if (this.player) {
  //     this.player.destroy();
  //   }

  //   const metadata = show ? this.fadeIn() : this.fadeOut();

  //   const factory = this.builder.build(metadata);
  //   const player = factory.create(this.el.nativeElement);

  //   player.play();
  // }

  constructor(private builder: AnimationBuilder, private el: ElementRef) {}

  ngOnInit(): void {
    if (this.player) {
      this.player.destroy();
    }
    const metadata = this.fadeIn();
    const factory = this.builder.build(metadata);
    this.player = factory.create(this.el.nativeElement);
    this.player.play();
  }
  ngOnDestroy(): void {
    if (this.player) {
      this.player.destroy();
    }
    const metadata = this.fadeOut();
    const factory = this.builder.build(metadata);
    this.player = factory.create(this.el.nativeElement);
    this.player.play();
  }
  private fadeIn(): AnimationMetadata[] {
    return [
      style({ opacity: 0, height: '0px', overflow: 'hidden', border: 'solid thin blue' }),
      animate('2.3s 2.4s ease-in',
        style({ opacity: 1, height: '*'})
      ),
      style({border: 'solid thin green', position: 'relative'})
    ];
  }

  private fadeOut(): AnimationMetadata[] {
    return [
      style({ opacity: 1, border: 'solid thin red' }),
      animate('2.3s ease-out',
        style({ opacity: 0, height: '0px'})
      )
    ];
  }
}
