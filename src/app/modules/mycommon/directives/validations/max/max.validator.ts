import { AbstractControl, Validators, ValidatorFn } from '@angular/forms';

// import { isPresent } from '../util/lang';

export const max = (max: number): ValidatorFn => {
    return (control: AbstractControl): { [key: string]: any } => {
        if (!isPresent(max)) { return null; }
        if (isPresent(Validators.required(control))) { return null; }

        const v: number = +control.value;
        return v <= +max ? null : { actualValue: v, requiredValue: +max, max: true };
    };
};

export function isPresent(obj: any): boolean {
    return obj !== undefined && obj !== null;
}

export function isDate(obj: any): boolean {
    return !/Invalid|NaN/.test(new Date(obj).toString());
}
