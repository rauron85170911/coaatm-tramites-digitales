import { AbstractControl, Validators, ValidatorFn } from '@angular/forms';
import { NifValidationService } from './../../../validators/nif-validation.service';

export const nif = (nif: boolean): ValidatorFn => {
  return (control: AbstractControl): {[key: string]: any} => {
    if (!nif) {return null; }
    // if (!isPresent(nif)) { return null; }
    if (isPresent(Validators.required(control))) { return null; }

    const validador: NifValidationService = new NifValidationService();
    const v: string = control.value;
    // const v: number = +control.value;
    const r = validador.ValidateSpanishID(v);
    return r.valid ? null : {nif: true};
      // return r.valid ? null : { nif: true}; //this.validator(c);
    // return v >= range[0] && v <= range[1] ? null : {actualValue: v, requiredValue: range, range: true};
  };
};

function isPresent(obj: any): boolean {
    return obj !== undefined && obj !== null;
}