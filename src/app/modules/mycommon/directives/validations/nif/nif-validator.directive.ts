import { NifValidationService } from './../../../validators/nif-validation.service';
import { Directive, Input, forwardRef, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { NG_VALIDATORS, Validator, ValidatorFn, AbstractControl } from '@angular/forms';

import { nif } from './nif.validator';
const NIF_VALIDATOR: any = {
  provide: NG_VALIDATORS,
  useExisting: forwardRef(() => NifValidatorDirective),
  multi: true
};
@Directive({
  selector: '[nif][formControlName],[nif][formControl],[nif][ngModel]',
  providers: [NIF_VALIDATOR]
})
export class NifValidatorDirective implements Validator, OnInit, OnChanges {
  @Input() nif: boolean;

  private validator: ValidatorFn;
  private onChange: () => void;

  constructor(private a: NifValidationService) {}
  ngOnInit() {
      this.validator = nif(this.nif);
  }

  ngOnChanges(changes: SimpleChanges) {
      for (const key in changes) {
          if (key === 'nif') {
              this.validator = nif(changes[key].currentValue);
              if (this.onChange) { this.onChange(); }
          }
      }
  }

  validate(c: AbstractControl): { [key: string]: any } {
    return this.validator(c);
    // if (this.nif)  {
    //   const valor = c.value;
    //   if (!valor) { return null; }
    //   const r = this.a.ValidateSpanishID(valor);
    //   return r.valid ? null : { nif: true}; //this.validator(c);
      
    // } else {
    //   return null;
    // }
  }

  registerOnValidatorChange(fn: () => void): void {
      this.onChange = fn;
  }
}

