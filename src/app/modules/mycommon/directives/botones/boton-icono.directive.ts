import { Directive, Input, ElementRef, Renderer2, OnInit, HostListener } from '@angular/core';

@Directive({
  selector: '[appBotonIcono]'
})
export class BotonIconoDirective implements OnInit {

  @Input('appBotonIcono') tipo: string;
  constructor(private el: ElementRef, private renderer: Renderer2) { }

  ngOnInit(): void {


    const tipos = ['EDITAR', 'BORRAR', 'DESCARGAR', 'VER', 'DOCUMENTACION', 'PAGAR','ENVIAR'];
    // const textos = ['Enviar', 'Aceptar', 'Cancelar', 'Guardar progreso', 'Seleccionar'];
    const iconos = ['pencil', 'trash', 'download', 'eye', 'file-pdf-o', 'credit-card', 'paper-plane-o'];
    this.renderer.addClass(this.el.nativeElement, 'btn');
    this.renderer.addClass(this.el.nativeElement, 'btn-link');
    this.renderer.setStyle(this.el.nativeElement, 'margin-left', '7px');
    this.renderer.setStyle(this.el.nativeElement, 'margin-right', '7px');
    this.renderer.setStyle(this.el.nativeElement, 'padding', '0px');

    // this.renderer.addClass(this.el.nativeElement, 'mr-3');

    let icono = iconos[tipos.indexOf(this.tipo)];
    icono = icono || 'frown-o'; //ponemos uno por defecto para que se vea pero indicando que no hay icono definido
    if (icono) {
      const iconoEl = this.renderer.createElement('i');
      this.renderer.setStyle(iconoEl, 'margin', '0px');
      this.renderer.setStyle(iconoEl, 'padding', '0px');
      this.renderer.addClass(iconoEl, 'fa');
      this.renderer.addClass(iconoEl, `fa-${icono}`);
      this.renderer.appendChild(this.el.nativeElement, iconoEl);
    } else {

    }





  }
}
