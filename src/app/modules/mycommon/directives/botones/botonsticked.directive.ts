import { Directive, ElementRef, Renderer2, AfterViewInit, HostListener, ComponentFactoryResolver } from '@angular/core';

@Directive({
  selector: '[appBotonsticked]'
})
export class BotonstickedDirective implements AfterViewInit {


  constructor(private el: ElementRef, private renderer: Renderer2) {

    // this.renderer.setStyle(el.nativeElement, 'background-color', 'yellow');
    this.renderer.setStyle(el.nativeElement, 'position', 'sticky');
    this.renderer.setStyle(el.nativeElement, 'bottom', '5px');
    // this.renderer.setStyle(el.nativeElement, 'left', '50%');
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.centrar();

  }

  ngAfterViewInit(): void {
    // lo centro horizontalmente

    this.centrar();


  }

  centrar() {

    const ancho = this.el.nativeElement.offsetWidth;
    const anchoContenedor = window.innerWidth; // this.el.nativeElement.parentElement.offsetWidth;
    this.renderer.setStyle(this.el.nativeElement, 'left', anchoContenedor / 2 - ancho / 2 + 'px');
  }

}
