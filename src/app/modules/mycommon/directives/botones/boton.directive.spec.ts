import { TestBed, ComponentFixture } from '@angular/core/testing';
import { BotonDirective } from './boton.directive';
import { Component, NgModule, DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { HtmlAstPath } from '@angular/compiler';



@Component({
  template: `<button appBoton="CANCELAR"></button>`,
})
class BotonFakeComponent { }
// @NgModule({
//   declarations: [BotonFakeComponent, BotonDirective],
//   exports: [BotonFakeComponent],
// })
// class HostFakeModule { }

describe('BotonDirective', () => {
  let component: BotonFakeComponent;
  let fixture: ComponentFixture<BotonFakeComponent>;
  let buttonEl: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BotonDirective, BotonFakeComponent]
    }).compileComponents();
    fixture = TestBed.createComponent(BotonFakeComponent);
    component = fixture.componentInstance;
    buttonEl = fixture.nativeElement.firstChild;

  });

  it('should create an instance', () => {
    const directive = new BotonDirective(null, null);
    expect(directive).toBeTruthy();
  });

  it('Se establece el texto y las clases adecuadas', () => {

    fixture.detectChanges();
    expect(buttonEl.innerText.toLocaleLowerCase()).toBe('cancelar');
    expect(buttonEl.classList.contains('btn')).toBeTruthy();
    expect(buttonEl.classList.contains('btn-primary')).toBeTruthy();

  });

});
