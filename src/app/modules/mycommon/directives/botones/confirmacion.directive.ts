import { Directive, HostListener, Input, Output, EventEmitter } from '@angular/core';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import {ConfirmacionComponent} from '../../components/confirmacion/confirmacion.component';
@Directive({
  selector: '[appConfirmacion]'
})
export class ConfirmacionDirective {
  @Input('appConfirmacion') pregunta: string = null;

  @Output() confirmado: EventEmitter<null> = new EventEmitter<null>();

  @HostListener('click', ['$event']) onClick($event: any) {

    const opcionesModal: NgbModalOptions = {
      backdrop: 'static',
      centered: true
    };

    const modalRef = this.modalService.open(ConfirmacionComponent, opcionesModal);
    modalRef.componentInstance.pregunta = this.pregunta || '¿Seguro?' ;
    modalRef.result.then((result) => {
      this.confirmado.emit(null);
      // this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      console.log('No se ha confirmado la acción, no hacemos nada');
      // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });  }

  constructor(private modalService: NgbModal) { }



}
