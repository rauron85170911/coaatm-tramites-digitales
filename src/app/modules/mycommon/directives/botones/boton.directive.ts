import { Directive, ElementRef, Input, APP_BOOTSTRAP_LISTENER, Renderer2, OnInit } from '@angular/core';

@Directive({
  selector: '[appBoton]'
})
export class BotonDirective implements OnInit {

  @Input('appBoton') tipo: string;

  constructor(private el: ElementRef, private renderer: Renderer2) {

  }
  ngOnInit(): void {

    const textos = {
      ACEPTAR: 'Aceptar',
      CANCELAR: 'Cancelar',
      ENVIAR: 'Enviar',
      GUARDARPROGRESO: 'Guardar progreso',
      SELECCIONAR: 'Seleccionar',
      AÑADIRDOCUMENTO: 'Añadir documento',
      FILTRAR: 'Filtrar',
      LIMPIARFILTROS: 'Limpiar',
      ENVIARTRAMITE: 'Enviar',
      DOCUMENTACION: 'Ver Documentos',
      PAGOS: 'Pagos',
      ELIMINAR: 'Eliminar',
      NUEVOTRAMITE: 'Nuevo tramite',
      VOLVER: 'Volver'
    };

    const tiposBoton = {
      ACEPTAR: 'btn-primary',
      CANCELAR: 'btn-primary',
      ENVIAR: 'btn-primary',
      GUARDARPROGRESO: 'btn-secondary',
      SELECCIONAR: 'btn-primary',
      AÑADIRDOCUMENTO: 'btn-primary',
      FILTRAR: 'btn-primary',
      LIMPIARFILTROS: 'btn-secondary',
      ENVIARTRAMITE: 'btn-secondary',
      DOCUMENTACION: 'btn-secondary',
      PAGOS: 'btn-secondary',
      ELIMINAR: 'btn-primary',
      NUEVOTRAMITE: 'btn-primary',
      VOLVER: 'btn-secondary'
    };

    const iconos = {
      ACEPTAR: 'paper-plane',
      CANCELAR: 'ok',
      ENVIAR: 'backward',
      GUARDARPROGRESO: 'save',
      SELECCIONAR: null,
    }; // ['paper-plane', 'ok', 'backward', 'save', null];
    const secundarios = ['GUARDARPROGRESO'];

    const tipos = ['ENVIAR', 'ACEPTAR', 'CANCELAR', 'GUARDARPROGRESO', 'SELECCIONAR'];
    // const textos = ['Enviar', 'Aceptar', 'Cancelar', 'Guardar progreso', 'Seleccionar'];
    // const estilos = ['btn-primary', 'btn-primary', 'btn-primary', 'btn-secondary', 'btn-primary'];
    // this.el.nativeElement.innerText = textos[tipos.indexOf(this.tipo)];

    const texto = textos[this.tipo] || `(${this.tipo})`; // (tipos.indexOf(this.tipo) > -1 ? textos[tipos.indexOf(this.tipo)] : this.tipo;
    const tipoBoton = tiposBoton[this.tipo] || 'btn-primary';
    this.renderer.setAttribute(this.el.nativeElement, 'title', texto);
    this.el.nativeElement.innerText = texto; // textos[tipos.indexOf(this.tipo)];
    this.renderer.addClass(this.el.nativeElement, 'btn');
    this.renderer.addClass(this.el.nativeElement, tipoBoton);
    /*this.renderer.addClass(this.el.nativeElement, 'btn-primary');*/

    const icono = iconos[this.tipo] || null;
    if (icono) {
      const iconoEl = this.renderer.createElement('i');
      this.renderer.setStyle(iconoEl, 'margin-left', '4px');
      this.renderer.addClass(iconoEl, 'fa');
      this.renderer.addClass(iconoEl, `fa-${icono}`);
      this.renderer.appendChild(this.el.nativeElement, iconoEl);
    }

    const estilo = secundarios.indexOf(this.tipo) > -1 ? 'btn-secondary' : 'btn-primary';
    if (estilo) {
      this.renderer.addClass(this.el.nativeElement, `${estilo}`);
    } else {
      // al tener tipos sin definir se pone por defecto btn-primary
      this.renderer.addClass(this.el.nativeElement, `btn-primary`);
    }

  }

}
