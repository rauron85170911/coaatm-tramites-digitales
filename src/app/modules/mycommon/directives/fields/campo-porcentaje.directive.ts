import { Directive, Output, EventEmitter } from '@angular/core';

@Directive({
  // tslint:disable-next-line: directive-selector
  selector: '[ngModel][campoporcentaje]',
  // tslint:disable-next-line: no-host-metadata-property
  host: {
    '(input)': 'onInputChange($event)'
  }
})
export class CampoPorcentajeDirective {
  @Output() ngModelChange: EventEmitter<any> = new EventEmitter();
  value: any;

  onInputChange($event) {
    this.value = $event.target.value.replace(',', '.');
    if (isNaN(this.value)) {
      this.value = '';
    } else if (this.value > 100) {
      this.value = '';
    }


    this.ngModelChange.emit(this.value);
  }
}
