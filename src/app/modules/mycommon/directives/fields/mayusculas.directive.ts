import { Directive, Output, EventEmitter } from '@angular/core';

@Directive({
  // tslint:disable-next-line: directive-selector
  selector: '[ngModel][mayusculas]',
  // tslint:disable-next-line: no-host-metadata-property
  host: {
    '(input)': 'onInputChange($event)'
  }
})
export class MayusculasDirective {
  @Output() ngModelChange: EventEmitter<any> = new EventEmitter();
  value: any;

  onInputChange($event) {
    debugger;
    this.value = $event.target.value.toUpperCase();
    this.ngModelChange.emit(this.value);
  }
}
