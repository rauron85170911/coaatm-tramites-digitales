import { Directive, Output, EventEmitter, HostListener, ElementRef, Renderer2, Input } from '@angular/core';
import { NgModel } from '@angular/forms';
import { DecimalFormatPipe } from '../../pipes/decimal-format.pipe';

@Directive({
  // tslint:disable-next-line: directive-selector
  selector: '[ngModel][campodecimal]',
  // tslint:disable-next-line: no-host-metadata-property
  host: {
    '(input)': 'onInputChange($event)'
  }
})
export class CampoDecimalDirective {

  @Input() decimals: string= '1.0-2';
  @Output() ngModelChange: EventEmitter<any> = new EventEmitter();
  constructor(
    private elementRef: ElementRef,
    private ngModel: NgModel,
    private decimalFormatPipe: DecimalFormatPipe,
    private renderer: Renderer2) {

      this.el = this.elementRef.nativeElement;
      // this.renderer.setStyle(this.el, 'background-color', '#FABADA');
  }

  private el: HTMLInputElement;
  value: any;
  

  @HostListener('focus', ['$event.target.value'])
  onFocus(value) {
    // cuando coge el foco sustituimos los posibles puntos por coma
    //this.el.value = this.ngModel.viewModel; //Display the raw value on the model
  }

  @HostListener('blur', ['$event.target.value'])
  onBlur(value) {
    //this.el.value = this.decimalFormatPipe.transform(this.el.value, this.decimals);
  }

  onInputChange($event) {
    this.value = $event.target.value.replace(',', '.');
    if (isNaN(this.value)) {
      this.value = '';
    }
    this.ngModelChange.emit(this.value);
  }
}
