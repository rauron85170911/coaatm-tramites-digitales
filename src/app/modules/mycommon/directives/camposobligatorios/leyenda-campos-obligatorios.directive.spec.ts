import { LeyendaCamposObligatoriosDirective } from './leyenda-campos-obligatorios.directive';
import { Component } from '@angular/core';
import { Template } from '@angular/compiler/src/render3/r3_ast';
import { ComponentFixture, TestBed } from '@angular/core/testing';

@Component({
  template: `<div class="cont_formulario" [appLeyendaCamposObligatorios]='true'>`
})
class DivFakeComponent { }

describe('LeyendaCamposObligatoriosDirective', () => {
  let component: DivFakeComponent;
  let fixture: ComponentFixture<DivFakeComponent>;
  let campoEl: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DivFakeComponent, LeyendaCamposObligatoriosDirective]//,
      //providers: [ElementRef, Renderer2]
    }).compileComponents();
    fixture = TestBed.createComponent(DivFakeComponent);
    component = fixture.componentInstance;
    campoEl = fixture.nativeElement.firstChild;
    fixture.detectChanges();

  });

  it('Crea el elemento legend', () => {
    expect(campoEl.querySelector('legend')).toBeTruthy();
  });
});
