import { Directive, AfterViewInit, ElementRef, Renderer2, Input, OnChanges, SimpleChanges } from '@angular/core';

@Directive({
  selector: '[appBloqueObligatorio]'
})
export class BloqueObligatorioDirective implements AfterViewInit, OnChanges {


  @Input('appBloqueObligatorio') obligatorio: boolean;
  @Input() debug: boolean
  @Input() datos: any = null;
  constructor(private el: ElementRef, private renderer: Renderer2) {

  }

  ngAfterViewInit(): void {

    this.configurar();

  }
  ngOnChanges(changes: SimpleChanges): void {
   this.configurar();
  }

  private configurar() {
    if (this.debug) {
      console.log(this.datos);
    }
    const color = this.obligatorio ? '#ffffcc' : 'white';
    this.el.nativeElement.querySelectorAll('input').forEach(e => {
      this.renderer.setStyle(e, 'background-color', color);
    });

    this.el.nativeElement.querySelectorAll('textarea').forEach(e => {
      this.renderer.setStyle(e, 'background-color', color);
    });

    this.el.nativeElement.querySelectorAll('select').forEach(e => {
      this.renderer.setStyle(e, 'background-color', color);
    });

    this.el.nativeElement.querySelectorAll('.form-check').forEach(e => {
      this.renderer.setStyle(e, 'background-color', color);
    });
  }

}
