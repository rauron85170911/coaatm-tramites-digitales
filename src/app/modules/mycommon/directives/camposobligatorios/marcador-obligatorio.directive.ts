import {
  Directive,
  ElementRef,
  Input,
  OnInit,
  Renderer2,
  HostListener
  } from '@angular/core';

@Directive({
  selector: '[appMarcadorObligatorio]'
})
export class MarcadorObligatorioDirective implements OnInit {

  @Input('appMarcadorObligatorio') obligatorio: boolean;
  constructor(private el: ElementRef, private renderer: Renderer2) { }

  // @HostListener('click') onClick() {
  //   alert(this.el.nativeElement.innerText);
  // }

  ngOnInit(): void {

    const child = document.createElement('span');
    child.innerText = '*';
    this.renderer.setStyle(child, 'color', '#aa1e32');
    this.renderer.setStyle(child, 'margin-right', '3px');
    this.renderer.insertBefore(this.el.nativeElement, child, this.el.nativeElement.childNodes[0]);

    if (!this.obligatorio) {
      this.renderer.setStyle(child, 'visibility', 'hidden');
    }
  }


}
