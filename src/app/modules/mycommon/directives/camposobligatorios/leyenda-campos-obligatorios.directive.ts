import { Directive,
         ElementRef,
         Input,
         OnInit,
         Renderer2
} from '@angular/core';

@Directive({
  selector: '[appLeyendaCamposObligatorios]'
})
export class LeyendaCamposObligatoriosDirective implements OnInit {

  @Input('appLeyendaCamposObligatorios') tipos: string;

  constructor(private el: ElementRef, private renderer: Renderer2) {}

  ngOnInit(): void {

    const tipos = ['LITERAL', 'IMAGEN'];
    const child = document.createElement('legend');

    switch (this.tipos) {
      case 'LITERAL':
        console.log(`el valor de tipos es: ${this.tipos}`);
        child.innerText = '* Campos Obligatorios';
        this.renderer.addClass(child, 'literal');
        this.renderer.insertBefore(this.el.nativeElement, child, this.el.nativeElement.childNodes[0]);
        break;
      case 'IMAGEN':
        console.log(`el valor de tipos es: ${this.tipos}`);
        child.innerText = 'Campos Obligatorios';
        this.renderer.addClass(child, 'imagen');
        this.renderer.insertBefore(this.el.nativeElement, child, this.el.nativeElement.childNodes[0]);
        break;
      default:
        console.log(`el valor de tipos es: por defecto`);
        console.log(`el valor de tipos es: ${this.tipos}`);
        child.innerText = '* Campos Obligatorios';
        this.renderer.addClass(child, 'literal');
        this.renderer.insertBefore(this.el.nativeElement, child, this.el.nativeElement.childNodes[0]);
    }

  }

}
