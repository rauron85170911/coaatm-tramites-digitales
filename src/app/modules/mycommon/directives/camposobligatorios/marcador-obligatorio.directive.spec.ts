import { ElementRef, Renderer2, Component } from '@angular/core';
import { MarcadorObligatorioDirective } from './marcador-obligatorio.directive';
import { TestBed, ComponentFixture } from '@angular/core/testing';


@Component({
  template: `<label [appMarcadorObligatorio]="true">aaaa</label>`,
})
class CampoFakeComponent { }

describe('MarcadorObligatorioDirective', () => {
  let component: CampoFakeComponent;
  let fixture: ComponentFixture<CampoFakeComponent>;
  let campoEl: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CampoFakeComponent, MarcadorObligatorioDirective]//,
      //providers: [ElementRef, Renderer2]
    }).compileComponents();
    fixture = TestBed.createComponent(CampoFakeComponent);
    component = fixture.componentInstance;
    campoEl = fixture.nativeElement.firstChild;
    fixture.detectChanges();

  });
  // it('should create an instance', () => {
  //   const directive = new MarcadorObligatorioDirective(null, null);
  //   expect(directive).toBeTruthy();
  // });

  it('Crea en el dom el span con el asterisco', () => {
    
    expect(campoEl.textContent.startsWith('*')).toBeTruthy();
  });
});
