import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CollapseDirective } from './app-collapse.directive';
import { Component, Input, Injectable } from '@angular/core';
import { ComponentFixture, TestBed, tick, async, fakeAsync } from '@angular/core/testing';
import { AnimationBuilder } from '@angular/animations';
import { TitleCasePipe } from '@angular/common';

@Component({
  template: `<div [appCollapse]="colapsado"><p>Lorem ipsum</p></div>`,
})
class CapaFakeComponent {
  @Input() colapsado = true;
}


describe('CollapseDirective', () => {
  let component: CapaFakeComponent;
  let fixture: ComponentFixture<CapaFakeComponent>;
  let capaEl: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[BrowserAnimationsModule],
      declarations: [CollapseDirective, CapaFakeComponent]
    }).compileComponents();
    fixture = TestBed.createComponent(CapaFakeComponent);
    component = fixture.componentInstance;
    capaEl = fixture.nativeElement.firstChild;
  });
});
