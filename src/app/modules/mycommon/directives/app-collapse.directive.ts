import { Directive, AfterViewInit, ElementRef, Renderer2, Input, OnChanges, SimpleChanges, OnInit } from '@angular/core';
import { AnimationBuilder, animate, keyframes, style, AnimationPlayer } from '@angular/animations';

@Directive({
  selector: '[appCollapse]'
})
export class CollapseDirective implements OnInit, AfterViewInit, OnChanges {


  @Input('appCollapse') collapsed: boolean;

  public player: AnimationPlayer;

  constructor(private el: ElementRef, private renderer: Renderer2, private builder: AnimationBuilder) {

  }

  ocultacioninicial: any[] = [
    // style({ height: '*', overflow: 'hidden' }),
    style({ height: '100%', overflow: 'hidden'}),
    animate('0s ease-out', keyframes([
      style({ height: '0px' })
    ]))];

  animacionmostrar: any[] = [
    // style({ display: 'block', height: '0px', overflow: 'hidden', opacity: 0}),
    style({ display: 'block', height: '0px', opacity: 0 }),
    animate('0.5s ease-out',
      style({ height: '*', opacity: 1, display: 'block' })
    )];

  animacionocultar: any[] = [
    style({ height: '*', overflow: 'hidden' }),
    animate('0.5s ease-out', keyframes([
      style({ height: '0px', opacity: 0 })
    ]))];

  ngOnInit(): void {

  }
  ngAfterViewInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    //
    if (changes.collapsed && !changes.collapsed.firstChange) {
      this.animar(changes.collapsed.currentValue);
    } else {
      if (changes.collapsed.currentValue === true) {
        if (this.player) {
          this.player.destroy();
        }
        const factory = this.builder.build(this.ocultacioninicial);
        this.player = factory.create(this.el.nativeElement);
        //this.player.play();
      }
    }

    // this.renderer.setStyle(this.el.nativeElement, 'display', this.collapsed ? 'none' : 'block');
  }

  animar(ocultar) {
    if (this.player) {
      this.player.destroy();
    }
    const factory = this.builder.build(ocultar ? this.animacionocultar : this.animacionmostrar);
    this.player = factory.create(this.el.nativeElement);
    this.player.play();

    

  }

}
