import { environment } from 'src/environments/environment';
import { Injectable, EventEmitter } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable, Subject } from 'rxjs';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class GuiUtilsService {

  constructor(private spinner: NgxSpinnerService, private router: Router) { }

  // public Messages = new EventEmitter<any>();

  public Messages = new Subject();

  public TituloPrincipal$ = new Subject<string>();

  set TituloPrincipal(titulo: string) {
    this.TituloPrincipal$.next(titulo);

  }


  public ShowError(mensaje: string) {
    this.MostrarMensaje(mensaje, 'error');


  }
  public ShowSuccess(mensaje: string) {
    this.MostrarMensaje(mensaje, 'success');
  }
  public ShowInfo(mensaje: string) {
    this.MostrarMensaje(mensaje, 'info');
  }

  public ShowErrorPage() {
    const actual = this.router.routerState.snapshot.url;
    this.router.navigateByUrl(`error?previous=${actual}`);
  }


  // public ShowConfirmation(): Observable<bool> {


  // }


  private MostrarMensaje(mensaje: string, tipo: string) {

    this.Messages.next({
      mensaje,
      tipo
    });
    // this.Messages.emit({
    //   mensaje,
    //   tipo: 'error'
    // });
  }


  // public StartLoading() {
  //   this.spinner.show();
  // }
  // public StopLoading() {
  //   this.spinner.hide();
  //   console.log('Se oculta el spinner de loading');
  // }
}
