import { TestBed } from '@angular/core/testing';

import { PasosService } from './pasos.service';

describe('PasosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PasosService = TestBed.get(PasosService);
    expect(service).toBeTruthy();
  });
});
