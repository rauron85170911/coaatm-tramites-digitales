import { Injectable } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { EventData } from '../models/eventdata.model';
import { filter, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EventBusService {

  constructor() { }
  private subject$ = new Subject();

  emit(event: EventData) {
    this.subject$.next(event);
  }

  on(eventName: string, action: any): Subscription {
    return this.subject$.pipe(
      filter((e: EventData) => e.name === eventName),
      map((e: EventData) => e.data)).subscribe(action);
  }
}
