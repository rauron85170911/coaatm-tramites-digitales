import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PasosService {

  public PasoActivado$ = new Subject<string>();

  constructor() { }


  public ActivarPaso(paso: string){
    this.PasoActivado$.next(paso);
  }




}
