import { Validators } from '@angular/forms';
import { Injectable } from '@angular/core';
import { ErrorValidacion } from '../models/models.index';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ValidacionService {

  constructor() { }

  // public ErroresValidacion: Subject<ErrorValidacion> = new Subject<ErrorValidacion>();




  public OnErrorValidacion: Subject<ErrorValidacion> = new Subject<ErrorValidacion>();
  public OnComenzar: Subject<null> = new Subject<null>();
  public OnNavegar: Subject<ErrorValidacion> = new Subject<ErrorValidacion>();

  private contador = 0;


  public Inicializar() {
    this.contador = 0;
    this.OnComenzar.next();
  }

  public get Valido(): boolean {
    console.log(this.contador);
    return this.contador === 0;
  }

  /**
   * Para añadir un error de validación personalizado
   * @param descripcion
   * @param paso
   * @param referenciaCampo
   */
  public AddErrorValidacion(descripcion: string, paso: string, referenciaCampo?: string) {
    this.EmitirError({
      Paso: paso,
      Contenido: descripcion
    });
  }


  public ValidarRegex(descripcionCampo: string,
                      valor: any,
                      expresion: string, validarNulo: boolean, paso: string, referenciaCampo?: string) {
    const r: RegExp = new RegExp(expresion);

    if (!validarNulo && !valor) {
      return;
    }
    if (!r.test(valor)) {
      this.EmitirError({
        Paso: paso,
        Contenido: `El campo \'${descripcionCampo} no tiene un formato válido`
      });
    }

  }
  public ValidarRequerido(descripcionCampo: string, valor: any, paso: string, referenciaCampo?: string) {    
    if (valor === null) {
      this.EmitirError({
        Paso: paso,
        Contenido: `El campo \'${descripcionCampo}\' es obligatorio`
      });
    }
  }

  public ValidarMinimo(descripcionCampo: string, valor: any, minimo: number, paso: string, referenciaCampo?: string) {
    if (!!valor) {
      this.EmitirError({
        Paso: paso,
        Contenido: `El campo \'${descripcionCampo}\' debe ser mayor o igual a ${minimo}`
      });
    }
  }
  public ValidarMaximo(descripcionCampo: string, valor: any, maximo: number, paso: string, referenciaCampo?: string) {
    if (!!valor) {
      this.EmitirError({
        Paso: paso,
        Contenido: `El campo \'${descripcionCampo}\' debe ser menor o igual a  ${maximo}`
      });
    }
  }

  public ValidarRango(descripcionCampo: string, valor: any, minimo: number, maximo: number, paso: string, referenciaCampo?: string) {
    if (!isNaN(valor)) {
      if (Number(valor) < minimo || Number(valor) > maximo) {
        this.EmitirError({
          Paso: paso,
          Contenido: `El campo \'${descripcionCampo}\' debe estar comprendido entre ${minimo} y ${maximo}`
        });
      }
    }
  }

  public NavegarAError(errorValidacion: ErrorValidacion) {
    this.OnNavegar.next(errorValidacion);
  }



  private EmitirError(error: ErrorValidacion) {
    this.contador += 1;
    console.log(`El contador de errores está a ${this.contador}`);
    this.OnErrorValidacion.next(error);
  }



}
