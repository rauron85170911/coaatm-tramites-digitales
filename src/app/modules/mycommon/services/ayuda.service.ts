import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject, Observable, of } from 'rxjs';
import { map, catchError, tap, switchMap } from 'rxjs/operators';
import { ItemAyuda } from '../models/models.index';

@Injectable({
  providedIn: 'root'
})
export class AyudaService {

  indice: any = null;
  public Ayuda = new Subject<ItemAyuda>();
  constructor(private httpclient: HttpClient) { }


  /**Devuelve true si hay ayuda disponible para el id pasado */
  public AyudaDisponible(id: string): Observable<boolean> {

    return this.getIndice().pipe(
      switchMap((indice: any) => {
        const url = eval('indice.' + id);
        return url ? of(true) : of(false);
      })
    );
  }

  public ShowAyuda(id: string) {
    this.construirAyuda(id).subscribe(ayuda => {
      this.Ayuda.next(ayuda);
    });
  }

  private construirAyuda(id: string): Observable<ItemAyuda> {
    return this.getIndice().pipe(
      switchMap(indice => {
        const url = eval('indice.' + id);
        if (url) {
          return this.getAyudaFromUrl(url);
        } else {
          const ayuda: ItemAyuda = {
            Titulo: `Error en la ayuda`,
            Contenido: `No hay definida ayuda para la sección ${id}`
          };
          return of(ayuda);
          return of(null);
        }
      })
    );
  }

  private getIndice(): Observable<any> {
    if (!!this.indice) {
      return of(this.indice);
    } else {
      return this.httpclient.get('assets/ayuda/index.json').pipe(
        tap(response => {
          this.indice = response;
        })
      );
    }
  }

  private getAyudaFromUrl(url: string): Observable<ItemAyuda> {
    return this.httpclient.get(url, { responseType: 'text' }).pipe(
      map(response => {
        const ayuda: ItemAyuda = {
          Titulo: `Ayuda`,
          Contenido: '' + response
        };
        return ayuda;
      }),
      catchError((error) => {
        const ayuda: ItemAyuda = {
          Titulo: `Error en la ayuda`,
          Contenido: 'No se pudo cargar el fichero de ayuda'
        };
        return of(ayuda);
      }));
  }



}
