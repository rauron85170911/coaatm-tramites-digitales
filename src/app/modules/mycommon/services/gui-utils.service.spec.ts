import { TestBed, getTestBed, async, tick } from '@angular/core/testing';
import { Location } from '@angular/common';
import { GuiUtilsService } from './gui-utils.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Routes } from '@angular/router';
import { GenericErrorComponent } from 'src/app/pages/generic-error/generic-error.component';

describe('GuiUtilsService', () => {
  let injector: TestBed;
  let httpMock: HttpTestingController;
  let servicio: GuiUtilsService;
  let location: Location;
  
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [GuiUtilsService]
    });

    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
    servicio = injector.get(GuiUtilsService);
    location = TestBed.get(Location);

  });

  it('should be created', () => {
    expect(servicio).toBeTruthy();
  });

  it('Muestra error', async(() => {
    expect(servicio).toBeTruthy();
    servicio.Messages.subscribe((mensaje: any) => {
      expect(mensaje.tipo).toBe('error');
      expect(mensaje.mensaje).toBe('error');
    });
    servicio.ShowError('error');
  }));

  it('Muestra info', async(() => {
    expect(servicio).toBeTruthy();
    servicio.Messages.subscribe((mensaje: any) => {
      expect(mensaje.tipo).toBe('info');
      expect(mensaje.mensaje).toBe('informacion');
    });
    servicio.ShowInfo('informacion');
  }));

  it('Muestra sucess', async(() => {
    expect(servicio).toBeTruthy();
    servicio.Messages.subscribe((mensaje: any) => {
      expect(mensaje.tipo).toBe('success');
      expect(mensaje.mensaje).toBe('exito');
    });
    servicio.ShowSuccess('exito');
  }));

  


  //ya lo revisaremos
  // it('Redirige a pagina de error', async(() => {
  //   servicio.ShowErrorPage();
  //   tick(1000);
  //   expect(location.path()).toBe('/error');
  // }));

});
