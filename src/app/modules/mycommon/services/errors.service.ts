import { GuiUtilsService } from 'src/app/modules/mycommon/services/gui-utils.service';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ErrorsService {

  public Errores = new Subject<any>();
  constructor(private guiutils: GuiUtilsService) { }


  Registrar(error: any) {
    this.Errores.next(error);
  }

  RegistrarErrorHttp(error: any) {
    
    if (!environment.production) {
      this.Registrar(error);
      const mensaje = error.error || error.message;
      this.guiutils.ShowError(mensaje);
    }
    
    // if (!environment.production && error.error) {
    //   // mensaje += `. ${error.error.Message} ${error.error.ExceptionMessage}` ;
    //   mensaje += `. ${error.error}`;
    // }
    
    // this.ShowError(mensaje);
  }

  DepurarObjeto(objeto: any) {
    this.Errores.next(objeto);
  }


}
