import { TestBed, getTestBed, async } from '@angular/core/testing';

import { AyudaService } from './ayuda.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpErrorResponse } from '@angular/common/http';

describe('AyudaService', () => {
  let injector: TestBed;
  let httpMock: HttpTestingController;
  let servicio: AyudaService;

  const INDICEAYUDAFAKE: any = {
    certificados: {
      CC: 'assets/ayuda/CC.html',
      estados: 'assets/ayuda/estadoscertificado.html'
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [AyudaService]
    });

    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
    servicio = injector.get(AyudaService);


  });

  it('should be created', () => {
    // const service: AyudaService = TestBed.get(AyudaService);
    expect(servicio).toBeTruthy();
  });

  it('Se carga y lanza el evento de ayuda', async(() => {
    expect(servicio).toBeTruthy();

    servicio.Ayuda.subscribe( ayuda => {
      expect(ayuda.Titulo).toBe('Ayuda');
    });
    servicio.ShowAyuda('certificados.CC');

    const req = httpMock.expectOne(`assets/ayuda/index.json`);
    expect(req.request.method).toBe('GET');
    req.flush(INDICEAYUDAFAKE);
    const req2 = httpMock.expectOne(INDICEAYUDAFAKE.certificados.CC);
    httpMock.expectNone(INDICEAYUDAFAKE.certificados.estados);
    expect(req2.request.method).toBe('GET');
    req2.flush('Una ayuda de prueba');
    httpMock.verify();

  }));



  it('No pide el índice si ya está cargado', async(() => {
    servicio = injector.get(AyudaService);

    servicio.Ayuda.subscribe(ayuda => {
      expect(ayuda.Titulo).toBe('Ayuda');
    });
    httpMock.expectNone(`assets/ayuda/index.json`);
    servicio.indice = INDICEAYUDAFAKE;

    servicio.ShowAyuda('certificados.CC');
    const req = httpMock.expectOne(INDICEAYUDAFAKE.certificados.CC);
    req.flush('una ayuda fake');
    httpMock.verify();
  }));

  it('Si no encuentra url de ayuda en el índice también funciona', async(() => {
    servicio = injector.get(AyudaService);

    servicio.Ayuda.subscribe(ayuda => {
      expect(ayuda.Titulo).toBe('Error en la ayuda');
    });

    httpMock.expectNone(`assets/ayuda/index.json`);
    servicio.indice = INDICEAYUDAFAKE;
    servicio.ShowAyuda('certificados.CCC'); // hemos pedido una ayuda que no está en el índice
    httpMock.verify();
  }));


  /// revisar este test, no pierdo más tiempo de momento
  // it('Si falla al cargar la url de ayuda también funciona', async(() => {
  //   servicio = injector.get(AyudaService);
  //   servicio.Ayuda.subscribe(ayuda => {
  //     expect(ayuda.Titulo).toBe('Ayuda');
  //   });

  //   httpMock.expectNone(`assets/ayuda/index.json`);
  //   const req = httpMock.expectOne(`assets/ayuda/CC.html`);
  //   servicio.indice = INDICEAYUDAFAKE;
  //   servicio.ShowAyuda('certificados.CC');
  //   const mockErrorResponse = { status: 200, statusText: 'Ok' };
  //   req.flush('pepepepepe', mockErrorResponse);
  //   httpMock.verify();
  // }));







});
