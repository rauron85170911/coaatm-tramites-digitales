import { TestBed, async, getTestBed } from '@angular/core/testing';

import { ErrorsService } from './errors.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

describe('ErrorsService', () => {
  let injector: TestBed;
  
  let servicio: ErrorsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [ErrorsService]
    });

    injector = getTestBed();
    servicio = injector.get(ErrorsService);


  });

  it('should be created', () => {
    //const service: ErrorsService = TestBed.get(ErrorsService);
    expect(servicio).toBeTruthy();
  });

  it('Muestra un error http', async(() => {
    expect(servicio).toBeTruthy();
    servicio.Errores.subscribe((error: any) => {
      expect(error).toBeTruthy();
    });
    servicio.RegistrarErrorHttp({ message: 'aaaa' });
  }));
});
