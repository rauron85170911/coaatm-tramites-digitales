import { NgbDatepickerI18nDefault } from '@ng-bootstrap/ng-bootstrap/datepicker/datepicker-i18n';
import { PanelComponent } from './components/panel/panel.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MensajesComponent } from './components/mensajes/mensajes.component';
import { TamanioPipe } from './pipes/tamanio.pipe';
import { MarcadorObligatorioDirective } from './directives/camposobligatorios/marcador-obligatorio.directive';
import { LeyendaCamposObligatoriosDirective } from './directives/camposobligatorios/leyenda-campos-obligatorios.directive';
import { BotonDirective } from './directives/botones/boton.directive';
import { YesNoComponent } from './components/yes-no/yes-no.component';
import { YesNoButtonComponent } from './components/yes-no-button/yes-no-button.component';
import { FechaPipe } from './pipes/fecha.pipe';
import { CollapseDirective } from './directives/app-collapse.directive';
import { AyudaComponent } from './components/ayuda/ayuda.component';
import { PanelAyudaComponent } from './components/panel-ayuda/panel-ayuda.component';
import { PasoComponent } from './components/pasos/paso/paso.component';
import { MarcadorPasosComponent } from './components/pasos/marcador-pasos/marcador-pasos.component';
import { PanelErrorComponent } from './components/panel-error/panel-error.component';
import { BotonstickedDirective } from './directives/botones/botonsticked.directive';
import { SeccionComponent } from './components/secciones/seccion/seccion.component';
import { FadeInOutAnimationDirective } from './directives/fade-in-out-animation.directive';
import { DropzoneDirective } from './directives/dropzone.directive';
import { NgbModule, NgbDateAdapter, NgbDateNativeAdapter, NgbDateParserFormatter, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import { CustomFieldComponent } from './components/fields/custom-field/custom-field.component';
import { BloqueObligatorioDirective } from './directives/camposobligatorios/bloque-obligatorio.directive';
import { ValidacionComponent } from './components/validacion/validacion.component';
import { MaxValidatorDirective } from './directives/validations/max/max.directive';
import { RangeValidatorDirective } from './directives/validations/range/range.directive';
import { BotonIconoDirective } from './directives/botones/boton-icono.directive';
import { ConfirmacionComponent } from './components/confirmacion/confirmacion.component';
import { ConfirmacionDirective } from './directives/botones/confirmacion.directive';
import { SiNoPipe } from './pipes/si-no.pipe';
import { NifValidatorDirective } from './directives/validations/nif/nif-validator.directive';
import { TipoPersonaPipe } from './pipes/tipo-persona.pipe';
import { NgbDateESParserFormatter } from './formatters/NgbDateESParserFormatter';
import { DrowdownCardComponent } from './components/cards/drowdown-card/drowdown-card.component';
import { TableResponsiveDirective } from './directives/table-responsive.directive';
import { CustomcurrencyPipe } from './pipes/customcurrency.pipe';
import { PaginationComponent } from './components/pagination/pagination.component';
import { NgbDatepickerESI18n } from './i18n/ngbdatepickeresi18n';
import { PanelFijoComponent } from './components/panel-fijo/panel-fijo.component';
import { TabularCardComponent } from './components/cards/tabular-card/tabular-card.component';
import { PorcentajePipe } from './pipes/porcentaje.pipe';
import { SuperficiePipe } from './pipes/superficie.pipe';
import { DecimalFieldDirective } from './directives/fields/decimal-field.directive';
import { DecimalFormatPipe } from './pipes/decimal-format.pipe';
import { MayusculasDirective } from './directives/fields/mayusculas.directive';
import { CampoDecimalDirective } from './directives/fields/campo-decimal.directive';
import { CampoPorcentajeDirective } from './directives/fields/campo-porcentaje.directive';

@NgModule({
  declarations: [MensajesComponent,
    TamanioPipe,
    MarcadorObligatorioDirective,
    LeyendaCamposObligatoriosDirective,
    BotonDirective,
    YesNoComponent,
    YesNoButtonComponent,
    FechaPipe,
    CollapseDirective,
    AyudaComponent,
    PanelAyudaComponent,
    PasoComponent,
    MarcadorPasosComponent,
    PanelErrorComponent,
    PanelComponent,
    BotonstickedDirective,
    SeccionComponent,
    FadeInOutAnimationDirective,
    DropzoneDirective,
    CustomFieldComponent,
    BloqueObligatorioDirective,
    ValidacionComponent,
    MaxValidatorDirective,
    RangeValidatorDirective,
    BotonIconoDirective,
    ConfirmacionComponent,
    ConfirmacionDirective,
    SiNoPipe,
    NifValidatorDirective,
    TipoPersonaPipe,
    DrowdownCardComponent,
    TableResponsiveDirective,
    CustomcurrencyPipe,
    PaginationComponent,
    PanelFijoComponent,
    TabularCardComponent,
    PorcentajePipe,
    SuperficiePipe,
    DecimalFieldDirective,
    DecimalFormatPipe,
    MayusculasDirective,
    CampoDecimalDirective,
    CampoPorcentajeDirective
  ],
  imports: [
    CommonModule,
    NgbModule
  ],
  exports: [MensajesComponent,
    TamanioPipe,
    MarcadorObligatorioDirective,
    LeyendaCamposObligatoriosDirective,
    BotonDirective,
    YesNoComponent,
    YesNoButtonComponent,
    FechaPipe,
    CollapseDirective,
    AyudaComponent,
    PanelAyudaComponent,
    PanelErrorComponent,
    PasoComponent,
    MarcadorPasosComponent,
    PanelComponent,
    BotonstickedDirective,
    SeccionComponent,
    FadeInOutAnimationDirective,
    DropzoneDirective,
    CustomFieldComponent,
    BloqueObligatorioDirective,
    ValidacionComponent,
    MaxValidatorDirective,
    RangeValidatorDirective,
    BotonIconoDirective,
    ConfirmacionDirective,
    SiNoPipe,
    NifValidatorDirective,
    TipoPersonaPipe,
    DrowdownCardComponent,
    TableResponsiveDirective,
    CustomcurrencyPipe,
    PaginationComponent,
    PanelFijoComponent,
    TabularCardComponent,
    PorcentajePipe,
    SuperficiePipe,
    DecimalFieldDirective,
    DecimalFormatPipe,
    MayusculasDirective,
    CampoDecimalDirective,
    CampoPorcentajeDirective
  ],
  providers: [
    { provide: NgbDateAdapter, useClass: NgbDateNativeAdapter },
    { provide: NgbDateParserFormatter, useClass: NgbDateESParserFormatter },
    DecimalFormatPipe
    // {provide: NgbDatepickerI18n, useClass: NgbDatepickerESI18n}
  ],
  entryComponents: [ConfirmacionComponent]
})
export class MyCommonModule { }
