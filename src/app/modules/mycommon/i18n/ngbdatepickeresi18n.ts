import { Injectable } from '@angular/core';
import { NgbDateStruct, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import { NgbDatepickerI18nDefault } from '@ng-bootstrap/ng-bootstrap/datepicker/datepicker-i18n';

@Injectable()
export class NgbDatepickerESI18n extends NgbDatepickerI18n {
    getWeekdayShortName(weekday: number): string {
        const weekdays: string[] = ['L', 'M', 'X', 'J', 'V', 'S', 'D'];
        return weekdays[weekday - 1];

    }
    getMonthShortName(month: number, year?: number): string {
        const months: string[] = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];
        return months[month - 1];
    }
    getMonthFullName(month: number, year?: number): string {
        const months: string[] =
        ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        return months[month - 1];
    }
    getDayAriaLabel(date: NgbDateStruct): string {
       return  `${date.day}-${date.month}-${date.year}`;
    }


}
