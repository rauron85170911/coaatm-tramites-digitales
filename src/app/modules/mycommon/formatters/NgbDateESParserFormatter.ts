import { Injectable } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

@Injectable()
export class NgbDateESParserFormatter extends NgbDateParserFormatter {
    parse(value: string): NgbDateStruct { // parse receive your string dd/mm/yyy
        // return a NgbDateStruct
        // calculate year,month and day from "value"
        if (!!value) {
            const p = value.split('/');
            const year = Number(p[2]);
            const month = Number(p[1]);
            const day = Number(p[0]);


            return { year, month, day };
        }

    }

    format(date: NgbDateStruct): string { // receive a NgbDateStruct
        // return a string
        if (!!date) {
            return '' + date.day + '/' + date.month + '/' + date.year;
        } else {
            return '';
        }
    }
}
