import { Pipe, PipeTransform } from '@angular/core';
import { DecimalPipe, formatNumber } from '@angular/common';

@Pipe({
  name: 'tamanio'
})
export class TamanioPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const kb = 1024;
    const mb = kb * kb;

    if (!value) { return '';}
    let unidad = 'b';
    if (value > mb) {
      unidad = 'Mb';
      value = value / mb;
    } else if (value > kb) {
      unidad = 'Kb';
      value = value / kb;
    }

    return `${Math.round(value)} ${unidad}`;
  }

}
