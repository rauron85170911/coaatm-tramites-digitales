import { TamanioPipe } from './tamanio.pipe';

describe('TamanioPipe', () => {
  it('create an instance', () => {
    const pipe = new TamanioPipe();
    expect(pipe).toBeTruthy();
  });


  it('Traduce a bytes', () => {
    const pipe = new TamanioPipe();
    expect(pipe.transform(100)).toBe('100 b');
  });


  it('Traduce a Kbytes', () => {
    const pipe = new TamanioPipe();
    expect(pipe.transform(1500)).toBe('1 Kb');
  });


  it('Traduce a Mbytes', () => {
    const pipe = new TamanioPipe();
    expect(pipe.transform(1500000)).toBe('1 Mb');
  });

});
