import { Pipe, PipeTransform } from '@angular/core';
import { formatNumber } from '@angular/common';
@Pipe({
  name: 'superficie'
})
export class SuperficiePipe implements PipeTransform {

  transform(value: number, args?: any): any {
    return formatNumber(value, 'es', '1.0-2') + ' m2';


  }

}
