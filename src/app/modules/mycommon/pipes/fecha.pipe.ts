import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fecha'
})
export class FechaPipe implements PipeTransform {

  transform(value: any, args?: any): any {


    const f: Date = new Date(value);
    return `${f.getDate()}/${f.getMonth() + 1}/${f.getFullYear()}`;
  }

}
