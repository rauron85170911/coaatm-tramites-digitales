import { Pipe, PipeTransform, Inject, LOCALE_ID } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Pipe({ name: 'decimalFormatPipe' })
export class DecimalFormatPipe implements PipeTransform {

    constructor(@Inject(LOCALE_ID) private locale: string) { }

    transform(value: any, args: string) {
        if (!value) { return ''; }
        
        const pipe = new DecimalPipe(this.locale);
        if (isNaN(value)) { return pipe.transform('0', args); }
        return pipe.transform(value, args);
    }
}
