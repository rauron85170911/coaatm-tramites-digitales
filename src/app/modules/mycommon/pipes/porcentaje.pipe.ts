import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'porcentaje'
})
export class PorcentajePipe implements PipeTransform {

  transform(value: number, args?: any): any {
    if (value) {
      return value + '%';
    } else {
      return '0%';
    }
  }

}
