import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'tipoPersona'
})
export class TipoPersonaPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (!value) { return '' }
    return value === 'J' ? 'Jurídica' : 'Física';
  }

}
