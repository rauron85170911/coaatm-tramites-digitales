import { FechaPipe } from './fecha.pipe';

describe('FechaPipe', () => {
  it('create an instance', () => {
    const pipe = new FechaPipe();
    expect(pipe).toBeTruthy();
  });


  it('Comprobar que siempre devuelve null', () => {
    const pipe = new FechaPipe();
    expect(pipe.transform(null)).toBeNull();
  });
});
