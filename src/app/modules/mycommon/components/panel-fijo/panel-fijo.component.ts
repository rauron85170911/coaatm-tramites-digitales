import { Component, OnInit, ViewChild, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Component({
  selector: 'app-panel-fijo',
  templateUrl: './panel-fijo.component.html',
  styleUrls: ['./panel-fijo.component.scss']
})
export class PanelFijoComponent implements OnInit {

  @ViewChild('contenedorpanelfijo', null) contenedorpanelfijo: ElementRef;
  constructor(private renderer: Renderer2) { }
  
  @HostListener('window:scroll', ['$event'])
  doSomethingOnWindowsScroll($event: Event) {
    // const scrollOffset = $event.target.children[0].scrollTop;
    const ejeY = window.pageYOffset;
    // TODO: buscar otra forma de no usar el objeto window
    const sticky = this.contenedorpanelfijo.nativeElement.offsetHeight;
    // console.log('el scroll vale: ' + scrollOffset);
    if (ejeY > 0) {
      // console.log('es sticky');
      this.renderer.addClass(this.contenedorpanelfijo.nativeElement, 'sticky');
    } else {
      // console.log('no es sticky');
      this.renderer.removeClass(this.contenedorpanelfijo.nativeElement, 'sticky');
    }
  }
  ngOnInit() {
  }




}
