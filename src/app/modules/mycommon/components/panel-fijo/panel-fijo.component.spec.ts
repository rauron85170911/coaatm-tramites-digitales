import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelFijoComponent } from './panel-fijo.component';

describe('PanelFijoComponent', () => {
  let component: PanelFijoComponent;
  let fixture: ComponentFixture<PanelFijoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelFijoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelFijoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
