import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-custom-field',
  templateUrl: './custom-field.component.html',
  styleUrls: ['./custom-field.component.scss']
})
export class CustomFieldComponent implements OnInit {

  @Input() label = '';
  @Input() obligatorio:boolean;
  constructor() { }

  ngOnInit() {
  }

}
