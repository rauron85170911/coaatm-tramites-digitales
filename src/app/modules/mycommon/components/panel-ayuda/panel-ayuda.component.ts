import { Component, OnInit, HostListener } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';
import { Router } from '@angular/router';
import { AyudaService } from '../../services/ayuda.service';
import { ItemAyuda } from '../../models/models.index';

@Component({
  selector: 'app-panel-ayuda',
  templateUrl: './panel-ayuda.component.html',
  styleUrls: ['./panel-ayuda.component.scss'],
  animations: [
    trigger(
      'inOutAnimation',
      [
        transition(
          ':enter',
          [
            style({ opacity: 0, width: '0px' }),
            animate('0.2s ease-out',
              style({ width: '*', opacity: 1 })
            )
          ]
        ),
        transition(
          ':leave',
          [
            style({ opacity: 1, overflow: 'hidden' }),
            animate('0.2s ease-out',
              style({ opacity: 0, width: '0px' })
            )
          ]
        )
      ]
    )
  ]
})
export class PanelAyudaComponent implements OnInit {

  constructor(private router: Router, private ayudaService: AyudaService) { }

  ayuda: ItemAyuda = null;

  @HostListener('window:keyup', ['$event'])
  keyevent(event: KeyboardEvent) {
    if (event.code === 'Escape') {
      this.ayuda = null;
    }
  }
  ngOnInit() {
    this.router.events.subscribe(e => {
      // lo unico que hacemos es ocultar el componente en cualquier cambio
      // en la ruta para que el posible mensaje o error no se muestre más
      this.ayuda = null;
    });

    this.ayudaService.Ayuda.subscribe(ayuda => {
      this.ayuda = ayuda;
      /*setTimeout(() => {
        this.data = null;
      }, 5000);*/
    });
  }

}