import { Component, OnInit, OnChanges, SimpleChanges, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-yes-no-button',
  templateUrl: './yes-no-button.component.html',
  styleUrls: ['./yes-no-button.component.scss']
})
export class YesNoButtonComponent implements OnInit, OnChanges {
  
  @Input() valor = false;
  @Output() cambio: EventEmitter<boolean> = new EventEmitter<boolean>();
  constructor() { }

  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.valor.firstChange) {

    }
  }
  toggle() {

    this.valor = !this.valor;
    this.cambio.emit(this.valor);
  }
}
