import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrowdownCardComponent } from './drowdown-card.component';

describe('DrowdownCardComponent', () => {
  let component: DrowdownCardComponent;
  let fixture: ComponentFixture<DrowdownCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrowdownCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrowdownCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
