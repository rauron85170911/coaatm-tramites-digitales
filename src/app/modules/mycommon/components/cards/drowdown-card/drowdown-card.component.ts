import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { trigger, transition, style, animate, state } from '@angular/animations';

@Component({
  selector: 'app-drowdown-card',
  templateUrl: './drowdown-card.component.html',
  styleUrls: ['./drowdown-card.component.scss'],
  animations:
    [
      trigger(
        'dropdown',
        [
          state('open', style({
            opacity: 1, height: '*', overflow: 'hidden'
          })),
          state('closed', style({
            opacity: 1, height: '0px', overflow: 'hidden', padding: '0px'
          })),
          transition(
            '* => open',
            [
              animate('0.3s ease-in')
            ]
          ),
          transition(
            '* => closed',
            [
              animate('0.3s ease-in')
            ]
          )
        ]
      )
    ]
})
export class DrowdownCardComponent implements OnInit, OnChanges {


  @Input() seccionayuda: string = null;
  @Input() titulo: string = 'Sin título';
  @Input() icono: string;
  @Input() ocultadoinicialmente = true;
  constructor() { }

  isCollapsed = true;
  ngOnInit() {
    this.isCollapsed = this.ocultadoinicialmente;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.ocultadoinicialmente && changes.ocultadoinicialmente.isFirstChange) {

      this.isCollapsed = changes.ocultadoinicialmente.currentValue;
    }
  }


}
