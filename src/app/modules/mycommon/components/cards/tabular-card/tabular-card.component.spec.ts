import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabularCardComponent } from './tabular-card.component';

describe('TabularCardComponent', () => {
  let component: TabularCardComponent;
  let fixture: ComponentFixture<TabularCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabularCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabularCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
