import { Component, OnInit, forwardRef, Output, EventEmitter } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';


@Component({
  selector: 'app-yes-no',
  templateUrl: './yes-no.component.html',
  styleUrls: ['./yes-no.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => YesNoComponent),
      multi: true
    }
  ]
})
export class YesNoComponent implements ControlValueAccessor {
  
  @Output() pulsado: EventEmitter<boolean> = new EventEmitter<boolean>();
  val = false;
  interno = false;
  onChange: any = (valor: boolean) => {
    
    this.pulsado.emit(valor);
  }
  onTouch: any = () => {};

  set value(val) {
    if (val !== undefined && this.val !== val) {
      this.val = val;
      this.onChange(val);
      this.onTouch(val);
    }
  }


  writeValue(value: any): void {
    this.value = value;
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  toggle() {
    this.writeValue(!this.val);
  }
  constructor() { }



}
