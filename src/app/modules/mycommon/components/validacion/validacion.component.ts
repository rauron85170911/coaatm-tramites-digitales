import { PasosService } from './../../services/pasos.service';
import { Subscription } from 'rxjs';
import { ErrorValidacion } from './../../models/models.index';
import { ValidacionService } from './../../services/validacion.service';
import { Component, OnInit, OnDestroy, HostListener, Output, EventEmitter } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';
import { Router } from '@angular/router';


@Component({
  selector: 'app-validacion',
  templateUrl: './validacion.component.html',
  styleUrls: ['./validacion.component.scss'],
  animations: [
    trigger(
      'inOutAnimation',
      [
        transition(
          ':enter',
          [
            style({ opacity: 0, height: '0px', 'padding-top': '0px', 'padding-bottom': '0px', overflow: 'hidden' }),
            animate('0.5s ease-out',
              style({ opacity: 1, height: '*', 'padding-top': '*', 'padding-bottom': '*' }))
          ]
        ),
        transition(
          ':leave',
          [
            style({ opacity: 1, overflow: 'hidden', 'padding-top': '*', 'padding-bottom': '*', height: '*' }),
            animate('0.3s ease-out',
              style({ opacity: 0, 'padding-top': '0px', 'padding-bottom': '0px', height: '0px', 'margin-top': 0, 'margin-bottom': 0 }))
          ]
        )
      ]
    )
  ]
})
export class ValidacionComponent implements OnInit, OnDestroy {

  @Output() reverificar: EventEmitter<null> = new EventEmitter<null>();
  constructor(private validacionService: ValidacionService, private pasosService: PasosService, private router: Router) { }

  data: ErrorValidacion[] = [];
  estado = 'oculto';

  subscripcion1: Subscription = null;
  subscripcion2: Subscription = null;

  @HostListener('window:keyup', ['$event'])
  keyevent(event: KeyboardEvent) {
    if (event.code === 'Escape') {
      this.data = [];
    }
  }
  ngOnInit() {

    this.router.events.subscribe(e => {
      // lo unico que hacemos es ocultar el componente en cualquier cambio
      // en la ruta para que el posible mensaje o error no se muestre más
      this.data = null;
    });




    this.subscripcion1 = this.validacionService.OnErrorValidacion.subscribe(data => {
      this.data = this.data || [];
      this.data.push(data);
      this.estado = 'visible';
    });

    this.subscripcion2 = this.validacionService.OnComenzar.subscribe(() => {
      this.data = [];
    });



  }

  ngOnDestroy(): void {
    this.subscripcion1.unsubscribe();
    this.subscripcion2.unsubscribe();
  }
  navegar(item) {
    this.validacionService.NavegarAError(item);
    debugger;
    if (item.Paso) {
      this.pasosService.ActivarPaso(item.Paso);
    }
  }
  eliminar(item) {
    this.data = this.data.filter(i => i !== item);
    this.data = [];
  }

  eliminarTodas() {
    this.estado = 'oculto';
    this.data = [];
  }

}
