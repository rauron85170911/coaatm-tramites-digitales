import { ErrorsService } from './../../services/errors.service';
import { Component, OnInit, HostListener } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';
import { Router } from '@angular/router';

@Component({
  selector: 'app-panel-error',
  templateUrl: './panel-error.component.html',
  styleUrls: ['./panel-error.component.scss'],
  animations: [
    trigger(
      'inOutAnimation',
      [
        transition(
          ':enter',
          [
            style({ opacity: 0, width: '0px' }),
            animate('0.2s ease-out',
              style({ width: '*', opacity: 1})
            )
          ]
        ),
        transition(
          ':leave',
          [
            style({ opacity: 1, overflow: 'hidden' }),
            animate('0.2s ease-out',
                style({ opacity: 0, width: '0px'})
            )
          ]
        )
      ]
    )
  ]
})
export class PanelErrorComponent implements OnInit {
  ultimoerror: any = null;
  constructor(private router: Router, private errorService: ErrorsService) { }

  @HostListener('window:keyup', ['$event'])
  keyevent(event: KeyboardEvent) {
    if (event.code === 'Escape') {
      this.ultimoerror = null;
    }
  }
  ngOnInit() {
    this.router.events.subscribe(e => {
      // lo unico que hacemos es ocultar el componente en cualquier cambio
      // en la ruta para que el posible mensaje o error no se muestre más
      this.ultimoerror = null;
    });
    this.errorService.Errores.subscribe(error => {
      this.ultimoerror = error;
      // setTimeout(() => {
      //   this.ultimoerror = null;
      // }, 15000);
    });

  }

}
