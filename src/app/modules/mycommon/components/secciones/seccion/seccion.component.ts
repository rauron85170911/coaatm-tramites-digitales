import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-seccion',
  templateUrl: './seccion.component.html',
  styleUrls: ['./seccion.component.scss']
})
export class SeccionComponent implements OnInit {

  @Input() titulo = 'Sin establecer';
  @Input() ayuda = null;
  constructor() { }

  ngOnInit() {
  }

}
