import { Component, OnInit, HostListener } from '@angular/core';
import { GuiUtilsService } from '../../services/gui-utils.service';
import { trigger, transition, style, state, animate, keyframes } from '@angular/animations';
import { Router } from '@angular/router';
@Component({
  selector: 'app-mensajes',
  templateUrl: './mensajes.component.html',
  styleUrls: ['./mensajes.component.scss'],
  animations: [
    trigger(
      'inOutAnimation',
      [
        transition(
          ':enter',
          [
            style({ opacity: 0, height: '0px', 'padding-top': '0px', 'padding-bottom': '0px', overflow: 'hidden' }),
            animate('0.3s ease-out',
              style({ opacity: 1, height: '*', 'padding-top': '*', 'padding-bottom': '*' }))
          ]
        ),
        transition(
          ':leave',
          [
            style({ opacity: 1, overflow: 'hidden', 'padding-top': '*', 'padding-bottom': '*', height: '*' }),
            animate('0.3s ease-out',
              style({ opacity: 0, 'padding-top': '0px', 'padding-bottom': '0px', height: '0px', 'margin-top': 0, 'margin-bottom': 0 }))
          ]
        )
      ]
    )
  ]
})
export class MensajesComponent implements OnInit {

  data: any[] = [];
  estado = 'oculto';

  @HostListener('window:keyup', ['$event'])
  keyevent(event: KeyboardEvent) {
    if (event.code === 'Escape') {
      this.data = null;
    }
  }
  constructor(private guiutils: GuiUtilsService, private router: Router) { }

  ngOnInit() {

    this.router.events.subscribe(e => {
      // lo unico que hacemos es ocultar el componente en cualquier cambio
      // en la ruta para que el posible mensaje o error no se muestre más
      this.data = null;
    });

    this.guiutils.Messages.subscribe(data => {

      this.data = this.data || [];
      this.data.push(data);
      this.estado = 'visible';
      /*setTimeout(() => {
        this.data = null;
      }, 5000);*/
    });
  }

  eliminar(item) {
    this.data = this.data.filter(i => i !== item);
  }

}
