import { AyudaService } from './../../services/ayuda.service';
import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-ayuda',
  templateUrl: './ayuda.component.html',
  styleUrls: ['./ayuda.component.scss']
})
export class AyudaComponent implements OnInit, OnChanges {


  @Input() seccion: string = null;

  constructor(public ayudaService: AyudaService) { }
  disponible = false;
  ngOnInit() {
  }
  ngOnChanges(changes: SimpleChanges): void {

    if (changes.seccion && changes.seccion.currentValue) {
      this.ayudaService.AyudaDisponible(changes.seccion.currentValue).subscribe(disponible => {
        this.disponible = disponible;
      });
    }

  }

}



