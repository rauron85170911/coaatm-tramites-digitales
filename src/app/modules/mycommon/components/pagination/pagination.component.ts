import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  @Input() page;
  @Output() pageChange: EventEmitter<number> = new EventEmitter<number>();
  @Input() pageSize;
  @Input() collectionSize;
  constructor() { }

  ngOnInit() {
  }

}
