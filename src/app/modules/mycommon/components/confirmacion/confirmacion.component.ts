import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { trigger, transition, style, animate } from '@angular/animations';

@Component({
  selector: 'app-confirmacion',
  templateUrl: './confirmacion.component.html',
  styleUrls: ['./confirmacion.component.scss'],
  animations:
  [
    trigger(
      'fadeinout',
      [
        transition(
          ':enter',
          [
            style({ opacity: 0, height: '0px', overflow: 'hidden' }),
            animate('0.2s ease-in',
              style({ opacity: 1, height: '*' })
            )
          ]
        ),
        transition(
          ':leave',
          [
            style({ opacity: 1, overflow: 'hidden' }),
            animate('0.3s ease-in',
              style({ opacity: 0, height: '0px' })
            )
          ]
        )
      ]
    )
  ]
})
export class ConfirmacionComponent implements OnInit {

  @Input() pregunta: string = '¿Está seguro?';
  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {

  }

}
