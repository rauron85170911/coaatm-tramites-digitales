import { PasosService } from './../../../services/pasos.service';
import { Component, OnInit, Input, Output, EventEmitter, TemplateRef, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-paso',
  templateUrl: './paso.component.html',
  styleUrls: ['./paso.component.scss'],
  animations:
    [
      trigger(
        'inOutAnimation',
        [
          transition(
            ':enter',
            [
              style({ opacity: 0, height: '0px' }),
              animate('0.3s ease-out',
                style({ height: '*', opacity: 1 })
              )
            ]
          ),
          transition(
            ':leave',
            [
              style({ opacity: 1, height: '*' }),
              animate('0.3s ease-out',
                style({ opacity: 0, height: '0px' })
              )
            ]
          )
        ]
      )
    ]
})
export class PasoComponent implements OnInit {

  @Input() activo: string = null;
  @Input() titulo: string = null;
  @Input() paso: any = null;
  @Input() total: string = null;
  // @Input() indicador: string = null;
  @Input() plantilla;
  @Output() activar: EventEmitter<string> = new EventEmitter<string>();
  @ViewChild('#sindatos', { static: false }) sindatos;
  @ViewChild('capacontenedorapaso', null) capacontenedorapaso: ElementRef;
  constructor(private pasosService: PasosService, private render: Renderer2) { }

  ngOnInit() {
    this.pasosService.PasoActivado$.subscribe(paso => {      
      this.activo = paso;
    });

    this.pasosService.PasoActivado$.pipe(
      filter(paso => paso === this.paso.indicador)
    ).subscribe(paso => {
      //se ha activado el paso en el que estamos

      //vamos a hacer la 'trampa' del settimeout. TODO Buscar una alternativa
      setTimeout(() => {
        const alto = this.capacontenedorapaso.nativeElement.getBoundingClientRect().height;
        const indice = Number(paso) - 1;
        const offset = 25;
        const posy = (indice * alto) + offset * (indice === 0 ? 0 : 1);
        // this.capacontenedorapaso.nativeElement.getBoundingClientRect().y;
        // this.element.nativeElement.
        // console.log(`${this.capacontenedorapaso.nativeElement.innerText}<--> ${posy}<-->${window.scrollY}`);
        const options: ScrollToOptions = {
          top: posy
        };
        window.scrollTo(options);
      }, 1);





    });

  }

  toggle() {
    if (this.paso.enabled && this.activo !== this.paso.indicador) {
      this.pasosService.ActivarPaso(this.paso.indicador);
      //this.activar.emit(this.paso.indicador);
    }
  }

}
