import { Component, OnInit, Input, Output, EventEmitter, HostListener, ElementRef, Renderer2, ViewChild } from '@angular/core';
import { PasosService } from './../../../services/pasos.service';
import { trigger, transition, style, animate } from '@angular/animations';

@Component({
  selector: 'app-marcador-pasos',
  templateUrl: './marcador-pasos.component.html',
  styleUrls: ['./marcador-pasos.component.scss'],
  animations: [
    trigger(
      'inOutAnimation',
      [
        transition(
          ':enter',
          [
            style({ opacity: 0, width: '0px' }),
            animate('0.3s ease-out',
              style({ width: '*', opacity: 1 })
            )
          ]
        ),
        transition(
          ':leave',
          [
            style({ opacity: 1, width: '*' }),
            animate('0.3s ease-out',
              style({ opacity: 0, width: '0px' })
            )
          ]
        )
      ]
    )
  ]
})
export class MarcadorPasosComponent implements OnInit {

  // @ViewChild('marcadorPasos', null) marcadorPasos: ElementRef;
  @Input() pasos: any[] = [];
  @Input() activo: string = null;
  @Input() ayuda: string = null;
  @Output() activar: EventEmitter<string> = new EventEmitter<string>();

  constructor(private renderer: Renderer2, private pasosService: PasosService) { }

  ngOnInit() {
    this.pasosService.PasoActivado$.subscribe(paso => {
      this.activo = paso;
    });
  }

  goto(paso: any) {
    if (paso.enabled && paso.indicador !== this.activo) {
      this.pasosService.ActivarPaso(paso.indicador);
      // this.activar.emit(paso.indicador);
    }

  }

  /* Funcionalidad para hacer sticky el marcador de pasos  */
  // @HostListener('window:scroll', ['$event'])
  // doSomethingOnWindowsScroll($event: Event) {
  //   // const scrollOffset = $event.target.children[0].scrollTop;
  //   const ejeY = window.pageYOffset;
  //   // TODO: buscar otra forma de no usar el objeto window
  //   const sticky = this.marcadorPasos.nativeElement.offsetHeight;
  //   // console.log('el scroll vale: ' + scrollOffset);
  //   if (ejeY > 0) {
  //     // console.log('es sticky');
  //     this.renderer.addClass(this.marcadorPasos.nativeElement, 'sticky');
  //   } else {
  //     // console.log('no es sticky');
  //     this.renderer.removeClass(this.marcadorPasos.nativeElement, 'sticky');
  //   }

  // }

}
