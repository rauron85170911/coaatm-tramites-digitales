import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarcadorPasosComponent } from './marcador-pasos.component';

describe('MarcadorPasosComponent', () => {
  let component: MarcadorPasosComponent;
  let fixture: ComponentFixture<MarcadorPasosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarcadorPasosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarcadorPasosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
