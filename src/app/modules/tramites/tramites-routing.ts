import { NuevoTramiteComponent } from './components/nuevo-tramite/nuevo-tramite.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './pages/index/index.component';
// import { IndexComponent as IndexAltaIntervencionComponent} from './altaintervencion/index/index.component';
import { AuthGuard } from 'src/app/guards/auth.guard';
import { HomeComponent } from './pages/home/home.component';
const routes: Routes = [
  {
    path: 'tramites', component: IndexComponent, canActivate: [AuthGuard], canActivateChild: [AuthGuard],
          data: { titulo: 'Trámites' },
    children : [
        // {path: '', redirectTo: 'altaintervencion', pathMatch: 'full'},
        {path: '', component: HomeComponent},
        {path: 'nuevo', component: NuevoTramiteComponent, data: {subtitulo: 'Nuevo trámite digital'}}
    ]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TramitesRoutingModule { }
