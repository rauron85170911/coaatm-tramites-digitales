import { EstadoTramitePipe } from './../pipes/estado-tramite.pipe';
import { Directive, ElementRef, Input, Renderer2, OnInit } from '@angular/core';

@Directive({
  selector: '[appEstadoTramite]',
  providers: [EstadoTramitePipe]
})

export class EstadoTramiteDirective implements OnInit {

  @Input('appEstadoTramite') tipoEstado: string;

  constructor(private el: ElementRef, private renderer: Renderer2, private estadopipe: EstadoTramitePipe) { }

  ngOnInit(): void {

    // Estados = ['AN','EL', 'EN', 'FI', 'IN', 'VP', 'VT', 'PP'];
    // descripcion = ['ANULADO','ELABORADO','ENVIADO','FINALIZADO','INCIDENCIAS','VALIDADO PARCIAL','VALIDADO TOTAL','PTE. PAGO']
    const colores = {
      AN: '#EBEBEB',
      EL: '#4A87D9',
      EN: '#01439B',
      FI: '#498208',
      IN: '#FF0808',
      VP: '#FF9100',
      VT: '#68CF19',
      PP: '#AA1E32'
    };

    const span = this.renderer.createElement('span');

    this.estadopipe.transform(this.tipoEstado).subscribe(descripcion => {

      span.innerText = this.tipoEstado;

      this.renderer.setAttribute(this.el.nativeElement, 'title', descripcion);
      this.el.nativeElement.innerText = descripcion;
      this.renderer.appendChild(this.el.nativeElement, span);
    });

    const color = colores[this.tipoEstado] || '#EBEBEB' ;

    this.renderer.addClass(this.el.nativeElement, 'contenedor-estado-tramites');
    this.renderer.setStyle(span, 'background-color', color);
    /* this.renderer.setStyle(this.el.nativeElement, 'background-color', color); */



  }

}

