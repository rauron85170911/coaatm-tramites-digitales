import { Tramite2RouteService } from './../../core/services/tramite2-route.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { ResultadoAccion } from '../../core/models/models.index';
import { Tramite } from '../models/models.index';

@Injectable({
  providedIn: 'root'
})
export class BackendTramitesService {

  constructor(private http: HttpClient, private t2r: Tramite2RouteService) { }


  Buscar(filtro: any): Observable<any[]> {
    const url = `${environment.apiUrlBase}/tramites/`;
    return this.http.post(url, filtro).pipe(
      map((response: any) => response)
    );
  }

  GetDetalle(idsolicitud: number) {
    const url = `${environment.apiUrlBase}/tramites/${idsolicitud}`;
    return this.http.get(url).pipe(
      map((response: any) => response)
    );
  }

  GetPagos(idsolicitud: number) {
    const url = `${environment.apiUrlBase}/tramites/${idsolicitud}/pagos`;
    return this.http.get(url).pipe(
      map((response: any) => response)
    );
  }


  BorrarTramite(idsolicitud: number) {
    const url = `${environment.apiUrlBase}/tramites/${idsolicitud}`;
    return this.http.delete(url).pipe(
      map((response: any) => response)
    );
  }

  EnviarTramite(idsolicitud: number) {
    const url = `${environment.apiUrlBase}/tramites/${idsolicitud}/enviar`;
    return this.http.put(url, null).pipe(
      map((response: any) => response)
    );
  }

  CrearRenunciaIntervencion(idIntervencion: string, registro: boolean): Observable<ResultadoAccion> {
    const url = `${environment.apiUrlBase}/renunciaintervencion/${idIntervencion}?registro=${registro}`;
    return this.http.post(url, null).pipe(map((r: ResultadoAccion) => r));
  }

  CrearModificacionIntervencion(idIntervencion: string, registro: boolean): Observable<ResultadoAccion> {
    const url = `${environment.apiUrlBase}/modificacionintervencion/${idIntervencion}?registro=${registro}`;
    return this.http.post(url, null).pipe(map((r: ResultadoAccion) => r));
  }

  CrearAnexoIntervencion(idIntervencion: string, registro: boolean): Observable<ResultadoAccion> {
    const url = `${environment.apiUrlBase}/anexointervencion/${idIntervencion}?registro=${registro}`;
    return this.http.post(url, null).pipe(map((r: ResultadoAccion) => r));
  }

  CrearCambioGestionCobro(idIntervencion: string): Observable<ResultadoAccion> {
    const url = `${environment.apiUrlBase}/cambiogestioncobro/${idIntervencion}`;
    return this.http.post(url, null).pipe(map((r: ResultadoAccion) => r));
  }


  get PendientesPago(): Observable<Tramite[]> {
    return this.getTramites(`${environment.apiUrlBase}/tramites/estado/pagopendiente`);
  }
  get EnElaboracion(): Observable<Tramite[]> {
    return this.getTramites(`${environment.apiUrlBase}/tramites/estado/elaboracion`);
  }

  get Finalizados(): Observable<Tramite[]> {
    return this.getTramites(`${environment.apiUrlBase}/tramites/estado/finalizado`);
  }

  get Incidencias(): Observable<Tramite[]> {
    return this.getTramites(`${environment.apiUrlBase}/tramites/estado/incidencia`);
  }

  private getTramites(url: string): Observable<Tramite[]> {
    return this.http.get(url).pipe(
      map((response: any[]) => {
        return response.map(ts => {
          const t: Tramite = {
            Descripcion1: ts.Descripcion1,
            Descripcion2: ts.Descripcion2,
            Descripcion3: ts.Descripcion3,
            IdColegiado: ts.IdColegiado,
            IdSolicitud: ts.IdSolicitud,
            TipoTramite: ts.TipoTramite,
            Estado: ts.Estado,
            FechaUltimoEstado: ts.FechaUltimoEstado,
            Link: this.t2r.GetRoute(ts.TipoTramite, ts.IdSolicitud)
          };
          return t;
        });
      })
    );
  }









}
