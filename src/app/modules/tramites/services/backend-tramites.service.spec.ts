import { TestBed } from '@angular/core/testing';

import { BackendTramitesService } from './backend-tramites.service';

describe('BackendTramitesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BackendTramitesService = TestBed.get(BackendTramitesService);
    expect(service).toBeTruthy();
  });
});
