import { Router } from '@angular/router';
import { GuiUtilsService } from 'src/app/modules/mycommon/services/gui-utils.service';
import { BackendTramitesService } from './../../services/backend-tramites.service';
import { Component, OnInit, ViewChild, EventEmitter, HostListener, ElementRef, Renderer2 } from '@angular/core';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  @ViewChild('marcadorPasosTramite', null) marcadorPasosTramite: ElementRef;

  constructor(
    private backend: BackendTramitesService,
    private guiutils: GuiUtilsService,
    private renderer: Renderer2,
    public router: Router) { }
  filtro: any = null;
  tramites: any[] = [];

  page = 1;
  pageSize = 5;

  ngOnInit() {

    this.filtrar(null);
    this.guiutils.TituloPrincipal = 'Trámites';

  }


  filtrar(filtro) {
    this.page = 1;
    this.filtro = filtro || {};
    this.backend.Buscar(this.filtro).subscribe(
      data => {
        this.tramites = data;
      }
    );
  }

  repaginar(idsolicitudborrada: number) {
    this.tramites = this.tramites.filter(t => t.IdSolicitud !== idsolicitudborrada);
  }

  /* Funcionalidad para hacer sticky el marcador de pasos  */
  


}
