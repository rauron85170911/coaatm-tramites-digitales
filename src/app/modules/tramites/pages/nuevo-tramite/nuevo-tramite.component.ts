import { Tramite2RouteService } from 'src/app/modules/core/services/tramite2-route.service';

import { GuiUtilsService } from './../../../mycommon/services/gui-utils.service';
import { ResultadoAccion } from './../../../core/models/resultadoaccion.model';
import { BackendTramitesService } from './../../services/backend-tramites.service';

import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-nuevo-tramite',
  templateUrl: './nuevo-tramite.component.html',
  styleUrls: ['./nuevo-tramite.component.scss']
})
export class NuevoTramiteComponent implements OnInit {

  constructor(
    private backend: BackendTramitesService,
    private guiutils: GuiUtilsService,
    private tramiteroute: Tramite2RouteService) { }

  ngOnInit() {
  }



  nuevoTramite(intervencion: string, tramite: string) {
    let o: Observable<ResultadoAccion> = null;
    switch (tramite) {
      case 'ME':
        o = this.backend.CrearModificacionIntervencion(intervencion, false);
        break;
      case 'MR':
        o = this.backend.CrearModificacionIntervencion(intervencion, true);
        break;
      case 'RE':
        o = this.backend.CrearRenunciaIntervencion(intervencion, false);
        break;
      case 'RR':
        o = this.backend.CrearRenunciaIntervencion(intervencion, true);
        break;
      case 'AX':
        o = this.backend.CrearAnexoIntervencion(intervencion, false);
        break;
      case 'RX':
        o = this.backend.CrearAnexoIntervencion(intervencion, true);
        break;
      case 'GC':
        o = this.backend.CrearCambioGestionCobro(intervencion);
        break;
    }
    o.subscribe(r => {
      if (r.Ok) {
        this.tramiteroute.Navigate(tramite, r.IdAsociado);
      } else {
        this.guiutils.ShowError(r.Descripcion);
      }
    });


  }

}
