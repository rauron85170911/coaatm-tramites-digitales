import { FormsModule } from '@angular/forms';
import { CoreModule } from './../core/core.module';
import { TramitesRoutingModule } from './tramites-routing';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexComponent } from './pages/index/index.component';
import { MyCommonModule } from '../mycommon/mycommon.module';
import { HomeComponent } from './pages/home/home.component';
import { FiltroTramitesComponent } from './components/filtro-tramites/filtro-tramites.component';
import { NgbModule, NgbDateAdapter, NgbDateNativeAdapter, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { FichaTramiteComponent } from './components/ficha-tramite/ficha-tramite.component';
import { NgbDateESParserFormatter } from '../mycommon/formatters/NgbDateESParserFormatter';
import { PagosComponent } from './components/pagos/pagos.component';
import { NuevoTramiteComponent } from './components/nuevo-tramite/nuevo-tramite.component';
import { GrupoTipoTramitesComponent } from './components/grupo-tipo-tramites/grupo-tipo-tramites.component';
import { NuevoTramiteTipoComponent } from './components/nuevo-tramite-tipo/nuevo-tramite-tipo.component';
import { SeleccionarIntervencionComponent } from './components/seleccionar-intervencion/seleccionar-intervencion.component';
import { IrANuevoTramiteComponent } from './components/ir-anuevo-tramite/ir-anuevo-tramite.component';
import { CuadroMandoTramitesComponent } from './components/cuadromando/cuadro-mando-tramites/cuadro-mando-tramites.component';
import { SeccionCuadroMandoTramitesComponent } from './components/cuadromando/seccion-cuadro-mando-tramites/seccion-cuadro-mando-tramites.component';

@NgModule({
  declarations: [IndexComponent,
    HomeComponent,
    FiltroTramitesComponent,
    FichaTramiteComponent,
    PagosComponent,
    NuevoTramiteComponent,
    GrupoTipoTramitesComponent,
    NuevoTramiteTipoComponent,
    SeleccionarIntervencionComponent,
    IrANuevoTramiteComponent,
    CuadroMandoTramitesComponent, SeccionCuadroMandoTramitesComponent],
  imports: [
    CommonModule,
    MyCommonModule,
    TramitesRoutingModule,
    CoreModule,
    FormsModule,
    NgbModule
  ],
  exports: [
    IndexComponent, HomeComponent, IrANuevoTramiteComponent, NuevoTramiteComponent, CuadroMandoTramitesComponent ],
  providers: []
})
export class TramitesModule { }
