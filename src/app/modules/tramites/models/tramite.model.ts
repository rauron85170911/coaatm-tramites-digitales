export interface Tramite {
    Descripcion1: string;
    Descripcion2: string;
    Descripcion3: string;
    IdColegiado: number;
    IdSolicitud: number;
    TipoTramite: string;
    Estado: string;
    FechaUltimoEstado: Date;
    Link: string[];
}

