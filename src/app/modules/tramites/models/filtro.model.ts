export class Filtro {
    IdSolicitud: number;
    TipoTramite: string;
    GrupoTramites: string;
    EstadoTramite: string;
    FechaDesde: Date;
    FechaHasta: Date;
}
