import { GuiUtilsService } from './../../../mycommon/services/gui-utils.service';
import { AuthenticationService } from './../../../core/services/authentication.service';
import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-grupo-tipo-tramites',
  templateUrl: './grupo-tipo-tramites.component.html',
  styleUrls: ['./grupo-tipo-tramites.component.scss']
})
export class GrupoTipoTramitesComponent implements OnInit, OnChanges {

  @Input() titulo = null;
  @Input() icono = null;
  @Input() tipostramite: string[] = [];
  constructor(private authService: AuthenticationService, private guiutils: GuiUtilsService) { }

  disponibles: boolean[] = [];
  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.tipostramite && changes.tipostramite.currentValue) {
      this.disponibles = [];
      this.authService.TramitesDisponibles.subscribe(disp => {
        this.tipostramite.forEach(tipo => {
          this.disponibles.push(disp.findIndex(t => t.Codigo === tipo) > -1);
        });
      });
    }
  }

  nuevotramite(tipo) {
    this.authService.TramitesDisponibles.subscribe(disponibles => {
      if (disponibles.findIndex(tt => tt.Codigo === tipo) > -1) {
        this.guiutils.ShowSuccess(`Tiene permiso para iniciar un trámite de tipo ${tipo}` );
      } else {
        this.guiutils.ShowError(`No tiene permiso para iniciar un trámite de tipo  ${tipo}`);
      }
    });
  }

}
