import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrupoTipoTramitesComponent } from './grupo-tipo-tramites.component';

describe('GrupoTipoTramitesComponent', () => {
  let component: GrupoTipoTramitesComponent;
  let fixture: ComponentFixture<GrupoTipoTramitesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrupoTipoTramitesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrupoTipoTramitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
