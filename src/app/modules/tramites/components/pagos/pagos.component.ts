import { GuiUtilsService } from './../../../mycommon/services/gui-utils.service';
import { BackendTramitesService } from './../../services/backend-tramites.service';

import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-pagos',
  templateUrl: './pagos.component.html',
  styleUrls: ['./pagos.component.scss']
})
export class PagosComponent implements OnChanges {

  @Input() tramite: any = null;
  constructor(private backend: BackendTramitesService, public guiutils: GuiUtilsService) { }
  pagos: any[] = [];

  ngOnChanges(changes: SimpleChanges): void {

    if (changes.tramite && changes.tramite.isFirstChange) {
    
      this.backend.GetPagos(changes.tramite.currentValue.IdSolicitud).subscribe(pagos => {
        this.pagos = pagos;
      });
    }

  }

}
