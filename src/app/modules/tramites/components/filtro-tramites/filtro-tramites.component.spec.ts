import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltroTramitesComponent } from './filtro-tramites.component';

describe('FiltroTramitesComponent', () => {
  let component: FiltroTramitesComponent;
  let fixture: ComponentFixture<FiltroTramitesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltroTramitesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltroTramitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
