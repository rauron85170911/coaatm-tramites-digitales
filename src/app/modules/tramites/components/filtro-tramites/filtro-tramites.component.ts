import { CodigoDescripcion } from './../../../core/models/codigodescripcion.model';
import { TablasMaestrasCacheService } from 'src/app/modules/core/services/tablas-maestras-cache.service';
import { Filtro } from './../../models/models.index';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-filtro-tramites',
  templateUrl: './filtro-tramites.component.html',
  styleUrls: ['./filtro-tramites.component.scss']
})
export class FiltroTramitesComponent implements OnInit {

  @Output() filtrar: EventEmitter<Filtro> = new EventEmitter<Filtro>();

  constructor(private maestras: TablasMaestrasCacheService) { }
  filtro: Filtro = new Filtro();
  grupos: CodigoDescripcion[] = [];
  tipos: CodigoDescripcion[] = [];
  estados: CodigoDescripcion[] = [];
  ngOnInit() {

    this.maestras.GruposTiposTramite.subscribe(grupos => {
      grupos.map(t => this.grupos.push(t));
    });
    this.maestras.EstadosTramite.subscribe(estados => {
      this.estados = estados;
    });
    this.cargarTipos(null);
  }

  cargarTipos(grupo: string) {
    this.filtro.TipoTramite = null;
    this.tipos = [];
    this.maestras.TiposTramite.subscribe(tipos => {
      tipos.map(t => {
        if (t.CodigoGrupo === grupo || grupo === null) {
          this.tipos.push(t);
        }
      });
    });
  }

  limpiar() {
    this.filtro = new Filtro();
    this.filtrar.emit(this.filtro);
  }

}
