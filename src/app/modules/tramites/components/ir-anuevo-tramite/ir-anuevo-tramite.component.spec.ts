import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IrANuevoTramiteComponent } from './ir-anuevo-tramite.component';

describe('IrANuevoTramiteComponent', () => {
  let component: IrANuevoTramiteComponent;
  let fixture: ComponentFixture<IrANuevoTramiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IrANuevoTramiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IrANuevoTramiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
