import { Router } from '@angular/router';
import { GuiUtilsService } from '../../../mycommon/services/gui-utils.service';
import { AuthenticationService } from '../../../core/services/authentication.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-nuevo-tramite-tipo',
  templateUrl: './nuevo-tramite-tipo.component.html',
  styleUrls: ['./nuevo-tramite-tipo.component.scss']
})
export class NuevoTramiteTipoComponent implements OnInit {

  @Input() tipo: string = null;
  @Input() url: string = null;
  constructor(private authService: AuthenticationService, private guiutils: GuiUtilsService, private route: Router) { }
  abierto = false;
  ngOnInit() {
  }

  abrir() {
    this.authService.TramitesDisponibles.subscribe(disponibles => {
      if (disponibles.findIndex(tt => tt.Codigo === this.tipo) > -1) {
        if (!!this.url) {
          this.route.navigateByUrl(this.url);
        } else {
          this.abierto = true;
        }
      } else {
        this.guiutils.ShowError(`No tiene permiso para iniciar un trámite de tipo  ${this.tipo}`);
      }
    });
  }
}
