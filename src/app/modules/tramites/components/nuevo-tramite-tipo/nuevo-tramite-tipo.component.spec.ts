import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoTramiteTipoComponent } from './nuevo-tramite-tipo.component';

describe('NuevoTramiteComponent', () => {
  let component: NuevoTramiteTipoComponent;
  let fixture: ComponentFixture<NuevoTramiteTipoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoTramiteTipoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoTramiteTipoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
