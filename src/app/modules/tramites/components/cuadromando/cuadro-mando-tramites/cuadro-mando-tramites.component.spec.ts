import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CuadroMandoTramitesComponent } from './cuadro-mando-tramites.component';

describe('CuadroMandoTramitesComponent', () => {
  let component: CuadroMandoTramitesComponent;
  let fixture: ComponentFixture<CuadroMandoTramitesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CuadroMandoTramitesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CuadroMandoTramitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
