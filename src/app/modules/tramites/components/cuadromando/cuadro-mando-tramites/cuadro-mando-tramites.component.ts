import { forkJoin } from 'rxjs';
import { BackendTramitesService } from './../../../services/backend-tramites.service';
import { Component, OnInit } from '@angular/core';
import { Tramite } from '../../../models/models.index';

@Component({
  selector: 'app-cuadro-mando-tramites',
  templateUrl: './cuadro-mando-tramites.component.html',
  styleUrls: ['./cuadro-mando-tramites.component.scss']
})
export class CuadroMandoTramitesComponent implements OnInit {

  constructor(private backend: BackendTramitesService) { }


  secciones: any = {
    elaboracion: {
      visible: true,
      desplegado: false,
      tramites: []
    },
    finalizados: {
      visible: true,
      desplegado: false,
      tramites: []
    },

    incidencias: {
      visible: true,
      desplegado: false,
      tramites: []
    },
    pendientespago: {
      visible: true,
      desplegado: false,
      tramites: []
    },

  };


  // finalizados: Tramite[] = [];
  // elaboracion: Tramite[] = [];
  // incidencias: Tramite[] = [];
  // pendientespago: Tramite[] = [];

  todocargado = false;

  ngOnInit() {

    forkJoin(this.backend.Finalizados,
      this.backend.EnElaboracion,
      this.backend.Incidencias,
      this.backend.PendientesPago).subscribe(resultados => {
        this.secciones.finalizados.tramites = resultados[0];
        this.secciones.elaboracion.tramites = resultados[1];
        this.secciones.incidencias.tramites = resultados[2];
        this.secciones.pendientespago.tramites = resultados[3];
        this.todocargado = true;
      }
      );
    // this.backend.Finalizados.subscribe(tramites => {
    //   this.finalizados = tramites;
    // });

  }


  desplegar($event: string) {
    Object.keys(this.secciones).forEach(key => {
      if (key === $event) {
        this.secciones[key].visible = true;
        this.secciones[key].desplegado = true;
      } else {
        this.secciones[key].visible = false;
        this.secciones[key].desplegado = false;
      }
    });

  }

  plegar() {
    Object.keys(this.secciones).forEach(key => {
      this.secciones[key].visible = true;
      this.secciones[key].desplegado = false;
    });


    //   map(function(personNamedIndex){
    //     let person = persons[personNamedIndex];
    //     // do something with person
    //     return person;
    // });
    //   this.secciones.foreach


  }



}
