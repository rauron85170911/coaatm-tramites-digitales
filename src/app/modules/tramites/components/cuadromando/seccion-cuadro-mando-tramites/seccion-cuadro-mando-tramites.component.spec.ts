import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeccionCuadroMandoTramitesComponent } from './seccion-cuadro-mando-tramites.component';

describe('SeccionCuadroMandoTramitesComponent', () => {
  let component: SeccionCuadroMandoTramitesComponent;
  let fixture: ComponentFixture<SeccionCuadroMandoTramitesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeccionCuadroMandoTramitesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeccionCuadroMandoTramitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
