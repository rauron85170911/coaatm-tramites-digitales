import { GuiUtilsService } from './../../../../mycommon/services/gui-utils.service';
import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { Tramite } from '../../../models/models.index';

@Component({
  selector: 'app-seccion-cuadro-mando-tramites',
  templateUrl: './seccion-cuadro-mando-tramites.component.html',
  styleUrls: ['./seccion-cuadro-mando-tramites.component.scss']
})
export class SeccionCuadroMandoTramitesComponent implements OnChanges {


  @Input() titulo = null;
  @Input() icono = null;
  @Input() id = '';
  @Input() tramites: Tramite[] = [];
  @Input() visible = true;
  @Input() desplegado = false;

  @Output() desplegar: EventEmitter<string> = new EventEmitter<string>();
  @Output() plegar: EventEmitter<null> = new EventEmitter<null>();
  constructor(private guiutils: GuiUtilsService) { }

  documentosvisibles: boolean[] = []; // variable para almacenar si están visibles los documentos
  incidenciasvisibles: boolean[] = [];
  pagosvisibles: boolean[] = [];

  // primero: Tramite = null;
  // restantes: number = 0;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.tramites && changes.tramites.currentValue.length > 0) {
      this.documentosvisibles = this.tramites.map(t => false);
      this.incidenciasvisibles = this.tramites.map(t => false);
      this.pagosvisibles = this.tramites.map(t => false);
      // this.primero = changes.tramites.currentValue[0];
      // this.restantes = changes.tramites.currentValue.length - 1;
    }
  }
  toggle(id: string) {
    if (this.desplegado) {
      this.plegar.emit();
    } else {
      this.desplegar.emit(id);
    }
  }

  desplegardocumentos(i: number) {
    this.documentosvisibles[i] = true;
    this.guiutils.ShowInfo('Aquí se desplegarian los documentos del trámite');
  }
  desplegarpagos(i: number) {
    this.pagosvisibles[i] = true;
    this.guiutils.ShowInfo('Aquí se desplegarian los pagos del trámite');
  }
  desplegarincidencias(i: number) {
    this.incidenciasvisibles[i] = true;
    this.guiutils.ShowInfo('Aquí se desplegarian las incidencias del trámite');
  }
  eliminar(idsolicitud: number) {

  }

}
