import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeleccionarIntervencionComponent } from './seleccionar-intervencion.component';

describe('SeleccionarIntervencionComponent', () => {
  let component: SeleccionarIntervencionComponent;
  let fixture: ComponentFixture<SeleccionarIntervencionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeleccionarIntervencionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeleccionarIntervencionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
