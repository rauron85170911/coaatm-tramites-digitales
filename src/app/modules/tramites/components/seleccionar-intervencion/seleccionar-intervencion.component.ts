import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-seleccionar-intervencion',
  templateUrl: './seleccionar-intervencion.component.html',
  styleUrls: ['./seleccionar-intervencion.component.scss']
})
export class SeleccionarIntervencionComponent implements OnInit {

  @Input() textoconfirmacion = '¿Seguro?';
  @Input() textoaccion = 'Operar';
  @Output() ejecutar: EventEmitter<string> = new EventEmitter<string>();
  constructor() { }
  intervencion: string = null;
  ngOnInit() {
  }
  confirmar() {
    this.ejecutar.emit(this.intervencion);
  }

}
