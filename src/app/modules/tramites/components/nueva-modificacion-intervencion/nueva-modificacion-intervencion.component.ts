import { Component, OnInit, Input } from '@angular/core';
import { BackendTramitesService } from '../../services/backend-tramites.service';
import { GuiUtilsService } from '../../../mycommon/services/gui-utils.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nueva-modificacion-intervencion',
  templateUrl: './nueva-modificacion-intervencion.component.html',
  styleUrls: ['./nueva-modificacion-intervencion.component.scss']
})
export class NuevaModificacionIntervencionComponent implements OnInit {

  @Input() registro = false;
  constructor(private backend: BackendTramitesService, private guiutils: GuiUtilsService, private route: Router) { }
  intervencion: string = null;
  ngOnInit() {
  }
  modificar() {
    this.backend.CrearModificacionIntervencion(this.intervencion, this.registro).subscribe((resultado: any) => {
      if (!resultado.Ok) {
        this.guiutils.ShowError(resultado.Descripcion);
      } else {
        // se ha creado bien la solicitud de renuncia y redirigimos a la página correspondiente para su modificación
        if (this.registro) {
          this.route.navigate(['registros', 'modificacion', resultado.IdAsociado]);
        } else {
          this.route.navigate(['visados', 'modificacion', resultado.IdAsociado]);
        }
      }
    });
  }

}
