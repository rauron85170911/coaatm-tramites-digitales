import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevaModificacionIntervencionComponent } from './nueva-modificacion-intervencion.component';

describe('NuevaModificacionIntervencionComponent', () => {
  let component: NuevaModificacionIntervencionComponent;
  let fixture: ComponentFixture<NuevaModificacionIntervencionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevaModificacionIntervencionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevaModificacionIntervencionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
