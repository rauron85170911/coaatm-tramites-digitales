import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevaRenunciaIntervencionComponent } from './nueva-renuncia-intervencion.component';

describe('NuevaRenunciaIntervencionComponent', () => {
  let component: NuevaRenunciaIntervencionComponent;
  let fixture: ComponentFixture<NuevaRenunciaIntervencionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevaRenunciaIntervencionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevaRenunciaIntervencionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
