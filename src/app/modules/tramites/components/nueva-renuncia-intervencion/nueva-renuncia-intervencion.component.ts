import { Router } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { GuiUtilsService } from './../../../mycommon/services/gui-utils.service';
import { BackendTramitesService } from '../../services/backend-tramites.service';

@Component({
  selector: 'app-nueva-renuncia-intervencion',
  templateUrl: './nueva-renuncia-intervencion.component.html',
  styleUrls: ['./nueva-renuncia-intervencion.component.scss']
})
export class NuevaRenunciaIntervencionComponent implements OnInit {
  @Input() registro = false;
  constructor(private backend: BackendTramitesService, private guiutils: GuiUtilsService, private route: Router) { }
  intervencion: string = null;
  ngOnInit() {
  }
  renunciar() {
    this.backend.CrearRenunciaIntervencion(this.intervencion, this.registro).subscribe((resultado: any) => {
      if (!resultado.Ok) {
        this.guiutils.ShowError(resultado.Descripcion);
      } else {
        // se ha creado bien la solicitud de renuncia y redirigimos a la página correspondiente para su modificación
        if (this.registro) {
          this.route.navigate(['registros', 'renuncia', resultado.IdAsociado]);
        } else {
          this.route.navigate(['visados', 'renuncia', resultado.IdAsociado]);
        }
      }
    });
  }
}
