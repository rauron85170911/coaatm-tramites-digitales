import { Tramite2RouteService } from './../../../core/services/tramite2-route.service';
import { GuiUtilsService } from './../../../mycommon/services/gui-utils.service';
import { BackendTramitesService } from './../../services/backend-tramites.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-ficha-tramite',
  templateUrl: './ficha-tramite.component.html',
  styleUrls: ['./ficha-tramite.component.scss']
})
export class FichaTramiteComponent implements OnInit {
  @Input() tramite: any;
  @Output() borrado: EventEmitter<number> = new EventEmitter<number>();
  @Output() enviado: EventEmitter<number> = new EventEmitter<number>();
  constructor(
    private backend: BackendTramitesService,
    private guiutils: GuiUtilsService,
    private a: Tramite2RouteService
  ) { }
  mostrardocumentacion = false;
  mostrarpagos = false;
  ngOnInit() {
    // this.cargar();
  }



  /**
   * Devuelve los parámetros para enlazar el trámite con su página de detalle,
   * Se irá completando según vayamos implementando trámites
   */
  get link() {
    return this.a.GetRoute(this.tramite.TipoTramite, this.tramite.IdSolicitud);
    const fragmento1: any = {
      AR: '/registros',
      AE: '/visados',
      RR: '/registros',
      RE: '/visados',
      MR: '/registros',
      ME: '/visados'
    };

    const fragmento2: any = {
      AR: 'altaintervencion',
      AE: 'altaintervencion',
      RR: 'renuncia',
      RE: 'renuncia',
      MR: 'modificacionintervencion',
      ME: 'modificacionintervencion'
    };

    if (fragmento1[this.tramite.TipoTramite] && fragmento2[this.tramite.TipoTramite]) {
      return [fragmento1[this.tramite.TipoTramite], fragmento2[this.tramite.TipoTramite], this.tramite.IdSolicitud];
    } else {
      return null;
    }



  }


  cargar() {
    this.backend.GetDetalle(this.tramite.IdSolicitud).subscribe(t => {
      this.tramite.Descripcion1 = t.Descripcion1;
      this.tramite.Descripcion2 = t.Descripcion2;
      this.tramite.Descripcion3 = t.Descripcion3;

    });
  }

  borrar() {
    this.backend.BorrarTramite(this.tramite.IdSolicitud).subscribe(resul => {
      if (resul.Ok) {
        this.guiutils.ShowSuccess('Se ha borrado el trámite');
        this.borrado.emit(this.tramite.IdSolicitud);
      } else {
        this.guiutils.ShowError(resul.Descripcion);
      }
    });
  }

  enviar() {
    this.backend.EnviarTramite(this.tramite.IdSolicitud).subscribe(resul => {
      if (resul.Ok) {
        this.tramite.Estado = 'EN';
        this.guiutils.ShowSuccess('Se ha enviado correctamente el trámite');
      } else {
        this.guiutils.ShowError(resul.Descripcion);
      }
    });
  }




}
