import { MyCommonModule } from './../mycommon/mycommon.module';
import { CoreModule } from './../core/core.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FileSaverModule} from 'ngx-filesaver';
import { IndexComponent } from './pages/index/index.component';
import { CertificadosRoutingModule } from './certificados-routing.module';
import { EstadoCertificadoPipe } from './pipes/estado-certificado.pipe';
import { ListadoCertificadosComponent } from './components/listado-certificados/listado-certificados.component';
import { CertificadosSRCComponent } from './components/certificados-src/certificados-src.component';
import { CertificadosOtrosComponent } from './components/certificado-otros/certificados-otros.component';


@NgModule({
  declarations: [IndexComponent, CertificadosOtrosComponent, EstadoCertificadoPipe, ListadoCertificadosComponent, CertificadosSRCComponent],
  imports: [
    CommonModule,
    CoreModule,
    CertificadosRoutingModule,
    NgbModule,
    FileSaverModule,
    MyCommonModule
  ]
})
export class CertificadosModule { }
