import { BackendService } from './../../services/backend.service';
import { GuiUtilsService } from './../../../mycommon/services/gui-utils.service';
import { Component, OnInit } from '@angular/core';
import { Certificado, TipoCertificado, InfoColegiado, SolicitudCertificado } from '../../models/models.index';
import { FileSaverService } from 'ngx-filesaver';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  constructor(private utils: GuiUtilsService, private certificadosService: BackendService, private fileSaverService: FileSaverService) {
    const CC: TipoCertificado = {
      Codigo: 'CC',
      Titulo: 'Certificado de Colegiación',
      Descripcion: `Documento emitido por el departamento
        SAC del Colegio, certificado que, a la fecha, es Colegiado de esta Corporación.`,
      Certificados: null,
      Estado: 'X',
      Ultimo: null
    };

    const SR: TipoCertificado = {
      Codigo: 'SR',
      Titulo: 'Certificado de SRC',
      Descripcion: `Documento emitido por el departamento SAC del Colegio,
       certificando que, a fecha de emisión del mismo, tiene un seguro de Responsabilidad Civil`,
      Certificados: null,
      Estado: 'X',
      Ultimo: null
    };

    const EJ: TipoCertificado = {
      Codigo: 'EJ',
      Titulo: `Certificado de Ejerciente`,
      Descripcion: `Documento emitido por el departamento SAC del Colegio, certificando que a la fecha, ejerce`,
      Certificados: null,
      Estado: 'X',
      Ultimo: null
    };

    const CS: TipoCertificado = {
      Codigo: 'CS',
      Titulo: 'Certificado de Colegiación de Sociedad',
      Descripcion: `Documento emitido por el departamento SAC del Colegio, certificando que es una Sociedad colegiada`,
      Certificados: null,
      Estado: 'X',
      Ultimo: null
    };

    this.tipos.CC = CC;
    this.tipos.SR = SR;
    this.tipos.EJ = EJ;
    this.tipos.CS = CS;

  }

  tipos: any = {
    CC: null,
    SR: null,
    EJ: null,
    CS: null
  };

  infocolegiado: InfoColegiado = null;

  // certificados: Certificado[] = [];
  ngOnInit() {

    this.LoadCertificados();

    this.certificadosService.GetInfoColegiado().subscribe(info => {
      this.infocolegiado = info;
    });
  }

  Solicitar($event: SolicitudCertificado) {
    const tipo: TipoCertificado = this.tipos[$event.Tipo];
    this.certificadosService.SolicitarCertificado($event.Tipo).subscribe(
      response => {
        if (response && response.Ok) {
          // cargo los certificados y muestro un mensaje
          this.LoadCertificados();
          this.utils.ShowInfo(`Se ha solicitado un ${tipo.Titulo} correctamente`);
        } else {
          this.utils.ShowError(response.Descripcion);
          // de momento no hago nada, el mensaje con el posible error lo ha mostrado el servicio
        }
      }
    );
  }

  LanzarDescarga(idSolicitud: number) {
    // this.certificadosService.DescargarCertificado(url).subscribe(
      this.certificadosService.DescargarCertificadoPorIdSolicitud(idSolicitud).subscribe(
      blob => {
        if (blob) {
          const nombre = 'Certificado.pdf';
          this.fileSaverService.save(blob, nombre);
          this.utils.ShowInfo(`Se ha descargado el certificado correctamente`);
        }
      }
    );
  }
  private LoadCertificados() {
    this.certificadosService.GetCertificadosUsuario().subscribe(data => {
      Array.from(['CC', 'SR', 'EJ', 'CS']).forEach(t => {
        this.tipos[t].Certificados = data.filter(c => c.Tipo === t);

        if (this.tipos[t].Certificados.length > 0) {
          this.tipos[t].Ultimo = this.tipos[t].Certificados[0];
          this.tipos[t].Estado = this.tipos[t].Ultimo.Estado;
        } else {
          this.tipos[t].Estado = 'X'; // no tiene ningún certificado de ese tipo
        }
      });
    });
  }

}
