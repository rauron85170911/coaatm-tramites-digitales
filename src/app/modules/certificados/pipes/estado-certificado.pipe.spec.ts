import { EstadoCertificadoPipe } from './estado-certificado.pipe';

describe('EstadoCertificadoPipe', () => {
  it('create an instance', () => {
    const pipe = new EstadoCertificadoPipe();
    expect(pipe).toBeTruthy();
  });


  it('Traduce bien los estados', () => {
    const estados: any[] = [
      { Id: 'V', Desc: 'Vigente' },
      { Id: 'C', Desc: 'Caducado' },
      { Id: 'P', Desc: 'Pte. Emisión' }
    ];

    const pipe = new EstadoCertificadoPipe();
    expect(pipe).toBeTruthy();
    estados.forEach(estado => {
      const lit = pipe.transform(estado.Id);
      expect(lit).toBe(estado.Desc);
    });


  });

});
