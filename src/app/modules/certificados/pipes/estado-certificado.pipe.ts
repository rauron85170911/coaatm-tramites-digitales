import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'estadoCertificado'
})
export class EstadoCertificadoPipe implements PipeTransform {

  transform(value: any, args?: any): any {

    const estados: any[] = [
      {Id: 'V', Desc: 'Vigente'},
      {Id: 'C', Desc: 'Caducado'},
      {Id: 'P', Desc: 'Pte. Emisión'}
    ];
    const estado = estados.filter(e => e.Id === value)[0];
    if (!!estado) {
      return estado.Desc;
    } else {
      return 'Sin estado';
    }    
  }

}
