import { Certificado } from '../models/models.index';


export const INFOCOLEGIADOFAKEBACKEND: any = {
    Colegiado: {
        Id: null,
        NumeroColegiado: '9098',
        Nombre: 'Sonia-Pre NUÑO RODRIGUEZ',
        FechaAltaColegiacion: '1995-08-01T00:00:00',
        Situacion: 'Ejerciente',
        Sociedad: false
    },
    SeguroResponsabilidadCivil: {
        FechaAlta: '2017-01-01T00:00:00',
        FechaBaja: '0001-01-01T00:00:00',
        Compania: 'CATALANA OCCIDENTE (STA S.R.C.)',
        NombreProducto: 'SRC',
        Cobertura: '2500000,00'
    }
};

export const CERTIFICADOSFAKEBACKEND: any[] = [
    {
        Tipo: 'CC',
        FechaSolicitud: '2017-03-30T12:20:00',
        FechaEmision: '2017-03-30T12:19:48.99',
        FechaVigencia: '2017-04-29T12:19:48.99',
        Estado: 'P',
        IdSolicitudAsociada: 201709485,
        DocumentoAsociado: false
    },
    {
        Tipo: 'EJ',
        FechaSolicitud: '2017-03-30T12:22:00',
        FechaEmision: '2017-03-30T12:22:13.857',
        FechaVigencia: '2017-04-29T12:22:13.857',
        Estado: 'P',
        IdSolicitudAsociada: 201709490,
        DocumentoAsociado: false
    },
    {
        Tipo: 'EJ',
        FechaSolicitud: '2019-09-30T11:34:00',
        FechaEmision: '2019-09-30T11:33:44.89',
        FechaVigencia: '2019-10-30T11:33:44.89',
        Estado: 'P',
        IdSolicitudAsociada: 201709644,
        DocumentoAsociado: false
    },
    {
        Tipo: 'CC',
        FechaSolicitud: '2019-10-04T11:54:00',
        FechaEmision: '2019-10-04T11:54:00.067',
        FechaVigencia: '2019-11-03T11:54:00.067',
        Estado: 'P',
        IdSolicitudAsociada: 201709648,
        DocumentoAsociado: false
    },
    {
        Tipo: 'CC',
        FechaSolicitud: '2019-10-04T12:35:00',
        FechaEmision: '2019-10-04T12:35:09.437',
        FechaVigencia: '2019-11-03T12:35:09.437',
        Estado: 'P',
        IdSolicitudAsociada: 201709651,
        DocumentoAsociado: false
    },
    {
        Tipo: 'CC',
        FechaSolicitud: '2019-10-04T13:36:00',
        FechaEmision: '2019-10-04T13:36:01.35',
        FechaVigencia: '2019-11-03T13:36:01.35',
        Estado: 'P',
        IdSolicitudAsociada: 201709652,
        DocumentoAsociado: false
    },
    {
        Tipo: 'SR',
        FechaSolicitud: '2019-10-07T12:30:00',
        FechaEmision: '2019-10-07T12:29:52.677',
        FechaVigencia: '2019-11-06T12:29:52.677',
        Estado: 'P',
        IdSolicitudAsociada: 201709653,
        DocumentoAsociado: false
    },
    {
        Tipo: 'CC',
        FechaSolicitud: '2019-10-07T13:12:00',
        FechaEmision: '2019-10-07T13:11:35.587',
        FechaVigencia: '2019-11-06T13:11:35.587',
        Estado: 'P',
        IdSolicitudAsociada: 201709654,
        DocumentoAsociado: false
    },
    {
        Tipo: 'CC',
        FechaSolicitud: null,
        FechaEmision: '2013-02-28T11:52:01.06',
        FechaVigencia: '2013-03-30T11:52:01.06',
        Estado: 'C',
        IdSolicitudAsociada: 201304973,
        DocumentoAsociado: true
    }
];


export const CERTIFICADOSCCFAKE: Certificado[] = [
    {
        Tipo: 'CC',
        FechaSolicitud: null,
        FechaEmision: new Date('2013-02-28T11:52:01.06'),
        FechaVigencia: new Date('2013-03-30T11:52:01.06'),
        Estado: 'C',
        IdSolicitudAsociada: 201304973,
        DocumentoAsociado: true,
        Url: ''
    },
    {
        Tipo: 'CC',
        FechaSolicitud: new Date('2017-03-30T12:20:00'),
        FechaEmision: new Date('2017-03-30T12:19:48.99'),
        FechaVigencia: new Date('2017-04-29T12:19:48.99'),
        Estado: 'P',
        IdSolicitudAsociada: 201709485,
        DocumentoAsociado: false,
        Url: ''
    },
    {
        Tipo: 'CC',
        FechaSolicitud: new Date('2019-10-04T11:54:00'),
        FechaEmision: new Date('2019-10-04T11:54:00.067'),
        FechaVigencia: new Date('2019-11-03T11:54:00.067'),
        Estado: 'P',
        IdSolicitudAsociada: 201709648,
        DocumentoAsociado: false,
        Url: ''
    },
    {
        Tipo: 'CC',
        FechaSolicitud: new Date('2019-10-04T12:35:00'),
        FechaEmision: new Date('2019-10-04T12:35:09.437'),
        FechaVigencia: new Date('2019-11-03T12:35:09.437'),
        Estado: 'P',
        IdSolicitudAsociada: 201709651,
        DocumentoAsociado: false,
        Url: ''
    },
    {
        Tipo: 'CC',
        FechaSolicitud: new Date('2019-10-04T13:36:00'),
        FechaEmision: new Date('2019-10-04T13:36:01.35'),
        FechaVigencia: new Date('2019-11-03T13:36:01.35'),
        Estado: 'P',
        IdSolicitudAsociada: 201709652,
        DocumentoAsociado: false,
        Url: ''
    },
    {
        Tipo: 'CC',
        FechaSolicitud: new Date('2019-10-07T13:12:00'),
        FechaEmision: new Date('2019-10-07T13:11:35.587'),
        FechaVigencia: new Date('2019-11-06T13:11:35.587'),
        Estado: 'P',
        IdSolicitudAsociada: 201709654,
        DocumentoAsociado: false,
        Url: ''
    }
];

export const CERTIFICADOSSRFAKE = [,
    {
        Tipo: 'SR',
        FechaSolicitud: new Date('2019-10-07T12:30:00'),
        FechaEmision: new Date('2019-10-07T12:29:52.677'),
        FechaVigencia: new Date('2019-11-06T12:29:52.677'),
        Estado: 'P',
        IdSolicitudAsociada: 201709653,
        DocumentoAsociado: false,
        Url: ''
    }];
export const CERTIFICADOSEJFAKE = [
    {
        Tipo: 'EJ',
        FechaSolicitud: new Date('2017-03-30T12:22:00'),
        FechaEmision: new Date('2017-03-30T12:22:13.857'),
        FechaVigencia: new Date('2017-04-29T12:22:13.857'),
        Estado: 'P',
        IdSolicitudAsociada: 201709490,
        DocumentoAsociado: false,
        Url: ''
    },
    {
        Tipo: 'EJ',
        FechaSolicitud: new Date('2019-09-30T11:34:00'),
        FechaEmision: new Date('2019-09-30T11:33:44.89'),
        FechaVigencia: new Date('2019-10-30T11:33:44.89'),
        Estado: 'P',
        IdSolicitudAsociada: 201709644,
        DocumentoAsociado: false,
        Url: ''
    }];


