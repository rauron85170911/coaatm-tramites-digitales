import { HttpClientModule } from '@angular/common/http';
import { TestBed, inject, async, getTestBed } from '@angular/core/testing';
import { BackendService } from './backend.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { GuiUtilsService } from '../../mycommon/services/gui-utils.service';
import { Router } from '@angular/router';
import { MyCommonModule } from '../../mycommon/mycommon.module';
import { Certificado } from '../models/models.index';
import { CERTIFICADOSFAKEBACKEND, INFOCOLEGIADOFAKEBACKEND } from './certificados.fake.spec';
describe('BackendService', () => {

  let injector: TestBed;
  let httpMock: HttpTestingController;
  let servicio: BackendService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [BackendService]
    });

    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
    servicio = injector.get(BackendService);


  });
  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    servicio = TestBed.get(BackendService);
    expect(servicio).toBeTruthy();
  });

  it('Recupera certificados', async(() => {
    // service = TestBed.get(BackendService);
    expect(servicio).toBeTruthy();
    servicio.GetCertificadosUsuario().subscribe(
      (response) => {
        expect(response).not.toBeNull('La lista de certificados recuperada es nula');
        // comprobamos que todos los elementos de la lista tienen tipo
        response.forEach(c => {
          expect(['CC', 'EJ', 'SR'].indexOf(c.Tipo)).toBeGreaterThan(-1, `El tipo ${c.Tipo} no está permitido`);
        });
        expect(response.length).toBeGreaterThan(8);
      },
      (error) => fail(error)
    );

    const req = httpMock.expectOne(`${servicio.API_URL}/certificados`);
    expect(req.request.method).toBe('GET');
    req.flush(CERTIFICADOSFAKEBACKEND);

  }));

  it('Recupera información del colegiado', async(() => {
    expect(servicio).toBeTruthy();
    servicio.GetInfoColegiado().subscribe(
      (response) => {
        
        expect(response.SeguroResponsabilidadCivil).toBeTruthy();
      }
    );

    const req = httpMock.expectOne(`${servicio.API_URL}/certificados/colegiado`);
    expect(req.request.method).toBe('GET');
    req.flush(INFOCOLEGIADOFAKEBACKEND);

  }));


});
