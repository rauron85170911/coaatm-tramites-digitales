import { ResultadoAccion } from './../../core/models/resultadoaccion.model';
import { ErrorsService } from './../../mycommon/services/errors.service';
import { GuiUtilsService } from './../../mycommon/services/gui-utils.service';
import { Certificado, InfoColegiado } from '../models/models.index';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  readonly API_URL = environment.apiUrlBase; //'https://api.github.com';

  constructor(private http: HttpClient, private errorService: ErrorsService) { }



  /**
   * Hace una llamada al backend para recuperar todos los certificados solicitados y/o emitidos para el usuario
   *
   */
  public GetCertificadosUsuario(): Observable<Certificado[]> {
    const url = `${environment.apiUrlBase}/certificados`;

    return this.http.get(url).pipe(
      map((data: Array<any>) => {
        const response: Certificado[] = [];
        data.forEach(item => {
          const certificado: Certificado = {
            IdSolicitudAsociada: item.IdSolicitudAsociada,
            Tipo: item.Tipo,
            FechaEmision: item.FechaEmision,
            FechaSolicitud: item.FechaSolicitud,
            FechaVigencia: item.FechaVigencia,
            Estado: item.Estado,
            DocumentoAsociado: item.DocumentoAsociado,
            Url: item.DocumentoAsociado ? `/certificados/download/${item.IdSolicitudAsociada}` : null
          };
          response.push(certificado);
          response.sort((a, b) => {
            return ((new Date(b.FechaSolicitud)).getTime() - (new Date(a.FechaSolicitud)).getTime());
          });
        });
        return response;
      }),
      catchError((error) => {
        this.errorService.RegistrarErrorHttp(error);
        return of(null);
      })
    );
  }


  public GetInfoColegiado(): Observable<InfoColegiado> {
    const url = `${environment.apiUrlBase}/certificados/colegiado`;
    return this.http.get(url).pipe(
      map((data: any) => {
        const info: InfoColegiado = {
          Colegiado: {
            Nombre: data.Colegiado.Nombre,
            NumeroColegiado: data.Colegiado.NumeroColegiado,
            FechaAlta: data.Colegiado.FechaAltaColegiacion,
            FechaEjerciente: data.Colegiado.FechaEjerciente,
            Situacion: data.Colegiado.Situacion,
            Sociedad: data.Colegiado.Sociedad
          },
          SeguroResponsabilidadCivil: data.SeguroResponsabilidadCivil ? {
            Cobertura: data.SeguroResponsabilidadCivil.Cobertura,
            Compania: data.SeguroResponsabilidadCivil.Compania,
            FechaAlta: data.SeguroResponsabilidadCivil.FechaAlta,
            FechaBaja: data.SeguroResponsabilidadCivil.FechaAlta,
            NombreProducto: data.SeguroResponsabilidadCivil.NombreProducto
          } : null
        };
        return info;
      }),
      catchError((error) => {
        this.errorService.RegistrarErrorHttp(error);
        return of(null);
      }));
  }

  public SolicitarCertificado(tipo: string): Observable<ResultadoAccion> {
    const url = `${environment.apiUrlBase}/certificados/solicitar/${tipo}`;
    return this.http.post(url, null).pipe(
      map((response: ResultadoAccion) => response),
      catchError((error) => {
        this.errorService.RegistrarErrorHttp(error);
        const r = new ResultadoAccion();
        r.Ok = false;
        r.Descripcion = 'Error http';
        return of(r);
      })
    );
  }

  public DescargarCertificadoPorIdSolicitud(idSolicitud: number): Observable<Blob> {
    const urlCompleta = `${environment.apiUrlBase}/certificados/download/${idSolicitud}`;
    return this.http.get<Blob>(urlCompleta, { responseType: 'blob' as 'json' }).pipe(
      catchError(error => {
        this.errorService.RegistrarErrorHttp(error);
        return of(null);
      })
    );
  }
  public DescargarCertificado(url: string): Observable<Blob> {
    const urlCompleta = `${environment.apiUrlBase}/${url}`;
    return this.http.get<Blob>(urlCompleta, { responseType: 'blob' as 'json' }).pipe(
      catchError(error => {
        this.errorService.RegistrarErrorHttp(error);
        return of(null);
      })
    );
    // }));
  }

  private handleError(error) {
    return of(null);
  }
}
