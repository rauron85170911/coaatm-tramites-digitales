
export interface Seguro {
    Compania: string;
    FechaAlta: Date;
    FechaBaja: Date;
    Cobertura: string;
    NombreProducto: string;
}
