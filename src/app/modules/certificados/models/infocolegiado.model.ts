import { Colegiado } from './colegiado.model';
import { Seguro } from './seguro.model';

export interface InfoColegiado {
    Colegiado: Colegiado;
    SeguroResponsabilidadCivil: Seguro;
}
