import { Certificado } from './certificado.model';

export interface TipoCertificado {
    Codigo: string;
    Titulo: string;
    Descripcion: string;
    Certificados: any[];
    Ultimo: Certificado;
    Estado: string;
}
