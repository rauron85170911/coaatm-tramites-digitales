export interface Certificado {
    IdSolicitudAsociada: number;
    Tipo: string;
    FechaSolicitud: Date;
    FechaEmision: Date;
    FechaVigencia: Date;
    Estado: string;
    DocumentoAsociado: boolean;
    Url: string;
}
