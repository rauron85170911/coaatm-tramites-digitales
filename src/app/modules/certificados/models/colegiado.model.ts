export interface Colegiado {
    FechaAlta: Date;
    FechaEjerciente: Date;
    Situacion: string;
    NumeroColegiado: string;
    Nombre: string;
    Sociedad: boolean;
}
