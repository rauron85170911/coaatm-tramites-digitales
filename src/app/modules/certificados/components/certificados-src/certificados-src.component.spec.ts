import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertificadosSRCComponent } from './certificados-src.component';
import { RouterTestingModule } from '@angular/router/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('CertificadosSRCComponent', () => {
  let component: CertificadosSRCComponent;
  let fixture: ComponentFixture<CertificadosSRCComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [ CertificadosSRCComponent ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificadosSRCComponent);
    component = fixture.componentInstance;
    //fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  


});
