import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { TipoCertificado, InfoColegiado, SolicitudCertificado } from '../../models/models.index';
import { GuiUtilsService } from 'src/app/modules/mycommon/services/gui-utils.service';
import { trigger, transition, style, animate } from '@angular/animations';

@Component({
  selector: 'app-certificados-src',
  templateUrl: './certificados-src.component.html',
  styleUrls: ['./certificados-src.component.scss'],
  animations: [
    trigger(
      'inOutAnimation',
      [
        transition(
          ':enter',
          [
            style({opacity: 0, height: '0px', 'padding-top': '0px', 'padding-bottom': '0px', overflow: 'hidden' }),
            animate('0.5s ease-out',
                    style({ opacity: 1, height: '*', 'padding-top': '*', 'padding-bottom': '*' }))
          ]
        ),
        transition(
          ':leave',
          [
            style({ opacity: 1, overflow: 'hidden', 'padding-top': '*', 'padding-bottom': '*', height: '*' }),
            animate('0.5s ease-out',
              style({opacity: 0, 'padding-top': '0px', 'padding-bottom': '0px', height: '0px', 'margin-top': 0, 'margin-bottom': 0}))
          ]
        )
      ]
    )
  ]
})
export class CertificadosSRCComponent implements OnInit {

  @Input() info: TipoCertificado = null;
  @Input() infocolegiado: InfoColegiado = null;
  @Output() solicitar: EventEmitter<SolicitudCertificado> = new EventEmitter<SolicitudCertificado>();

  solicitandoNuevo = false;
  constructor(private utils: GuiUtilsService) { }

  ngOnInit() {
  }

  nuevo() {
    this.solicitandoNuevo = true;
  }
  confirmarSolicitud() {
    this.solicitar.emit({
      Tipo: this.info.Codigo
    });
    this.solicitandoNuevo = false;
  }
  cancelar() {
    this.solicitandoNuevo = false;
    this.utils.ShowInfo('No se ha solicitado el certificado. Acción cancelada por el usuario');
  }
}
