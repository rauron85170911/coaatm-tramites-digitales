import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListadoCertificadosComponent } from './listado-certificados.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { EstadoCertificadoPipe } from '../../pipes/estado-certificado.pipe';
import { CERTIFICADOSCCFAKE } from '../../services/certificados.fake.spec';

describe('ListadoCertificadosComponent', () => {
  let component: ListadoCertificadosComponent;
  let fixture: ComponentFixture<ListadoCertificadosComponent>;
  let element: HTMLElement;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListadoCertificadosComponent, EstadoCertificadoPipe ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadoCertificadosComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Renderiza bien la tabla de certificados', () => {
    component.certificados = CERTIFICADOSCCFAKE;
    fixture.detectChanges();
    const tablebody = element.querySelector('table>tbody');
    expect(tablebody).toBeTruthy();
    expect(tablebody.childElementCount).toBe(6);
  });

});
