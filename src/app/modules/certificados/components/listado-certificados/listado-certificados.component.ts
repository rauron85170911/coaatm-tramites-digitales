import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Certificado } from '../../models/models.index';

@Component({
  selector: 'app-listado-certificados',
  templateUrl: './listado-certificados.component.html',
  styleUrls: ['./listado-certificados.component.scss']
})
export class ListadoCertificadosComponent implements OnInit {

  @Input() certificados: Certificado[];
  @Output() descargar: EventEmitter<string> = new EventEmitter<string>();
  collapsed = true;
  constructor() {}

  ngOnInit() {
  }

  // De momento lo descargamos desde aquí, ya veremos si 
  // lo que hacemos en un futuro es lanzar un evento para que la página lo descargue
  lanzardescarga(url: string) {
    this.descargar.emit(url);
  }

}
