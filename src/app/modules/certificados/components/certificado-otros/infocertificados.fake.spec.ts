import { TipoCertificado } from './../../models/tipocertificado.model';
import { CERTIFICADOSCCFAKE } from '../../services/certificados.fake.spec';
export const datoscertificadosMock: TipoCertificado = {
    Codigo: 'CC',
    Titulo: 'Certificados de Colegiación',
    Descripcion: '',
    Ultimo: null,
    Estado: 'P',
    Certificados: CERTIFICADOSCCFAKE
};
