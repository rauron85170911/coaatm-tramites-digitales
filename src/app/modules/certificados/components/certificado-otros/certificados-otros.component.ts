import { AyudaService } from './../../../mycommon/services/ayuda.service';
import { GuiUtilsService } from '../../../mycommon/services/gui-utils.service';

import { Component, OnInit, Input, Output, EventEmitter, TemplateRef } from '@angular/core';
import { Certificado, TipoCertificado, InfoColegiado, SolicitudCertificado } from '../../models/models.index';
import { trigger, transition, style, animate } from '@angular/animations';



@Component({
  selector: 'app-certificados-otros',
  templateUrl: './certificados-otros.component.html',
  styleUrls: ['./certificados-otros.component.scss'],
  animations: [
    trigger(
      'inOutAnimation',
      [
        transition(
          ':enter',
          [
            style({ opacity: 0, height: '0px', 'padding-top': '0px', 'padding-bottom': '0px', overflow: 'hidden' }),
            animate('0.5s ease-out',
              style({ opacity: 1, height: '*', 'padding-top': '*', 'padding-bottom': '*' }))
          ]
        ),
        transition(
          ':leave',
          [
            style({ opacity: 1, overflow: 'hidden', 'padding-top': '*', 'padding-bottom': '*', height: '*' }),
            animate('0.5s ease-out',
              style({ opacity: 0, 'padding-top': '0px', 'padding-bottom': '0px', height: '0px', 'margin-top': 0, 'margin-bottom': 0 }))
          ]
        )
      ]
    )
  ]
})
export class CertificadosOtrosComponent implements OnInit {

  @Input() info: TipoCertificado = null;
  @Input() infocolegiado: InfoColegiado = null;
  @Output() solicitar: EventEmitter<SolicitudCertificado> = new EventEmitter<SolicitudCertificado>();
  @Output() descargar: EventEmitter<number> = new EventEmitter<number>();
  public solicitandoNuevo = false;
  public collapsed = true; // indica si la tabla con la vista de los certificados está abierta o cerrada
  // @Input() certificados: Certificado[] = [];
  constructor(private utils: GuiUtilsService, public ayudaService: AyudaService) { }
  mensajeconfirmacion = {
    CC: `Si confirma la petición, se procederá a emitir un certificado de Colegiación actualizado a día de hoy que estará
    próximamente disponible para su descarga en el repositorio de Solicitudes.`,
    EJ: `Si confirma la petición, una vez examinada por el COAATM y comprobada la veracidad del hecho a certificar, se
    procederá a emitir el certificado depositándolo en el repositorio de Solicitudes para su descarga.`,
    SR: `Si confirma la petición, una vez examinada por el COAATM y comprobada la veracidad del hecho a certificar, se
    procederá a emitir el certificado depositándolo en el repositorio de Solicitudes para su descarga.`,
    CS: `Si confirma la petición, una vez examinada por el COAATM y comprobada la veracidad del hecho a certificar, se
    procederá a emitir el certificado depositándolo en el repositorio de Solicitudes para su descarga.`
  };
  ngOnInit() {

  }

  // nuevo() {
  //   this.solicitandoNuevo = true;
  // }
  // cancelar() {
  //   this.solicitandoNuevo = false;
  //   this.utils.ShowInfo('No se ha solicitado el certificado. Acción cancelada por el usuario');
  // }
  confirmarSolicitud() {
    // la confirmación no la ejecuta el control, emite un evento para que el padre haga lo que tenga que hacer
    // si el tipo es SR comprobamos si tiene seguro de responsabilidad civil 

    if (this.info.Codigo === 'SR' && !this.infocolegiado.SeguroResponsabilidadCivil) {
      this.utils.ShowError('Debe tener un seguro de responsabilidad civil dado de alta para obtener este certificado');
    } else {
      this.solicitar.emit({
        Tipo: this.info.Codigo
      });
    }

    
    // this.solicitandoNuevo = false;
  }

  lanzardescarga() {
    
    if (this.info.Ultimo && this.info.Ultimo.DocumentoAsociado) {
      this.descargar.emit(this.info.Ultimo.IdSolicitudAsociada);
    } else {
      this.utils.ShowInfo('No hay documentación asociada al certificado');
    }



  }
}
