import { TipoCertificado } from './../../models/tipocertificado.model';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertificadosOtrosComponent } from './certificados-otros.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { EstadoCertificadoPipe } from '../../pipes/estado-certificado.pipe';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('CertificadosOtrosComponent', () => {
  let component: CertificadosOtrosComponent;
  let fixture: ComponentFixture<CertificadosOtrosComponent>;
  let element: HTMLElement;

  const datoscolegiadoMock = {
    Colegiado: {
      Id: null,
      NumeroColegiado: '9098',
      Nombre: 'Sonia-Pre NUÑO RODRIGUEZ',
      FechaAlta: new Date('1995-08-01T00:00:00'),
      Situacion: 'Ejerciente',
      Sociedad: false
    },
    SeguroResponsabilidadCivil: {
      FechaAlta: new Date('2017-01-01T00:00:00'),
      FechaBaja: new Date('0001-01-01T00:00:00'),
      Compania: 'CATALANA OCCIDENTE (STA S.R.C.)',
      NombreProducto: 'SRC',
      Cobertura: '2500000,00'
    }
  };

  const datoscertificadosMock: TipoCertificado = {
    Codigo: 'CC',
    Titulo: 'Certificados de Colegiación',
    Descripcion: '',
    Ultimo: null,
    Estado: 'P',
    Certificados: [
      {
        Tipo: 'CC',
        FechaSolicitud: '2019-10-04T11:54:00',
        FechaEmision: '2019-10-04T11:54:00.067',
        FechaVigencia: '2019-11-03T11:54:00.067',
        Estado: 'P',
        IdSolicitudAsociada: 201709648,
        DocumentoAsociado: false
      },
      {
        Tipo: 'CC',
        FechaSolicitud: '2019-10-04T12:35:00',
        FechaEmision: '2019-10-04T12:35:09.437',
        FechaVigencia: '2019-11-03T12:35:09.437',
        Estado: 'P',
        IdSolicitudAsociada: 201709651,
        DocumentoAsociado: false
      },
      {
        Tipo: 'CC',
        FechaSolicitud: '2019-10-04T13:36:00',
        FechaEmision: '2019-10-04T13:36:01.35',
        FechaVigencia: '2019-11-03T13:36:01.35',
        Estado: 'P',
        IdSolicitudAsociada: 201709652,
        DocumentoAsociado: false
      }
    ]
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule, NoopAnimationsModule],
      declarations: [CertificadosOtrosComponent, EstadoCertificadoPipe],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificadosOtrosComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement;
    component.info = datoscertificadosMock;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Muestra bien el título', () => {
    // component.infocolegiado = datoscolegiadoMock;
    // component.info = datoscertificadosMock;
    // fixture.detectChanges();
    const titleElement = element.querySelector('.card-header');
    expect(titleElement.textContent).toContain(datoscertificadosMock.Titulo);

  });


});
