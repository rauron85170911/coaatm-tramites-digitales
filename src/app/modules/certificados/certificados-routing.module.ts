import { AuthGuard } from 'src/app/guards/auth.guard';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { IndexComponent } from './pages/index/index.component';


const secondaryRoutes: Routes = [
  {
    path: 'certificados',
    component: IndexComponent,
    canActivate: [AuthGuard],
    data: { titulo: 'Certificados' }
    // children: [
    //     {path: '', redirectTo: '/cognitive/imagenes', pathMatch: 'full'}
    // ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(secondaryRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class CertificadosRoutingModule {

}
