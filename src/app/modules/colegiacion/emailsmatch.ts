import { AbstractControl } from '@angular/forms';
export class Emailsmatch {
    static matchMails(ac: AbstractControl) {
        const email1 = ac.get('email');
        const email2 = ac.get('email2');
        if (email1.value === email2.value || email1.value === '' || email2.value === '') {
            email2.setErrors(null);
            return null;
        }
 
        ac.get('email2').setErrors({ mustMatch: true });
        return true;
    }
}
