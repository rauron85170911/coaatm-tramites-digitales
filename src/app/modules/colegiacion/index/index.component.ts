import { ValidacionNifResult } from './../../../models/validacionnifresult';

import { TipoDocumento } from './../../../models/tipo-documento';
import { ColegiacionService } from './../services/colegiacion.service';
import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { BackofficeService } from '../backoffice.service';
import { GuiUtilsService } from '../../mycommon/services/gui-utils.service';
import { environment} from '../../../../environments/environment';
import { NifValidationService } from '../../mycommon/validators/nif-validation.service';
import { Emailsmatch } from '../emailsmatch';
@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit, AfterContentChecked {


  titulo = 'Colegiación';
  submitido = false;
  idSolicitud = null;
  formulario: FormGroup;
  documentos: Array<TipoDocumento> = [];
  totaltamaniodocumentos = 0;


  get f() { return this.formulario.controls; }
  get environment() { return environment; }
  constructor(private formBuilder: FormBuilder, private backoffice: BackofficeService, private servicio: ColegiacionService,
              private guiUtils: GuiUtilsService, private nifValidationService: NifValidationService) {



  }


  ngOnInit() {
    this.formulario = this.formBuilder.group({
      nif: ['', [Validators.required,  this.nifValidator.bind(this)]],
      email: ['', [Validators.required, Validators.email]],
      email2: ['', [Validators.required, Validators.email]],
      recaptcha: ['', Validators.required]
    },
    {
      validator: Emailsmatch.matchMails
    });

    // cargamos los tipos de documento
    this.servicio.GetTiposDocumento().subscribe(
      response => {
        this.documentos = response;
        
      },
      error => {
        this.guiUtils.ShowErrorPage();
        // this.guiUtils.ShowError(error);
      }
    );

  }
  ngAfterContentChecked(): void {
    this.totaltamaniodocumentos = 0;
    this.documentos.forEach((documento: TipoDocumento) => {
      if (documento.file !== null) {
        this.totaltamaniodocumentos += documento.file.size;
      }
    });
    // this.total = this.documentos.reduce( (sum, item) => { sum = sum + item.file !== null ? item.file.size : 0 ; }, 0);
  }

  handleFileInput($event) {
    // de momento ponemos en el nombre del documento que corresponda al seleccionado
    const id = $event.target.id;
    this.documentos.filter(d => d.Id === id)[0].file = $event.target.files[0];
  }

  borrar(id: string) {
    this.documentos.filter(d => d.Id === id)[0].file = null;
  }

  onEnviar() {
    this.submitido = true;
    if (this.formulario.invalid) {
      return;
    }
    const algundocumentosinseleccionar = this.documentos.some( item => {
      return item.Obligatorio && !item.file;
    });
    if (algundocumentosinseleccionar) {
      this.guiUtils.ShowError('Debe adjuntar todos los documentos obligatorios');
      return;
    }
    if (this.totaltamaniodocumentos > this.environment.maxFilesSize) {
      this.guiUtils.ShowError('El tamaño total de los archivos adjuntos supera el máximo permitido (50 Mb)');
      return;
    }



    console.log('Enviando....');
    this.backoffice.NifEsValido(this.f.nif.value).subscribe( (validacionNif: ValidacionNifResult) => {
      if (validacionNif.Valido) {
        this.backoffice.EnviarSolicitud(this.f.nif.value, this.f.email.value, this.f.recaptcha.value, this.documentos).subscribe(
          response => {
            this.guiUtils.ShowSuccess(`La solicitud se registró correctamente`);
            this.idSolicitud = response;
          },
          error => {
            this.guiUtils.ShowError(error);
          }
        );
      } else {
        this.guiUtils.ShowError(validacionNif.Motivo);
      }
    },
    error => {
      this.guiUtils.ShowError(error);
    }

    );
  }

  nifValidator(control: FormControl) {
      const s = control.value;
      if (s === null || s === '') {return null; }
      const resul = this.nifValidationService.ValidateSpanishID(s);

      return resul.valid && resul.type !== 'cif' ? null : {nif : true};
  }
}
