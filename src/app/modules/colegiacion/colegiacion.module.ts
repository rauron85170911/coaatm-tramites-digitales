import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';
import { NgxCaptchaModule } from 'ngx-captcha';
import { IndexComponent } from './index/index.component';
import { MyCommonModule } from '../mycommon/mycommon.module';

@NgModule({
  declarations: [IndexComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule, FormsModule,
    HttpClientModule,
    NgxCaptchaModule,
    MyCommonModule
  ]
})
export class ColegiacionModule { }
