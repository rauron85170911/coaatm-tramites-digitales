import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError, of } from 'rxjs';
import { map, catchError, mergeMap } from 'rxjs/operators';
import { environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BackofficeService {

  rootendpoint = environment.apiUrlBase; // 'http://localhost:85';
  constructor(private http: HttpClient) { }


  
  // public GetTiposDocumentoAsPromise(): Promise<any> {
  //   const endpoint = `${this.rootendpoint}/api/colegiacion/documentos`;
  //   return this.http.get<any>(endpoint).toPromise()
  //     .then(response => {
  //       const tipos: Array<any> = [];
  //       response.data.forEach(element => {
  //          tipos.push( {tipo: element.Tipo, descripcion: element.Descripcion});
  //       });
  //       return tipos;
  //     })
  //     .catch(error => {
  //       const tipos: Array<any> = [];
  //       return tipos;
  //     });
  // }

  public NifEsValido(nif: string): Observable<any> {
    const endpoint = `${this.rootendpoint}/api/colegiacion/validarnif?nif=${nif}`;
    return this.http.get<any>(endpoint).pipe(
      map(response => {
        return response;
      }),
      catchError( () => {
        // si se produce un error consideramos que es válido
        return of({Valido: true});
      })
    );
  }

  public GetTiposDocumento(){
    const endpoint = `${this.rootendpoint}/api/colegiacion/documentos`;
    return this.http
      .get<any>(endpoint).pipe(
        map(response => {
          response.forEach(element => {
            element.file = null;
          });
          return response;
        }),
      catchError(this.handleError));
  }

  

  public EnviarSolicitud(nif: string, email: string, captchas: string, documentos: Array<any>) {
    const endpoint = `${this.rootendpoint}/api/colegiacion/iniciar`;
    const formData: FormData = new FormData();
    documentos.forEach(doc => {
      if (doc.file) {
        formData.append(doc.Id, doc.file, doc.file.name);
      }
    });
    formData.append('nif', nif);
    formData.append('email', email);
    formData.append('recaptcha', captchas);
    return this.http
      .post(endpoint, formData).pipe(
        map(response => response),
      catchError(this.handleError));
  }



  handleError(error: HttpErrorResponse) {
    let errorMessage = '';

    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      if (error.status === 0) {
        errorMessage = `${error.message}`;
      } else if (error.status === 500 ) {
        errorMessage = `${error.error.ExceptionMessage}`;
      } else {
        errorMessage = `${error.error.Message}`;
      }

    }
    return throwError(errorMessage);
  }

}
