import { Validators } from '@angular/forms';
import { Injectable } from '@angular/core';
import { Observable, throwError, of } from 'rxjs';
import { map, catchError, mergeMap } from 'rxjs/operators';
import { TipoDocumento } from 'src/app/models/tipo-documento';
import { ColegiacionProxyService } from './colegiacion-proxy.service';
import { ValidacionNifResult } from 'src/app/models/validacionnifresult';
@Injectable({
  providedIn: 'root'
})
export class ColegiacionService {

  constructor(private proxy: ColegiacionProxyService) { }

  public GetTiposDocumento(): Observable<Array<TipoDocumento>> {

    return this.proxy.GetTiposDocumento().pipe(
      map(response => {
        let tipos: TipoDocumento[] = [];
        response.forEach(element => {
          const tipo: TipoDocumento = {
            Id: element.Id,
            Descripcion: element.Descripcion,
            Obligatorio: element.Obligatorio,
            file: null
          };
          tipos = [...tipos, tipo];
          // tipos.push(tipo);
        });
        return tipos;
      })
    );
  }

  public NifEsValido(nif: string): Observable<ValidacionNifResult> {
    return this.proxy.NifEsValido(nif).pipe(
      map(response => {
        const r: ValidacionNifResult = {
          Valido: response.Valido,
          Motivo: response.Motivo
        };
        return r;
      })
    );

  }

}
