import { TestBed } from '@angular/core/testing';

import { ColegiacionProxyService } from './colegiacion-proxy.service';

describe('ColegiacionProxyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ColegiacionProxyService = TestBed.get(ColegiacionProxyService);
    expect(service).toBeTruthy();
  });
});
