import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment} from '../../../../environments/environment';
import { Observable, throwError, of } from 'rxjs';
import { map, catchError, mergeMap } from 'rxjs/operators';
import { ValidacionNifResult } from 'src/app/models/validacionnifresult';
@Injectable({
  providedIn: 'root'
})
export class ColegiacionProxyService {
  rootendpoint = environment.apiUrlBase; // 'http://localhost:85';
  constructor(private http: HttpClient) { }


  public GetTiposDocumento() {
    const endpoint = `${this.rootendpoint}/api/colegiacion/documentos`;
    return this.http.get<any>(endpoint).pipe(
      catchError(this.handleError)
    );
  }

  public NifEsValido(nif: string): Observable<any> {
    const endpoint = `${this.rootendpoint}/api/colegiacion/validarnif?nif=${nif}`;
    return this.http.get<any>(endpoint).pipe(
      catchError( () => {
        // si se produce un error consideramos que es válido
        const r: ValidacionNifResult = {
          Valido: true,
          Motivo: null
        };
        return of(r);
      })
    );
  }





  handleError(error: HttpErrorResponse) {
    let errorMessage = '';

    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      if (error.status === 0) {
        errorMessage = `${error.message}`;
      } else if (error.status === 500 ) {
        errorMessage = `${error.error.ExceptionMessage}`;
      } else {
        errorMessage = `${error.error.Message}`;
      }

    }
    return throwError(errorMessage);
  }


}
