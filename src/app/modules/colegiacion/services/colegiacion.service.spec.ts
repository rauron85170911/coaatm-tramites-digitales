import { TestBed } from '@angular/core/testing';

import { ColegiacionService } from './colegiacion.service';

describe('ColegiacionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ColegiacionService = TestBed.get(ColegiacionService);
    expect(service).toBeTruthy();
  });
});
