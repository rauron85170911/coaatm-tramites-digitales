import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/usuario';
import { UsuariosService } from 'src/app/modules/usuarios/services/usuarios.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  usuarios: Array<Usuario> = [];

  /*usuarios: Array<any> = [
    {id: '1', name: 'Nombre usuario 1', direccion: 'Dirección 1'},
    {id: '2', name: 'Nombre usuario 2', direccion: 'Dirección 2'},
    {id: '3', name: 'Nombre usuario 3', direccion: 'Dirección 3'},
    {id: '4', name: 'Nombre usuario 4', direccion: 'Dirección 4'},
    {id: '5', name: 'Nombre usuario 5', direccion: 'Dirección 5'},
    {id: '6', name: 'Nombre usuario 6', direccion: 'Dirección 6'},
    {id: '7', name: 'Nombre usuario 7', direccion: 'Dirección 7'},
    {id: '8', name: 'Nombre usuario 8', direccion: 'Dirección 8'}

  ];*/

  constructor(private servicio: UsuariosService) { }

  ngOnInit() {

  this.servicio.GetUsuarios().subscribe(
    response => {
      this.usuarios = response;
      console.log(this.usuarios);
      console.log('Todo ok al recuperar los usuarios');
    },
    error => {
      console.log('Hay un error al recuperar a los usuarios');
    }
  );

  }

}
