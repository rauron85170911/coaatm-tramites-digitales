import { Subscription } from 'rxjs';
import { ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { switchMap, map } from 'rxjs/operators';
import { Usuario } from 'src/app/models/usuario';
import { UsuariosService } from 'src/app/modules/usuarios/services/usuarios.service';

@Component({
  selector: 'app-detalle-usuario',
  templateUrl: './detalle-usuario.component.html',
  styleUrls: ['./detalle-usuario.component.scss']
})
export class DetalleUsuarioComponent implements OnInit, OnDestroy {


  idSuscripcion: Subscription;
  idusuario: string;
  usuario: Usuario = null;  // se cambiara a tipo usuario

  constructor(private route: ActivatedRoute, private servicio: UsuariosService) { }

  ngOnInit() {

    // recuperación con snapshot, al cambiar de usuario no hace nada
     /*this.idusuario = this.route.snapshot.params.id;*/

     /*this.route.params.subscribe(p => {
        this.idusuario = p.id;
    });*/

     this.idSuscripcion = this.route.params.pipe(
      map((params: Params): string => params.id)
    ).subscribe(idusuario => {
      this.idusuario = idusuario;
      this.recargarDetalleUsuario(this.idusuario);
    });


  }

  ngOnDestroy(): void {
    if (this.idSuscripcion) { this.idSuscripcion.unsubscribe(); }
  }

  public recargarDetalleUsuario(id: string) {
    this.servicio.GetUsuario(id).subscribe(
      response => {
        this.usuario = response;
        console.log(this.usuario);
        console.log('Todo ok al recuperar el detalle de usuario');
      },
      error => {
        console.log('Hay un error al recuperar el detalle de usuario');
      }
    );
  }
}

