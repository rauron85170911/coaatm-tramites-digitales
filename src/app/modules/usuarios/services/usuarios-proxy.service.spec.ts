import { TestBed } from '@angular/core/testing';

import { UsuariosProxyService } from './usuarios-proxy.service';

describe('UsuariosProxyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UsuariosProxyService = TestBed.get(UsuariosProxyService);
    expect(service).toBeTruthy();
  });
});
