import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment} from '../../../../environments/environment';
import { Observable, throwError, of } from 'rxjs';
import { map, catchError, mergeMap } from 'rxjs/operators';
import { Usuario } from 'src/app/models/usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuariosProxyService {
  // rootendpoint = environment.apiUrlBase;

  constructor(private http: HttpClient) { }

  public GetUsuarios() {
    const endpoint = `http://www.somaku.com/users`;
    return this.http.get<any>(endpoint).pipe(
      catchError(this.handleError)
    );
  }

  public GetUsuario(Id: string): Observable<any> {
    const endpoint = `http://www.somaku.com/users?id=${Id}`;
    return this.http.get<any>(endpoint).pipe(
      catchError(
        this.handleError
        /* () => { // Viene del modelo de datos (models/usuario.ts)
        // si se produce un error consideramos que es válido
        const usuario: Usuario = {
          Id: '',
          Nombre: '',
          Usuario: '',
          Email: '',
          Direccion: null,
          Telefono: '',
        };
        return of(usuario);
      })*/));
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = '';

    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      if (error.status === 0) {
        errorMessage = `${error.message}`;
      } else if (error.status === 500 ) {
        errorMessage = `${error.error.ExceptionMessage}`;
      } else {
        errorMessage = `${error.error.Message}`;
      }

    }
    return throwError(errorMessage);
  }
}
