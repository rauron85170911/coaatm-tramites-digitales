import { Injectable } from '@angular/core';
import { Observable, throwError, of } from 'rxjs';
import { map, catchError, mergeMap } from 'rxjs/operators';
import { Usuario } from 'src/app/models/usuario';
import { UsuariosProxyService } from './usuarios-proxy.service';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  constructor(private proxy: UsuariosProxyService) { }

  public GetUsuarios(): Observable<Array<Usuario>> {
    return this.proxy.GetUsuarios().pipe(
      map(response => {
        let usuarios: Usuario[] = [];
        response.forEach(element => {
          const usuario: Usuario = {
            Id: element.id,
            Usuario: element.name,
            Nombre: element.username,
            Email: element.email,
            Telefono: element.phone,
            Direccion: element.address
          };
          usuarios = [...usuarios, usuario];
          //usuarios.push(usuario);
        });
        return usuarios;
      })
    );
  }

  public GetUsuario(Id: string): Observable<Usuario> {
    return this.proxy.GetUsuario(Id).pipe(
      map(response => {
        const usuario: Usuario = {
          Id: response[0].id,
          Usuario: response[0].name,
          Nombre: response[0].username,
          Email: response[0].email,
          Telefono: response[0].phone,
          Direccion: response[0].address
        };
        return usuario;
        //const u = {};
        //let usuariosDetalle: Usuario[] = []; // modificar para que solo coja un elemento

        /*response.forEach(element => {*/
          /*const usuarioDetalle: Usuario = {
            Id: element.id,
            Usuario: element.name,
            Nombre: element.username,
            Email: element.email,
            Telefono: element.phone,
            Direccion: element.address
          };
          usuariosDetalle = [...usuariosDetalle, usuarioDetalle];*/
          //usuariosDetalle.push(usuarioDetalle);
          //console.log(`la respuesta es ${response}`);
          //console.log(usuarioDetalle);
        /*});*/
        //return usuariosDetalle;
      })
    );
  }
}
