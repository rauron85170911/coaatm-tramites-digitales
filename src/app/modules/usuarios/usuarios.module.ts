import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexComponent } from './index/index.component';
import { DetalleUsuarioComponent } from './detalle-usuario/detalle-usuario.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [IndexComponent, DetalleUsuarioComponent],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class UsuariosModule { }
