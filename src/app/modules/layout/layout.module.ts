import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { RouterModule } from '@angular/router';
import { SubtituloComponent } from './subtitulo/subtitulo.component';

@NgModule({
  declarations: [HeaderComponent, FooterComponent, SubtituloComponent],
  imports: [
    CommonModule,
    RouterModule,
    NgbModule
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    SubtituloComponent
  ]
})
export class LayoutModule { }
