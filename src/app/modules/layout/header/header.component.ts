import { GuiUtilsService } from 'src/app/modules/mycommon/services/gui-utils.service';
import { ErrorsService } from './../../mycommon/services/errors.service';
import { Component, OnInit, OnDestroy, HostListener, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { AuthenticationService } from './../../core/services/authentication.service';
import { Usuario } from '../../core/models/models.index';
import { Subscription } from 'rxjs';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { filter, map } from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  navbarOpen = false;
  usuario: Usuario = null;
  suscripcion: Subscription;
  opciones: Array<any> = [
    { text: 'Opción 1', link: 'pepe' }
  ];
  titulo = null; // 'Titulo provisional';
  isCollapsed = true;
  @ViewChild('cabecera', null) cabecera: ElementRef;

  constructor(
    private authService: AuthenticationService,
    private errorService: ErrorsService,
    private guiutils: GuiUtilsService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private renderer: Renderer2) { }

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }

  ngOnInit() {

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        let ruta = this.activatedRoute;
        while (ruta && ruta.firstChild) {
          if (ruta.firstChild.snapshot.data.titulo) {
            this.titulo = ruta.firstChild.snapshot.data.titulo;
            ruta = null;
          } else {
            ruta = ruta.firstChild;
          }
        }
      }
    });



    // this.router.events.subscribe(e => {
    //   // lo unico que hacemos es ocultar el componente en cualquier cambio
    //   // en la ruta para que el posible mensaje o error no se muestre más
    //   this.titulo = null;
    // });

    // this.guiutils.TituloPrincipal$.subscribe(titulo => {
    //   this.titulo = titulo;
    // });
    this.suscripcion = this.authService.currentUser$.subscribe(usuario => {
      this.usuario = usuario;
    });
  }

  ngOnDestroy(): void {
    this.suscripcion.unsubscribe();
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }

  mostrarInformacionUsuario() {
    this.authService.TramitesDisponibles.subscribe(tramites => {
      const info = {
        usuario: this.authService.currentUser,
        tramites: this.authService.TramitesDisponibles
      };
      this.errorService.DepurarObjeto(info);
    });

  }

  /* Funcionalidad para hacer sticky menu  */
  @HostListener('window:scroll', ['$event'])
  doSomethingOnWindowsScroll($event: Event) {
    // const scrollOffset = $event.target.children[0].scrollTop;
    const ejeY = window.pageYOffset;
    // TODO: buscar otra forma de no usar el objeto window
    //const sticky = this.cabecera.nativeElement.offsetHeight;
    //console.log('el scroll vale: ' + scrollOffset);
    if (ejeY > 0) {
      // console.log('es sticky');
      this.renderer.addClass(this.cabecera.nativeElement, 'sticky');
    } else {
      // console.log('no es sticky');
      this.renderer.removeClass(this.cabecera.nativeElement, 'sticky');
    }

  }

}
