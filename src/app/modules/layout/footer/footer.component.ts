import { Component, OnInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  @ViewChild('menucompleto', null) menucompleto: ElementRef;

  constructor(private renderer: Renderer2) { }

  ngOnInit() {

  }

}
