import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-subtitulo',
  templateUrl: './subtitulo.component.html',
  styleUrls: ['./subtitulo.component.scss']
})
export class SubtituloComponent implements OnInit {

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) { }
  subtitulo: string = null;
  ngOnInit() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.subtitulo = null;
        let ruta = this.activatedRoute;
        while (ruta && ruta.firstChild) {
          if (ruta.firstChild.snapshot.data.subtitulo) {
            this.subtitulo = ruta.firstChild.snapshot.data.subtitulo;
            ruta = null;
          } else {
            ruta = ruta.firstChild;
          }
        }
      }
    });


  }

}
