
import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError, empty } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthenticationService } from './../modules/core/services/authentication.service';
import { ErrorsService } from '../modules/mycommon/services/errors.service';
import { Router } from '@angular/router';
@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private router: Router, private authenticationService: AuthenticationService, private errorService: ErrorsService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            
            if (err.status === 401) {

                // auto logout if 401 response returned from api
                this.authenticationService.logout();
                //location.reload();
            } else if (err.status === 404) {
                // this.router.navigateByUrl('/error404', {replaceUrl: false});
            }
            this.errorService.RegistrarErrorHttp(err);
            // const error = err.error.message || err.statusText;
            return throwError(err);
        }));
    }
}
