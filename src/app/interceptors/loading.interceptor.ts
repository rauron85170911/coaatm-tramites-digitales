import { Injectable } from '@angular/core';
import {
    HttpInterceptor,
    HttpRequest,
    HttpResponse,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse,
    HttpResponseBase
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable()
export class LoadingInterceptor implements HttpInterceptor {
    constructor(private spinner: NgxSpinnerService) { }

    requestCounter = 0;
    ultima = '';
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {


        if (this.hayqueincrementar(request)) {

            this.requestCounter++;
        }
        this.setLoading();

        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {

                if (event instanceof HttpResponse) {
                    if (this.hayquedecrementar(event)) {
                        this.requestCounter--;
                    }
                    this.ultima = event.url;
                    this.setLoading();
                    console.log('event--->>>', event);
                    // this.errorDialogService.openDialog(event);
                }

                return event;
            }),
            catchError((error: HttpErrorResponse) => {

                // let data = {};
                // data = {
                //     reason: error && error.error.reason ? error.error.reason : '',
                //     status: error.status
                // };
                if (this.hayquedecrementar(error)) {
                    this.requestCounter--;
                }
                this.ultima = error.url;
                this.setLoading();
                // this.errorDialogService.openDialog(data);
                return throwError(error);
            }));
    }

    private hayqueincrementar(request: HttpRequest<any>): boolean {
        const r: RegExp = new RegExp('.*html$');
        const r2: RegExp = new RegExp('.*\/upload\/.*$');
        return !r.test(request.url) && !r2.test(request.url);
    }
    private hayquedecrementar(response: HttpResponseBase): boolean {
        const r: RegExp = new RegExp('.*html$');
        const r2: RegExp = new RegExp('.*\/upload\/.*$');
        return !r.test(response.url) && !r2.test(response.url);
    }

    private setLoading() {

        console.log(`Peticiones pendientes ${this.requestCounter}. Ultima: ${this.ultima}`);
        if (this.requestCounter > 0) {
            this.spinner.show();
        } else {
            this.spinner.hide();
        }
    }
}
