import { environment } from './../../environments/environment';
import { AuthenticationService } from './../modules/core/services/authentication.service';
import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';



@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add auth header with jwt if user is logged in and request is to api url
        // const currentUser = this.authenticationService.
        
        const token = this.authenticationService.Token;
        // const isLoggedIn = currentUser && currentUser.token;

        const isApiUrl = request.url.startsWith(environment.apiUrlBase);
        if (!!token && isApiUrl) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${token}`
                }
            });
        }

        return next.handle(request);
    }
}