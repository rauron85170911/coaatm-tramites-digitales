export interface ValidacionNifResult {
    Valido: boolean;
    Motivo: string;
}