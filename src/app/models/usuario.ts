export interface Usuario {
  Id: string;
  Nombre: string;
  Usuario: string;
  Email: string;
  Direccion: Array<TemplateStringsArray>;
  Telefono: string;
}
