import { NgxSpinnerService } from 'ngx-spinner';
import { GuiUtilsService } from 'src/app/modules/mycommon/services/gui-utils.service';
import { AuthenticationService } from './../../modules/core/services/authentication.service';
import { Component, OnInit } from '@angular/core';
import { CodigoDescripcion } from 'src/app/modules/core/models/models.index';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(public auth: AuthenticationService, private guiUtils: GuiUtilsService, private spinner: NgxSpinnerService) { }

  tramites$: Observable<CodigoDescripcion[]>;
  modelo: any = {
    Nombre: 'yo',
    Administrador: false
  };

  modelo1: any = {
    Nombre: 'Otro',
    Administrador: true
  };

  texto = 'Un mensaje de prueba';
  tipo = 'I';

  ngOnInit() {
    // this.tramites$ = this.auth.TramitesDisponibles;
  }

  cambio($event) {

    console.log(`El valor seleccionado es ${$event}`);
    this.modelo1.Administrador = $event;
  }


  showSpinner(){
    this.spinner.show();
  }
  mostrarMensaje() {
    switch (this.tipo) {
      case 'I':
        this.guiUtils.ShowInfo(this.texto);
        break;
      case 'E':
        this.guiUtils.ShowError(this.texto);
        break;
      case 'S':
        this.guiUtils.ShowSuccess(this.texto);
        break;
    }
  }

}
