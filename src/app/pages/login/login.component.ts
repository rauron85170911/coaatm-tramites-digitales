import { HttpClient } from '@angular/common/http';
import { GuiUtilsService } from './../../modules/mycommon/services/gui-utils.service';
import { AuthenticationService } from './../../modules/core/services/authentication.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first, map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

// import { AlertService, AuthenticationService } from '@/_services';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private guiUtils: GuiUtilsService,
    private http: HttpClient
    // private alertService: AlertService
  ) {
    // redirect to home if already logged in
    if (this.authenticationService.currentUser) {
      this.router.navigate(['/']);
    }
  }
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  respuesta: any = null;
  mockUsers = [
    { usuario: '9098', password: 'Santiag0', descripcion: 'Adherido y no suspendido' },
    { usuario: '1707', password: '19421942', descripcion: 'Suspendido y no adherido' },
    { usuario: '2760', password: '1303TUI', descripcion: 'Suspendido pero adherido' },
    { usuario: '70005', password: 'NUL41489', descripcion: 'Sociedad (no adherida)' },
    { usuario: '80006', password: '1111XUB', descripcion: 'Sociedad (adherida)' },
    { usuario: '10250', password: '10250', descripcion: 'Isabel Hernandez'},
    { usuario: '13520', password: 'gusbi890', descripcion: 'Alberto Hermosa Hermosa'}
  ];
  loginincorrecto = false;

  returnUrl: string;



  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    // get return url from route parameters or default to '/'
    // tslint:disable-next-line: no-string-literal
    this.returnUrl = this.route.snapshot.queryParams['return'] || '/';
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loginincorrecto = false;

    this.authenticationService.login(this.f.username.value, this.f.password.value)
      // .pipe(first())
      .subscribe(
        ok => {
          this.loading = false;
          if (ok) {
            this.router.navigate([this.returnUrl]);
          } else {
            this.loginincorrecto = true;
            // this.guiUtils.ShowError('Credenciales incorrectas');
          }
        }
        // },
        // error => {
        //   this.guiUtils.ShowError(error);
        //   this.loading = false;
        // }
      );

  }

  mockLogin(usuario) {
    this.authenticationService.login(usuario.usuario, usuario.password)
      .pipe(first())
      .subscribe(
        ok => {
          this.loading = false;
          if (ok) {
            this.router.navigate([this.returnUrl]);
          } else {
            this.guiUtils.ShowError('Credenciales incorrectas');
          }
        },
        error => {
          this.guiUtils.ShowError(error);
          this.loading = false;
        });
  }
}
