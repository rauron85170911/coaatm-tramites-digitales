// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  debug: true,
  refreshToken: false,
  apiUrlBase: 'http://172.17.22.61:86/api/api', /* cambiado el puerto 85 */
  //apiUrlBase: 'http://localhost/api',
  recaptchasKey: '6Lc9RKcUAAAAAO_niEgzk0jgrFWWGajCFuswCQ2V',
  maxFilesSize: 51200000,
  constantes: {
    CODIGOMADRID: '28079',
    CODIGOPROVINCIAMADRID: '28'
  },
  documentacion: {
    maxfilesize: 20 // en megas
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
