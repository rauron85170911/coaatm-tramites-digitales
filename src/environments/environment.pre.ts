export const environment = {
    production: true,
    debug: true,
    refreshToken: false,
    apiUrlBase: '/api/api',
    recaptchasKey: '6Lc9RKcUAAAAAO_niEgzk0jgrFWWGajCFuswCQ2V',
    maxFilesSize: 51200000,
    constantes: {
      CODIGOMADRID: '28079',
      CODIGOPROVINCIAMADRID: '28'
    },
    documentacion: {
      maxfilesize: 4 // 4Mb
    }
  };
