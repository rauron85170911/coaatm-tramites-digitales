export const environment = {
  production: true,
  debug: false,
  refreshToken: false,
  apiUrlBase: '/colegiacion/apirest',
  recaptchasKey: '6Lc9RKcUAAAAAO_niEgzk0jgrFWWGajCFuswCQ2V',
  maxFilesSize: 51200000,
  constantes: {
    CODIGOMADRID: '28079',
    CODIGOPROVINCIAMADRID: '28'
  },
  documentacion: {
    maxfilesize: 20 // 20 Mb
  }
};
